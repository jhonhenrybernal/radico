<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estasdosgestion extends Model
{
   protected $table = 'estasdosgestion';
   public $timestamps = false;
   protected $fillable = ['id','EgNombreEstado'];
}
