@extends('layout')


@section('content')
<div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Busqueda de radicados <a type="button" class="btn btn-info" href="{{url('documentos/descargar/xlsx')}}">Exportar Radicados</a></h2>
            </div>
           
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
        
        <table id="raditable" class="display " >
        <thead>
            <tr>
                <th>Asunto</th>
                <th>Fecha de creacion</th>
                <th>Fecha de vencimiento</th>
                <th>Fecha de cierre</th>
                <th>tipo solicitud</th>
                <th># radicacion</th>
                <th>Sitio radicacion</th>
                <th>Accion</th>
        </thead>
        <tbody>
        	
        </tbody>
       </table>

@endsection

