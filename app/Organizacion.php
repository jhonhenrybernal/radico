<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizacion extends Model
{
    protected $table = 'organizaciones';
    protected $fillable = ['nombre', 'tipoidentificacion_id', 'identificacion', 'telefono', 'direccion', 'url'
        ];

    /**
     * A departamento can have many comments
     */
	public function tiposidentificacion()
	{
	    return $this->hasMany(TipoIdentificacion::class);
	    // return $this->hasMany('App\TipoIdentificacion');
	}
}
