<div class="row">
    <table WIDTH="1000">
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Descripcion:</strong>
                        {!! Form::text('TdrDescripcion', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
                
        </tr>
         <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Sigla:</strong>
                        {!! Form::text('TdrSigla', null, array('class' => 'form-control', 'placeholder'=>'Maximo 3 letras y minusculas','maxlength'=>'3')) !!}
                    </div>
                </div>
            </td>
                
        </tr>
    </table>
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Generar</button>
    </div>
</div>