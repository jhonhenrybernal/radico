<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\EstadoUsuario;
use App\User;
use App\role_user;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Role::create([
            'id'          => '22',
            'name'          => 'Administrador',
            'slug'          => 'Administrador',
            'descripcion'   => 'Control total',
            'special'   => 'all-access',
        ]);

          Role::create([
            'id'          => '23',
            'name'          => 'Usuario interno',
            'slug'          => 'Usuario interno',
            'descripcion'   => 've el modulo de gestion de correspondencia,  bitacoras  y mi usuario',
            
        ]);

           Role::create([
            'id'          => '24',
            'name'          => 'Radicador',
            'slug'          => 'Radicador',
            'descripcion'   => 've el modulo de radicacion, gestion de correspondencia, parametrizaciones, bitacoras y mi usuario',
           
        ]);

           EstadoUsuario::create([
            'nombre'          => 'Ativo',
            'descripcion'     => 'Ativo',
         ]);

            EstadoUsuario::create([
            'nombre'          => 'Inactivo',
            'descripcion'     => 'Inactivo',
         ]);

             EstadoUsuario::create([
            'nombre'          => 'Bloqueado1',
            'descripcion'     => 'Bloqueado por intentos fallidos',
         ]);

              EstadoUsuario::create([
            'nombre'          => 'Bloqueado2',
            'descripcion'     => 'Bloqueado por Inactividad',
         ]);

      role_user::create([
             'id'          => '4',
            'role_id'          => '22',
            'user_id'     => '1',
            'created_at'     => '2018-07-07 05:00:00',
            'updated_at'     => '2018-07-07 05:00:00',
         ]);
     role_user::create([
             'id'          => '5',
            'role_id'          => '24',
            'user_id'     => '3',
            'created_at'     => '2018-07-07 05:00:00',
            'updated_at'     => '2018-07-07 05:00:00',
         ]);
     role_user::create([
              'id'          => '6',
            'role_id'          => '23',
            'user_id'     => '2',
            'created_at'     => '2018-07-07 05:00:00',
            'updated_at'     => '2018-07-07 05:00:00',
         ]);
     role_user::create([
             'id'          => '9',
            'role_id'          => '23',
            'user_id'     => '26',
            'created_at'     => '2018-07-07 05:00:00',
            'updated_at'     => '2018-07-07 05:00:00',
         ]);

           
    }
}
