@extends('layout')


@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar ciudad </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('ciudades.index') }}"> Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> No se pudo realizar.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($ciudad, ['method' => 'PATCH','route' => ['ciudades.update', $ciudad->id]]) !!}
        @include('ciudades.form')
    {!! Form::close() !!}

@endsection