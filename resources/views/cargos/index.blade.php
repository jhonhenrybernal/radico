@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Cargos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('cargos.create') }}">Crear nuevo cargo</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Dependencia</th>
            <th width="280px">Operacion</th>
        </tr>
        @foreach ($cargos as $cargo)
        <tr>
            <td>{{ $cargo->codigo}}</td>
             <td>{{ $cargo->nombre}}</td>
            <td>
                @foreach ($dependencias as $dependencia)
                    @if ($dependencia->id === $cargo->dependencia_id)
                        {{$dependencia->nombre}}
                    @endif
                @endforeach
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('cargos.show',$cargo->id) }}">Ver</a>
                <a class="btn btn-primary" href="{{ route('cargos.edit',$cargo->id) }}">Editar</a>
                {!! Form::open(['method' => 'DELETE','route' => ['cargos.destroy', $cargo->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
    {!! $cargos->render() !!}
@endsection