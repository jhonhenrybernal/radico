@extends('layout')


@section('content')
      <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h2 >Detalles</h2>
           <div class="col-lg-12 margin-tb">
           
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('correspondencia.index') }}"> Volver</a>
            </div>
        </div>

         
        </div>
        <div class="box-body">
           @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="row">
       
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>fecha de radicacion:</strong>
                  <?php echo e($radicado['created_at']); ?>
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>fecha de vencimiento:</strong>
                  <?php echo e($radicado['RaFechaVencimiento']); ?>
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Asignado a:</strong>
            {{(empty($usuario['name']))?'Sin Asignar':$usuario->name}}
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Asunto:</strong>
                 {{$radicado['RaAsunto']}}
                
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Observaciones:</strong>
                 {{$radicado['RaObservaciones']}}
                
                
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Usuario quien lo radica:</strong>
                {{$usuariodos['name']}}  {{$usuariodos['lastname']}}
               
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Estado actual:</strong>
                {{$estado['ErDescripcion']}}
               
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de radicacion:</strong>
                {{$tiporadicacion['TrDescripcion']}}
               
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sitio de radicacion:</strong>
                {{$sitioradicacion['SrDescripcion']}}
               
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de currie:</strong>
                {{(empty($tipocurrie['TcoDescripcion']))?'':$tipocurrie['TcoDescripcion']}}               
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo de solicitud:</strong>
                {{$radicado->tipossolicitudes['TsTipoSolicitud']}}               
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Numero de guia:</strong>
                {{$radicado['RaNoGuia']}}               
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Numero de folios:</strong>
                {{$radicado['RaNoElementos']}}               
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <tr>
                    <td>
                  
                    <strong>Cantidad de archivos adjuntado:</strong> 
                     @foreach($archivos as $value) 
                     @endforeach
                    @if(!count($archivos) >= 1)
                    {{'Sin adjuntos '}}
                    </td>
                    <td>
                   
                    @else 

                    {{count($archivos)}}
                   <a type="button" class="btn bg-olive btn-flat margin" href="/corresver/descargar/{{ $radicado->id }}">Descargar adjuntos</a>
                    @endif
                   
                    </td> 
                </tr>                
            </div>
        </div>
    </div>


    <div class="row">

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Numero de radicado: {{ $radicado['RaNumero'] }}</strong>
               
                              
            </div>
        </div>
          <div class="row">

         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              {!! DNS1D::getBarcodeHTMLtwo($radicado['RaNumero'], 'C39+') !!}
                              
            </div>
        </div>

    </div>
     <div class="box-footer">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" target="_blank" href="/radipdf/{{ $radicado->id }}">Generar Pdf</a>
    <a class="btn btn-primary" href="{{ route('gestion.index',$radicado->id) }}"> Gestionar</a>

            </div>
        </div>

     </section> 

@endsection