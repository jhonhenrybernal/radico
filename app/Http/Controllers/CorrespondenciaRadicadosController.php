<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Radicados;
use App\RadicadosAsociado;
use App\Gestiones;
use App\User;
use Yajra\DataTables\Datatables;
use Auth;
use DB;
use App\trazabilidad;
use App\Archivosdoc;
use Illuminate\Support\Facades\Storage;
use Zipper;
use App\SitioRadicado;
use App\TiposCourier;
use App\TiposSolicitudes;
use Illuminate\Support\Facades\Mail;


class CorrespondenciaRadicadosController extends Controller
{
   public function index(){

    	return view('correspondencia.index'); 
    }

    public function tablaAdmin(){

      return view('correspondencia.tabla-admin'); 
    }

     public function mostrarTabla(){
        $id = \Auth::user()->id;	   
        $Radicados = Radicados::with('usuario','estado','tiporadicacion','sitioradicacion','radicadoasociado','tipocurrie','tipossolicitudes','tipossolicitudes','destinatario')->select('Radicados.*')->where('RaUsuarioAsignar','=', $id)->whereNotBetween('RaGestionId',[11,12]);
		return Datatables::of($Radicados)->addColumn('action', function($Radicados)
			{
		 
		 return	
     '<a type="button" class="btn btn-info" href="/corresver/'.$Radicados->id.'">Mas detalles</a>'.
     '<a type="button" class="btn btn-info" href="/coregestion/crear/'.$Radicados->id.'">Gestionar</a>';

		   })->make(true);
    }

  public function mostrarTablaAdmin(){
      $id = \Auth::user()->id;     
      $Radicados = Radicados::with('usuario','estado','tiporadicacion','sitioradicacion','radicadoasociado','tipocurrie','tipossolicitudes','tipossolicitudes','destinatario')->select('Radicados.*')->whereNotBetween('RaGestionId',[11,12]);
  return Datatables::of($Radicados)->addColumn('action', function($Radicados)
    {
   
   return 
   '<a type="button" class="btn btn-info" href="/corresver/'.$Radicados->id.'">Mas detalles</a>'.
   '<a type="button" class="btn btn-info" href="/coregestion/crear/'.$Radicados->id.'">Gestionar</a>';

     })->make(true);
  }

	public function gestion($id){

		$Radicados = Radicados::find($id);
    $listDelegUsers = User::where('UsDependencia',Auth::user()->UsDependencia)->get();
    $listReasigUsers = User::all();
    
    $RadiPrincipal = (!empty($Radicadosasociado['RasRadcadoAsociado']))?Radicados::find($Radicadosasociado['RasRadcadoAsociado'])->first():'';
   
        $aRadiHistorial = trazabilidad::
        join('users as UsId', 'trazabilidad.Usuarios_UsId','=','UsId.id')
        ->join('users as asig','trazabilidad.Usuario_asig_id','=','asig.id')
        ->join('estasdosgestion','estasdosgestion.id','=','trazabilidad.status_gestion')
        ->join('radicados','radicados.id','=','trazabilidad.Radicados_RaId')
        ->select('UsId.name','UsId.lastname','trazabilidad.fecha_gestion','estasdosgestion.EgNombreEstado','radicados.RaNumero','trazabilidad.observaciones','asig.name as asig_name')
        ->where('Radicados_RaId',$id)->get();
 
        $aRadiinformacion = DB::table('radicados_asociados')
       ->join('radicados', 'radicados_asociados.RasRadicados', '=', 'radicados.id')
       ->join('tipos_radicados', 'tipos_radicados.id', '=', 'radicados.RaTipo')
       ->join('sitios_radicacion', 'sitios_radicacion.id', '=', 'radicados.RaSitio')
       ->join('users', 'users.id', '=', 'radicados.RaUsuario')
       ->join('estasdosgestion', 'estasdosgestion.id', '=', 'radicados.RaEstado')
       ->select('radicados.RaNumero','radicados.id','radicados.RaAsunto','radicados.RaObservaciones','tipos_radicados.TrDescripcion','sitios_radicacion.SrDescripcion','users.name','users.lastname','radicados.created_at','estasdosgestion.EgNombreEstado')
       ->where('RasRadicados',$id)->first();
   
       	return view('correspondencia.create',['infRadicado' => $aRadiinformacion, 'historial' => (empty($aRadiHistorial))? []: $aRadiHistorial,'lista_usuarios_reasignar' => $listReasigUsers,'lista_usuarios_delegar' => $listDelegUsers,'radicado_principal' => (!empty($RadiPrincipal['RaNumero']))?$RadiPrincipal['RaNumero']:'','estado_radicado' => $Radicados->RaTipoTiempo ]); 
    }

    public function gestionar(Request $request){
      
            if ($request->hasFile('img')) {
              $archivos = DB::table('archivos')->where('ArRadicado',$request['id_radicado'])->get();
               if (count($archivos) > 1) {
                 $destination_path = public_path('/imagenes/'.$request['id_radicado']);
               }else{
                $destination_path = public_path('/imagenes/');
               }
                $image_array = $request->file('img');

                $array_len = count($image_array);

                for ($i = 0; $i < $array_len; $i ++) {
                    $image_size = $image_array[$i]->getClientSize();
                    $image_ext = $image_array[$i]->getClientOriginalExtension();

                    $new_image_name = rand(123456, 999999) . "." . $image_ext;

                    $destination_path = public_path('/imagenes/'.$request['id_radicado']);

                    $image_array[$i]->move($destination_path, $new_image_name);

                    $img = new Archivosdoc();
                    $img->rutaarchivos = $destination_path;
                    $img->ArRadicado = $request['id_radicado'];
                    $img->archivos = $new_image_name;
                    $img->save();
                }
            } 
        
        $fecha  = DB::table('secuencias_radicacion')->orderBy('id', 'ASC')->SELECT('SecrAño')->pluck('SecrAño','id')->first();
        $code = $this->generarCodigo(5);
        $Radicados = Radicados::find($request['id_radicado']);
       
            switch ($request['select-acciones']) {
              case 2:
                  //actualizamos el radico para reasignar
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = $request['selecion-usuario-reasignar'];
                  $radicar->RaGestionId = 2;
                  $radicar->save();

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId = (empty($request['selecion-usuario-reasignar']))?Auth::user()->id:intval($request['selecion-usuario-reasignar']);
                  $valorgestiones->Estasdosgestion_EgId = 2;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero = 'Sin valor';
                  $valorgestiones->created_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")));
                  $valorgestiones->save();

                  $Users = User::where('id',(empty($request['selecion-usuario-reasignar']))?Auth::user()->id:intval($request['selecion-usuario-reasignar']))->first();
                  $datos =  [
                    'observaciones' => $request['RaObservaciones'],
                    'RaNumero' => 'Sin numero de radicado',
                    'nombre' => $Users['name'],
                    'apellido' => $Users['lastname'],
                    'email' =>  $Users['email'],
                    'asunto' => 'Radicado a sido reasignado'
                  ];

                  $this->email($datos,'radicaciones.mails.reasignar');
                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion, $request['id_radicado'],2,$request['RaObservaciones'], $request['selecion-usuario-reasignar']);

                   return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
              case 3:
               //actualizamos el radico para Delegar
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = $request['selecion-usuario-delegar'];
                  $radicar->RaGestionId = 3;
                  $radicar->save();

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId = (empty($request['selecion-usuario-delegar']))?Auth::user()->id:intval($request['selecion-usuario-delegar']);
                  $valorgestiones->Estasdosgestion_EgId = 3;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero = 'Sin valor';
                  $valorgestiones->save();

                  $Users = User::where('id',(empty($request['selecion-usuario-delegar']))?Auth::user()->id:intval($request['selecion-usuario-delegar']))->first();
                  $datos =  [
                    'observaciones' => $request['RaObservaciones'],
                    'RaNumero' => 'Sin numero de radicado',
                    'nombre' => $Users['name'],
                    'apellido' => $Users['lastname'],
                    'email' =>  $Users['email'],
                    'asunto' => 'Radicado a sido delegado'
                  ];

                  $this->email($datos,'radicaciones.mails.delegar');
                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion , $request['id_radicado'],3,$request['RaObservaciones'],$request['selecion-usuario-delegar']);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
              case 4:

                  $id_devolver_usuario = trazabilidad::where('Radicados_RaId',$request['id_radicado'])->orderBy('fecha_gestion','acs')->first();
                  
                  //actualizamos el radico para devolver
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaGestionId = 4;
                  $radicar->RaUsuarioAsignar =($id_devolver_usuario['Usuarios_UsId'])? $id_devolver_usuario['Usuarios_UsId']: $radicar['RaUsuario'];
                  $radicar->save();

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId =($id_devolver_usuario['Usuarios_UsId'])? $id_devolver_usuario['Usuarios_UsId']: $radicar['RaUsuario'];
                  $valorgestiones->Estasdosgestion_EgId = 4;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero = 'Sin valor';
                  $valorgestiones->save();

                   $Users = User::where('id',$id_devolver_usuario['Usuarios_UsId'])->first();
                  $datos =  [
                    'observaciones' => $request['RaObservaciones'],
                    'RaNumero' => 'Sin numero de radicado',
                    'nombre' => $Users['name'],
                    'apellido' => $Users['lastname'],
                    'email' =>  $Users['email'],
                    'asunto' => 'Radicado a sido devuelto'
                  ];

                  $this->email($datos,'radicaciones.mails.devolver');

                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion , $request['id_radicado'],4,$request['RaObservaciones'],Auth::user()->id);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
              case 5:
               //actualizamos el radico para ser parcial
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = Auth::user()->id;
                  $radicar->RaGestionId = 5;
                  $radicar->RaDesPersona = 0;
                  $radicar->save();

                  $radicados = $radicar;

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId = Auth::user()->id;
                  $valorgestiones->Estasdosgestion_EgId = 5;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero ='rp'.$code.$fecha;
                  $valorgestiones->save();

                  
                  $responseDay = $this->radicaciones()->fechaVencimiento(TiposSolicitudes::where('TsTipoSolicitud','solicitud')->value('id'));
                  $dayVenc = $responseDay['oTipoVenci'];

                  $radicar = new Radicados();
                  $radicar->RaNumero = 'rp'.$code.$fecha;
                  $radicar->RaUsuarioAsignar = Auth::user()->id;
                  $radicar->RaAsunto =  $radicados['RaNumero'];
                  $radicar->RaObservaciones = $request['RaObservaciones'];
                  $radicar->RaUsuario = Auth::user()->id;
                  $radicar->RaEstado = 1;
                  $radicar->RaTipo = 3;
                  $radicar->RaSitio = SitioRadicado::where('SrDescripcion','pqr')->value('id');
                  $radicar->RaCourier = TiposCourier::where('TcoDescripcion','Mensajeria')->value('id');
                  $radicar->RaTiposSolicitud = TiposSolicitudes::where('TsTipoSolicitud','solicitud')->value('id');
                  $radicar->RaDesPersona =  Auth::user()->id;
                  $radicar->RaGestionId = 11;
                  $radicar->RaFechaVencimiento = date("Y-m-d",strtotime(date("Y-m-d")."+ $dayVenc days"));
                  $radicar->RaFechaVencimiento2 =  $responseDay['RaFechaVen2'];
                  $radicar->created_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")));
                  $radicar->RaTipoTiempo = 3;
                  $radicar->RaTipoTiempoNnombre = 'iniciado';
                  $radicar->save();

                  $id = $radicar->id;

                  $valorasociado = new RadicadosAsociado();
                  $valorasociado->RasRadicados = $id;
                  $valorasociado->RasRadcadoAsociado =$request['id_radicado'];
                  $valorasociado->save();

                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion, $request['id_radicado'],5,$request['RaObservaciones'], Auth::user()->id);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa'); 
                break;
              case 6:
              //actualizamos el radico para ser definitivo
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = $radicar['RaUsuario'];
                  $radicar->RaGestionId = 6;
                  $radicar->RaDesPersona = 0;
                  $radicar->RaTipoTiempo = 6;
                  $radicar->RaFechaCierre = date("Y-m-d",strtotime(date("Y-m-d")));
                  $radicar->RaTipoTiempoNnombre = 'respondido';
                  $radicar->save();

                  $radicados = $radicar;

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId =$radicar['RaUsuario'];
                  $valorgestiones->Estasdosgestion_EgId = 6;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero ='rd'.$code.$fecha;
                  $valorgestiones->save();

                  $responseDay = $this->radicaciones()->fechaVencimiento(TiposSolicitudes::where('TsTipoSolicitud','solicitud')->value('id'));
                  $dayVenc = $responseDay['oTipoVenci'];

                  $radicar = new Radicados();
                  $radicar->RaNumero = 'rd'.$code.$fecha;
                  $radicar->RaUsuarioAsignar = Auth::user()->id;
                  $radicar->RaAsunto =  $radicados['RaNumero'];
                  $radicar->RaObservaciones = $request['RaObservaciones'];
                  $radicar->RaUsuario = Auth::user()->id;
                  $radicar->RaEstado = 1;
                  $radicar->RaTipo = 3;
                  $radicar->RaSitio = SitioRadicado::where('SrDescripcion','pqr')->value('id');
                  $radicar->RaCourier = TiposCourier::where('TcoDescripcion','Mensajeria')->value('id');
                  $radicar->RaTiposSolicitud = TiposSolicitudes::where('TsTipoSolicitud','solicitud')->value('id');
                  $radicar->RaDesPersona =  Auth::user()->id;
                  $radicar->RaGestionId = 12;
                  $radicar->RaFechaVencimiento = date("Y-m-d",strtotime(date("Y-m-d")."+ $dayVenc days"));
                  $radicar->RaFechaVencimiento2 =  $responseDay['RaFechaVen2'];
                  $radicar->created_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")));
                  $radicar->RaTipoTiempo = 3;
                  $radicar->RaTipoTiempoNnombre = 'iniciado';
                  $radicar->save();

                  $id = $radicar->id;

                  $valorasociado = new RadicadosAsociado();
                  $valorasociado->RasRadicados = $id;
                  $valorasociado->RasRadcadoAsociado = $request['id_radicado'];
                  $valorasociado->save();

                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion, $request['id_radicado'],6,$request['RaObservaciones'],  Auth::user()->id);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
              case 9:
               //actualizamos el radico para mensajeria
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = $request['selecion-usuario'];
                  $radicar->RaGestionId = 9;
                  $radicar->save();

                  $valorgestiones = new Gestiones();       
                  $valorgestiones->Usuarios_UsId = (empty($request['selecion-usuario']))?Auth::user()->id:intval($request['selecion-usuario']);
                  $valorgestiones->Estasdosgestion_EgId = 9;
                  $valorgestiones->Observaciones = $request['RaObservaciones'];
                  $valorgestiones->Radicados_RaId = $request['id_radicado'];
                  $valorgestiones->RaNumero ='Sin valor';
                  $valorgestiones->save();

                  $id_gestion =  $valorgestiones->id;
                  $this->radicaciones()->trazabilidad($id_gestion, $request['id_radicado'],9,$request['RaObservaciones'],$request['selecion-usuario']);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
                case 10:
                 //actualizamos el radico para mensajeria
                  $radicar = Radicados::find($request['id_radicado']);
                  $radicar->RaUsuarioAsignar = $radicar->RaUsuarioAsignar = (!empty( $request['selecion-usuario']))?$request['selecion-usuario']:$radicar['RaUsuario'];
                  $radicar->RaGestionId = 10;
                  $radicar->RaTipoTiempo = 10;
                  $radicar->created_at = date('Y-m-d', strtotime($request['date']));
                  $radicar->RaTipoTiempoNnombre = 'cerrado';
                  $radicar->save();

                  $valorgestion = new Gestiones();       
                  $valorgestion->Usuarios_UsId = Auth::user()->id;
                  $valorgestion->Estasdosgestion_EgId = 10;
                  $valorgestion->Observaciones = $request['RaObservaciones'];
                  $valorgestion->Radicados_RaId = $request['id_radicado'];
                  $valorgestion->RaNumero ='Sin valor';
                  $valorgestion->save();
           
                  $id_gestion =  $valorgestion->id;
                  $this->radicaciones()->trazabilidad($id_gestion, $request['id_radicado'],10,$request['RaObservaciones'],Auth::user()->id);

                  return redirect()->route('coregestion.showtwo',$request['id_radicado'])->with('success', 'Gestion exitosa');
                break;
            }              
      
    }

    public function show($id)
    {
        $radicado = Radicados::find($id);
        $usuario = $radicado->usuario;
        $estado = $radicado->estado;
        $tiporadicacion = $radicado->tiporadicacion;
        $sitioradicacion = $radicado->sitioradicacion;
        $radicadoasociado = $radicado->radicadoasociado;
        $tipocurrie = $radicado->tipocurrie;
        $tipossolicitudes = $radicado->tipossolicitudes;
        $usuariodos = $radicado->usuariodos;
        $archivos = Archivosdoc::where('ArRadicado',$radicado['id'])->get();
        $radicadoprincipal = Radicados::all();

        return view('correspondencia.show', compact('radicado', 'usuario', 'tiporadicacion', 'sitioradicacion', 'radicadoasociado', 'tipocurrie', 'tipossolicitudes', 'radicadoprincipal', 'estado', 'usuariodos','archivos'));

    }

     public function showtwo($id)
    {

       $radicado = Radicados::find($id);
       //id de gestion actual
       $aRadiId  = DB::table('gestiones')->WHERE('Radicados_RaId', '=',$id) ->orderBy('created_at', 'desc')->max('id');
       $Radicadosasociado = RadicadosAsociado::find($id);
       //id de gestion actual total
       $aRadiIdTo  = DB::table('gestiones')->WHERE('Radicados_RaId', '=',$id)->max('id');
        //numero de gestion codigo
       $aRadiNum  = DB::table('gestiones')->WHERE('id', '=',$aRadiId)->pluck('RaNumero');
       //consulta de fecha
       $aRadiDate  = DB::table('gestiones')->WHERE('id', '=',$aRadiId)->pluck('created_at');
        //consulta de usuario quien lo realizo
       $aRadiestadosu  = DB::table('gestiones')->WHERE('Radicados_RaId', '=',$id)->pluck('Usuarios_UsId');
       //consulta de observaiciones        
       $aRadiObservacines = DB::table('gestiones')->WHERE('id', '=',$aRadiId)->pluck('Observaciones');
        //consulta de observaiciones modal       
       $aRadiObservacinesmodal = DB::table('gestiones')->WHERE('id', '=',$aRadiIdTo)->value('Observaciones');
       //consulta de estado de gestion
       $aRadiGestion = DB::table('gestiones')->WHERE('id', '=',$aRadiId)->pluck('Estasdosgestion_EgId');
       //colsulta usuario quien lo realiza
       $aRadiUsuario = DB::table('users')->WHERE('id', '=',$aRadiestadosu)->pluck('name');

       //consulta de estado de gestion historico por uno co id de radicado
       $aRadiestadohisto  = DB::table('gestiones')
       ->join('users', 'gestiones.Usuarios_UsId' , '=', 'users.id')
      
       ->join('radicados', 'gestiones.Radicados_RaId' , '=', 'radicados.id')
       ->select('gestiones.*', 'users.name')
       ->WHERE('Radicados_RaId', '=',$id)
       ->WHERE('Usuarios_UsId', '=',$aRadiestadosu)->get();

         
       //consulta de numero de gestion
       $aRadiNumGes = DB::table('gestiones')->WHERE('Radicados_RaId', '=',$id) ->orderBy('created_at', 'desc')->max('RaNumero');
        
        return view('correspondencia.showtwo', compact('radicado','aRadiestadohisto','aRadiDate','aRadiNum','aRadiObservacines','aRadiUsuario','aRadiNumGes','aRadiId','aRadiIdTo','aRadiObservacinesmodal'));
    }

    public function showmodal($id)
    {
  
      $val = Gestiones::find($id);
        return response()->json($val);  

    }


     function generarCodigo($longitud)
    {
        $key = '';
        $pattern = '1234567890';
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i ++)
            $key .= $pattern{mt_rand(0, $max)};
        return $key;
    }

    public function descargarArchivo($idRadicado)
    {
      $archivos = DB::table('archivos')->where('ArRadicado',$idRadicado)->get();
     if (count($archivos)  > 1 ) {
        
          
              $headers = ["Content-Type"=>"application/zip"];
              $namefilezip ='documento'.$idRadicado.'.zip';
              
              Zipper::make(public_path('/imagenes/'.'documento'.$idRadicado.'.zip'))->add(public_path()."/imagenes/".$idRadicado)->close(); //files tobe zipped
              return response()->download(public_path('/imagenes/'.$namefilezip),$namefilezip, $headers);


            
      }else{
          foreach ($archivos as  $value) {
            $archivo = $value->archivos;
            $ruta =  $value->rutaarchivos;
            $pathtoFile = public_path().'/imagenes/'.$idRadicado.'/'.$archivo;
            return response()->download($pathtoFile);
        }
      }
      
        

    }

     public function descargarEjemploArchivo()
      {
        return response()->download(public_path('/documents/ejemploCarga/ejemplo.xls'));
      }

      public function email($datos,$view){
              
                Mail::send($view,$datos, function($message) use ($datos){
                $message->subject($datos['asunto']);
                $message->to($datos['email']);
                $message->from('correspondenciaradicados@epmcibate.com','anuncios ');
                });               
    }
      
        
}

