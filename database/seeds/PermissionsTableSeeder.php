<?php

use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'id'          => '1',
            'name'        => 'Pagina principal de role',
            'slug'        => 'roles.index',
            'description' => 'Permiso de la pagina principal de roles',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '2',
            'name'        => 'Ver usuarios',
            'slug'        => 'usuarios',
            'description' => 'Muestra el link de usuarios',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '3',
            'name'        => 'Ver correspondencia',
            'slug'        => 'correspondencia',
            'description' => 'Permite ver vista correspondencia',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '4',
            'name'        => 'Ver radicacion',
            'slug'        => 'radicacion',
            'description' => 'Permite ver vista radicacion',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '5',
            'name'        => 'Ver parametrizacion general',
            'slug'        => 'parageneral',
            'description' => 'Permite ver vista parametrizacion general',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '6',
            'name'        => 'Ver parametrizacion radicacion',
            'slug'        => 'pararadicacion',
            'description' => 'Permite ver vista parametrizacion radicacion',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '7',
            'name'        => 'Ver parametrizacion correspondencia',
            'slug'        => 'paracorrespondencia',
            'description' => 'Permite ver vista parametrizacion correspondencia',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '8',
            'name'        => 'Ver bitacora',
            'slug'        => 'bitacora',
            'description' => 'Permite ver vista bitacora',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

        Permission::create([
            'id'          => '9',
            'name'        => 'Ver gestion de radicaciones',
            'slug'        => 'radicaciones.create',
            'description' => 'Permite ver vista radicaciones a crear',
            'created_at'  => '2018-07-09 11:05:37',
            'updated_at'  => '2018-07-09 11:05:37',
        ]);

    }
}
