<?php

use Illuminate\Database\Seeder;
use App\TiposSolicitudes;

class TiposSolicitudesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TiposSolicitudes::create([
            'TsTipoSolicitud'   => 'Peticion',
            'TsTiempoSolicitud' => '24',
         ]);

        TiposSolicitudes::create([
            'TsTipoSolicitud'   => 'Queja',
            'TsTiempoSolicitud' => '24',
         ]);

        TiposSolicitudes::create([
            'TsTipoSolicitud'   => 'Reclamo',
            'TsTiempoSolicitud' => '24',
         ]);

        TiposSolicitudes::create([
            'TsTipoSolicitud'   => 'Solicitud',
            'TsTiempoSolicitud' => '24',
         ]);

        TiposSolicitudes::create([
            'TsTipoSolicitud'   => 'Denuncia',
            'TsTiempoSolicitud' => '24',
         ]);


    }
}
