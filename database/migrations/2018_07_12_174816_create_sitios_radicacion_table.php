<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitiosRadicacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitios_radicacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SrDescripcion',255);
            $table->integer('SrSecuenciaNoRad')->default(1);
            $table->string('SrFuncionNumeracion',255);
            $table->string('SrSigla',3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_sitios_radicacion');
    }
}
