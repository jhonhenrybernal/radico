
<div class="row">
    <input type="hidden" name="remember_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Apellido:</strong>
            {!! Form::text('lastname', null, array('placeholder' => 'Apellido','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Usuario de red:</strong>
            {!! Form::text('username', null, array('placeholder' => 'Usuario','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Password:</strong>
            {!! Form::password('password', null, array('placeholder' => 'Password','class' => 'form-control','required')) !!}
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tipo de usuario:</strong>
           <select name='tipo_role' id='tipo_role' class="form-control" required>
              <option value="">Seleccione por favor</option>
              @foreach ($roles as $tipo)
              <option value="{{ $tipo['id'] }}">{{ $tipo['name'] }}</option>
              @endforeach
            </select>

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tipo de identificacion:</strong>
            <select name='UsTipoIdentificacion' id='UsTipoIdentificacion' class="form-control" >
              <option value="">Seleccione por favor</option>
              @foreach ($oLisIdentidad as $tipo)
              <option value="{{ $tipo['id'] }}">{{ $tipo['nombre'] }}</option>
              @endforeach
            </select>
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Numero de identificacion:</strong>
            {!! Form::number('UsIdentificacion', null, array('placeholder' => 'numero','class' => 'form-control','required')) !!}
        </div>
    </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Numero telefonico local:</strong>
            {!! Form::text('UsTelefono', null, array('placeholder' => 'numero','class' => 'form-control','required')) !!}
        </div>
    </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Numero telefonico celular:</strong>
            {!! Form::text('UsCelular', null, array('placeholder' => 'numero','class' => 'form-control')) !!}
        </div>
    </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tipo de cargo:</strong>
           <select name='UsCargos' id='UsCargos' class="form-control" >
              <option value="">Seleccione por favor</option>
              @foreach ($oLisCargo as $tipo)
              <option value="{{ $tipo['id'] }}">{{ $tipo['nombre'] }}</option>
              @endforeach
            </select>
        </div>
    </div>
     <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tipo de dependencia:</strong>
           <select name='UsDependencia' id='UsDependencia' class="form-control" >
              <option value="">Seleccione por favor</option>
              @foreach ($oLisDependencia as $tipo)
              <option value="{{ $tipo['id'] }}">{{ $tipo['nombre'] }}</option>
              @endforeach
            </select>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>