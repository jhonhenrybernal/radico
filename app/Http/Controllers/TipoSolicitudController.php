<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposSolicitudes;

class TipoSolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $tsolicitud = TiposSolicitudes::all();
        return view('tiposolicitud.index',compact('tsolicitud'));
    }    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $tsolicitud = TiposSolicitudes::all();
        return view('tiposolicitud.create', compact('tsolicitud'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
        'TsTipoSolicitud' => 'required',
        'TsTiempoSolicitud' => 'required',
        'descripcion' => 'required',
       
        ],
     [
        'TsTiempoSolicitud.min' => 'Por favor maximo 30 dias',
        
    ]);
        TiposSolicitudes::create([
            'TsTipoSolicitud' => request()->TsTipoSolicitud,
            'TsTiempoSolicitud' => request()->TsTiempoSolicitud,
            'descripcion' => request()->descripcion,
            'TsSigla' => strtoupper(request()->TsSigla)
        ]);
        return redirect()->route('tiposolicitud.index')
                        ->with('success','Sitio creado exitosamente');
    }

   
    public function edit($id)
    {
        $tsolicitud = TiposSolicitudes::find($id);
        return view('tiposolicitud.edit',compact('tsolicitud'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'TsTipoSolicitud' => 'required',
            'TsTiempoSolicitud' => 'required',
            'descripcion' => 'required',
        ]);
        TiposSolicitudes::find($id)->update($request->all());
        return redirect()->route('tiposolicitud.index')
                        ->with('success','Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         TiposSolicitudes::find($id)->delete();
        return redirect()->route('tiposolicitud.index')
                        ->with('success','Eliminado');
    }
}
