<?php

use App\permission_role;
use Illuminate\Database\Seeder;

class permission_roleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        permission_role::create([
            'id'            => '11',
            'permission_id' => '3',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '12',
            'permission_id' => '6',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '13',
            'permission_id' => '7',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '14',
            'permission_id' => '5',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '15',
            'permission_id' => '6',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '16',
            'permission_id' => '8',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '17',
            'permission_id' => '3',
            'role_id'       => '23',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

        permission_role::create([
            'id'            => '18',
            'permission_id' => '8',
            'role_id'       => '23',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);
        permission_role::create([
            'id'            => '19',
            'permission_id' => '4',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);
        permission_role::create([
            'id'            => '20',
            'permission_id' => '9',
            'role_id'       => '24',
            'created_at'    => '2018-07-07 05:00:00',
            'updated_at'    => '2018-07-07 05:00:00',
        ]);

    }
}
