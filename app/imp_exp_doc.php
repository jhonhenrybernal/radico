<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imp_exp_doc extends Model
{
    protected $table = 'imp_exp_doc';
    public $timestamps = false;
    protected $fillable = ['TipoRequerimiento', 'Folios', 'txtDependencia','estado','tipo_radicado','tipo_documento','asignar','Observaciones','asunto',
        ];
}
