<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
		protected $table = 'tiposusuario';
		public $timestamps = false;
        protected $fillable = ['nombre', 'descripcion'
        ];
}
