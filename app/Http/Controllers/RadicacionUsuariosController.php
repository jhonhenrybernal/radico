<?php
namespace App\Http\Controllers;

use App\Ciudad;
use DB;
use App\Departamento;
use App\Pais;
use App\Persona;
use App\User;
use App\Roles;
use App\Archivosdoc;
use App\RolUsuario;
use App\Radicados;
use App\TipoIdentificacion;
use App\EstadosRadicados;
use App\TiposRadicados;
use App\TiposSolicitudes;
use App\RadicadosAsociado;
use App\TiposCourier;
use App\SitioRadicado;
use App\TiposDocumentalesRadicados;
use App\Remitente;
use App\Destinatarios;
use App\Gestiones;
use App\trazabilidad;
use Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class RadicacionUsuariosController extends Controller
{

    public function create()
    {
        $pais = Pais::all();
        $departamento = Departamento::all();
        $ciudad = Ciudad::all();
        $tipoidentificacion = TipoIdentificacion::all();
        $persona = persona::all();
        $usuario = User::all();
        $radicado = EstadosRadicados::all();
        $tipoderadicados = TiposRadicados::all();
        $tipodesolicitudes = TiposSolicitudes::all();
        $radicadoasociar = RadicadosAsociado::all();
        $tiposdecurrie = TiposCourier::all();
        $sitioradicar = SitioRadicado::all();
        $tiposducmentos = TiposDocumentalesRadicados::all();
        $radicadoprincipal = Radicados::all();
        return view('radicaciones.create', compact('pais', 'departamento', 'ciudad', 'tipoidentificacion', 'persona', 'usuario', 'radicado', 'tipoderadicados', 'tipodesolicitudes', 'radicadoasociar', 'tiposdecurrie', 'sitioradicar', 'tiposducmentos', 'radicadoprincipal'));
    }

    public function emerpersona()
    {
        $pais = Pais::all();
        $departamento = Departamento::all();
        $ciudad = Ciudad::all();
        $tipoidentificacion = TipoIdentificacion::all();
        return view('radicaciones.modalrem', compact('pais', 'departamento', 'ciudad', 'tipoidentificacion'));
    }

    public function emerdesti()
    {
        $user = User::latest()->paginate(5);
        $roles = Roles::latest()->paginate(5);
        return view('radicaciones.modaldes', compact('user', 'roles'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function storedesti(Request $request)
    {
        request()->validate([
            'name' => 'required|max:255|unique:users',
            'lastname' => 'required|max:255|unique:users',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required',
            'tipo_role' => 'required'
        ], [
            'name.required' => 'El campo nombre es obligatorio',
            'lastname.required' => 'El campo apellido es obligatorio',
            'username.required' => 'El campo usuario de red es obligatorio',
            'email.required' => 'El campo Email de red es obligatorio',
            'password.required' => 'El campo password de red es obligatorio',
            'tipo_role.required' => 'El campo Tipo de usuario de red es obligatorio',
            'lastname.unique' => 'El apellido ya esta registrado',
            'username.unique' => 'El usuario de red ya esta registrado',
            'email.unique' => 'El Email ya esta registrado'
        ]);
        $usuario = User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'remember_token' => $request['remember_token']
        ]);
        $id_user = $usuario->id;

        RolUsuario::create([
            'user_id' => $id_user,
            'role_id' => $request['tipo_role']
        ]);
        return redirect()->route('radicaciones.emerdesti')->with('success', 'Creacion de usuario exitosa');
    }

    public function storepersona(Request $request)
    {
        request()->validate([
        'primer_nombre' => 'required',
        'primer_apellido' => 'required',
        'segundo_apellido' => 'required',
        'tipoidentificacion_id' => 'required',
        'identificacion'=> 'required|unique:personas,identificacion', 
        ]);

        $segundo_nombre = (empty(request()->segundo_nombre))?'Sin Apellido':request()->segundo_nombre;
        
        Persona::create([
            'primer_nombre' => request()->primer_nombre,
            'segundo_nombre' =>  $segundo_nombre,
            'primer_apellido' => request()->primer_apellido,
            'segundo_apellido' => request()->segundo_apellido,
            'tipoidentificacion_id' => request()->tipoidentificacion_id,
            'identificacion' => request()->identificacion,
            'telefono_fijo' => request()->telefono_fijo,
            'telefono_movil' => request()->telefono_movil,
            'email' => request()->email,
            'direccion' => request()->direccion,
            'pais_id' => request()->pais_id,
            'departamento_id' => request()->departamento_id,
            'ciudad_id' => request()->ciudad_id,
            'ocupacion' => request()->ocupacion,
        ]);
        return redirect()->route('radicaciones.vistauno')->with('success', 'Remitente guardado exitosamente')->withInput();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    function generarCodigo($longitud)
    {
        $key = '';
        $pattern = '1234567890';
        $max = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i ++)
            $key .= $pattern{mt_rand(0, $max)};
        return $key;
    }

     




    public function store(Request $request)
    {
       
        $code = $this->generarCodigo(5); 
        $ordencodigoradicacion = $this->raNumero($request);

        $RaUsuMail = $request['RaUsuarioAsignar'];
        $RaPerMail = $request['RePersona'];
        $datos = [
                       'RaUsuarioAsignar'=> $request['RaUsuarioAsignar'],
                       'code' => $ordencodigoradicacion,
                       'RaUsuarioAsignar'=> $request['RaUsuarioAsignar'],
                       'RaEstado'=> $request['RaEstado'],
                       'RaTipo'=> $request['RaTipo'],
                       'RaSitio'=> $request['RaSitio'],
                       'RaTiposSolicitud'=> $request['RaTiposSolicitud'],
                       'RaObservaciones'=> $request['RaObservaciones'],
                       'RaAsunto'=> $request['RaAsunto']];

      $this->Email($RaUsuMail,$RaPerMail,$datos);

        request()->validate([
            'RaEstado' => 'required',
            'RaTipo' => 'required',
            'RaTiposSolicitud' => 'required',
            'RaSitio' => 'required',
            'RaAsunto' => 'required',
            'RaObservaciones' => 'required',
            'RaUsuarioAsignar' => 'required',
            'RePersona' => 'required'
        ], [
            'RaEstado.required' => 'El campo Estado  es obligatorio',
            'RaTipo.required' => 'El campo Tipo radicado es obligatorio',
            'RaTiposSolicitud.required' => 'El campo tipo de radicacion es obligatorio',
            'RaSitio.required' => 'El campo Sitio de radicacion es obligatorio',
            'RaAsunto.required' => 'El campo Asunto es obligatorio',
            'RaObservaciones.required' => 'El campo Observaciones es obligatorio',
            'RaUsuarioAsignar.required' => 'El campo Asignar a es obligatorio',
            'RePersona.required' => 'El campo Informacion de remitente a es obligatorio'
        ]);

        $responseDay = $this->fechaVencimiento($request['RaTiposSolicitud']);
        $dayVenc = $responseDay['oTipoVenci'];
    
        $radicar = new Radicados();
        $radicar->RaNumero = $ordencodigoradicacion;
        $radicar->RaUsuarioAsignar = $request['RaUsuarioAsignar'];
        $radicar->RaAsunto = $request['RaAsunto'];
        $radicar->RaObservaciones = $request['RaObservaciones'];
        $radicar->RaUsuario = Auth::user()->id;
        $radicar->RaEstado = $request['RaEstado'];
        $radicar->RaTipo = $request['RaTipo'];
        $radicar->RaSitio = $request['RaSitio'];
        $radicar->RaCourier = $request['RaCourier'];
        $radicar->RaTiposSolicitud = $request['RaTiposSolicitud'];
        $radicar->RaNoGuia = $request['RaNoGuia'];
        $radicar->RaNoElementos = $request['RaNoElementos'];
        $radicar->RaTipoDocumental = $request['RaTipoDocumental'];
        $radicar->RaDesPersona = ($request['DesPersona'] == 0)?$request['RePersona']:$request['DesPersona'];
        $radicar->RaGestionId = 1;
        $radicar->RaFechaVencimiento = date("Y-m-d",strtotime(date("Y-m-d")."+ $dayVenc days"));
        $radicar->RaFechaVencimiento2 =  $responseDay['RaFechaVen2'];
        $radicar->created_at = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")));
        $radicar->RaTipoTiempo = 3;
        $radicar->RaTipoTiempoNnombre = 'iniciado';
        $radicar->save();

        $id = $radicar->id;
        $id_asignar = $radicar->RaUsuarioAsignar;
        $valor = "1";
        $id_radicado = $radicar->$code;

        $valorasociado = new RadicadosAsociado();
        $valorasociado->RasRadicados = $id;
        $valorasociado->RasRadcadoAsociado = $request['RasRadcadoAsociado'];
        $valorasociado->save();


        if ($request['RasRadcadoAsociado']) {
          //  $this->trazabilidad($id_gestion = null, $request['RasRadcadoAsociado'], 8,$request['RaObservaciones']);
        }

        $valorgestiones = new Gestiones();
        $valorgestiones->Radicados_RaId = $id;
        $valorgestiones->Usuarios_UsId = Auth::user()->id;
        $valorgestiones->Estasdosgestion_EgId = 1;
        $valorgestiones->Observaciones = $request['RaObservaciones'];
        $valorgestiones->RaNumero = $ordencodigoradicacion;
        $valorgestiones->save();

        $idGestiones = $valorgestiones->id;
        
        request()->validate([
            'RePersona' => 'required',
        ], [
            'RePersona.required' => 'El campo Informacion de remitente a es obligatorio',
        ]);
        

        $remiradicado = new Remitente();
        $remiradicado->ReRadicado = $id;
        $remiradicado->RePersona = $request['RePersona'];
        $remiradicado->save();

        $remidestinatario = new Destinatarios();
        $remidestinatario->DesRadicado = $id;
        $remidestinatario->DesPersona = $request['DesPersona'];
        $remidestinatario->save();


        $this->trazabilidad($idGestiones ,$id,1,$request['RaObservaciones'],$request['RaUsuarioAsignar']);

        if ($request->hasFile('file')) {
             $image_array = $request->file('file');

            $array_len = count($image_array);

            for ($i = 0; $i < $array_len; $i ++) {
                $image_size = $image_array[$i]->getClientSize();
                $image_ext = $image_array[$i]->getclientoriginalname();

                $new_image_name = rand(123456, 999999) . "_" . $image_ext;

                $destination_path = public_path('\imagenes\/'.$id);
                
                $image_array[$i]->move($destination_path, $new_image_name);

                $img = new Archivosdoc();
                $img->rutaarchivos = $destination_path;
                $img->ArRadicado = $id;
                $img->archivos = $new_image_name;
                $img->save();
              }

        } 
            return redirect()->route('radicaciones.show', $id)
                ->with('success', 'Creacion de radicacion exitosa')
                ->with('id_radicado', $id);
       
    }

    public function Email($RaUsuMail,$RaPerMail,$datos){
               $user = User::findOrFail($RaUsuMail);
              
                               
                Mail::send('radicaciones.mails.nuevoradicado',$datos, function($message) use ($user){
                $message->subject('Nuevo radicado');
                $message->to($user->email);
                $message->from('correspondenciaradicados@epmcibate.com','anuncios ');
                });
                if (!$RaPerMail== null) {
                 
                $per = Persona::findOrFail($RaPerMail);
                Mail::send('radicaciones.mails.nuevoradicadocliente',$datos, function($message) use ($per){
                $message->subject('Su solicitud fue creado');
                $message->to((empty($per->email))?'mailes@mailer.com':$per->email );
                $message->from('correspondenciaradicados@epmcibate.com','anuncios ');
                });
                }
    }

    public function show($id)
    {
        $radicado = Radicados::find($id);
        $usuario = $radicado->usuario;
        $estado = $radicado->estado;
        $tiporadicacion = $radicado->tiporadicacion;
        $sitioradicacion = $radicado->sitioradicacion;
        $radicadoasociado = $radicado->radicadoasociado;
        $tipocurrie = $radicado->tipocurrie;
        $tipossolicitudes = $radicado->tipossolicitudes;
        $usuariodos = $radicado->usuariodos;
        $archivos = Archivosdoc::where('ArRadicado',$radicado['id'])->get();
        $radicadoprincipal = Radicados::all();
       
        return view('radicaciones.show', compact('radicado', 'usuario', 'tiporadicacion', 'sitioradicacion', 'radicadoasociado', 'tipocurrie', 'tipossolicitudes', 'radicadoprincipal', 'estado', 'usuariodos','archivos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response /**
     *         Update the specified resource in storage.
     *        
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getRadicados(Request $request)
    {

        $term = trim($request->q);

   

        $aUsuarioAsig = User::name($term)->orderBy('id','DESC')->get();
       
   
        
        return response()->json($aUsuarioAsig);
      
    }

   
    public function getRemitente(Request $request)
    {
         $term = trim($request->q);
         $aPersoRemite = Persona::persona($term)->orderBy('id','DESC')->get();

          return response()->json($aPersoRemite);
    }

     public function getRadicadoAsociado()
    {
         $aRadiAsoci = DB::table('radicados')->SELECT('id','RaNumero')->get();

          return response()->json($aRadiAsoci);
    }

    public function trazabilidad($id_gestion = null,$id_radicado, $id_status,$RaObservaciones,$id_asignar)
    {
        $trazabilidad = new trazabilidad;
            $trazabilidad->Usuarios_UsId = Auth::user()->id; 
            $trazabilidad->Radicados_RaId = $id_radicado;
            $trazabilidad->Usuario_asig_id = $id_asignar;
            $trazabilidad->fecha_gestion = date("Y-m-d h:i:s");
            $trazabilidad->Gestiones_GeId = ($id_gestion)?$id_gestion:null;
            $trazabilidad->status_gestion = $id_status;
            $trazabilidad->observaciones = $RaObservaciones;
            $trazabilidad->save();
    }

    public function raNumero($request){
       
        $code = $this->generarCodigo(5); 
                     
        $id_rasitio = $request['RaSitio'];
        $rasitio  = DB::table('sitios_radicacion')->WHERE('id', '=',$id_rasitio)->pluck('SrSigla','id')->first();

        $id_ratipodoc = $request['RaTipoDocumental'];
        $ratipodoc  = DB::table('tipos_documentales_radicados')->WHERE('id', '=',$id_ratipodoc)->pluck('TdrSigla','id')->first();

        $id_radtipo = $request['RaTipo'];
        $radtipo  = DB::table('tipos_radicados')->WHERE('id', '=',$id_radtipo)->pluck('TrSigla','id')->first();

        $id_tiposol = $request['RaTiposSolicitud'];
        $tiposol  = DB::table('tipos_solicitudes')->WHERE('id', '=',$id_tiposol)->pluck('TsSigla','id')->first();

        $fecha  = DB::table('secuencias_radicacion')->orderBy('id', 'ASC')->SELECT('SecrAño')->pluck('SecrAño','id')->first();

        //Ajuste generar codigo de radicacion
         $valorsecuencia  = DB::table('secuencias_radicacion')->orderBy('id', 'ASC')->SELECT('SecrNumero')->pluck('SecrNumero','id')->first();
        if ($valorsecuencia == 1) {
          $ordencodigoradicacion = $rasitio.$radtipo.$ratipodoc.$tiposol.$code.$fecha;
        } elseif ($valorsecuencia == 2) {
           $ordencodigoradicacion = $radtipo.$rasitio.$ratipodoc.$tiposol.$code.$fecha;
        } elseif ($valorsecuencia == 3) {
           $ordencodigoradicacion = $radtipo.$ratipodoc.$rasitio.$tiposol.$code.$fecha;
        }elseif ($valorsecuencia == 4) {
           $ordencodigoradicacion = $radtipo.$ratipodoc.$tiposol.$rasitio.$code.$fecha;
        }elseif ($valorsecuencia == 5) {
           $ordencodigoradicacion = $radtipo.$ratipodoc.$tiposol.$code.$rasitio.$fecha;
        }elseif ($valorsecuencia == 6) {
           $ordencodigoradicacion = $radtipo.$ratipodoc.$tiposol.$code.$fecha.$rasitio;
        }elseif ($valorsecuencia == 7) {
           $ordencodigoradicacion = $ratipodoc.$radtipo.$tiposol.$code.$fecha.$rasitio;
        }elseif ($valorsecuencia == 8) {
           $ordencodigoradicacion = $ratipodoc.$tiposol.$radtipo.$code.$fecha.$rasitio;
        }elseif ($valorsecuencia == 9) {
           $ordencodigoradicacion = $ratipodoc.$tiposol.$code.$radtipo.$fecha.$rasitio;
        }elseif ($valorsecuencia == 10) {
           $ordencodigoradicacion = $ratipodoc.$tiposol.$code.$fecha.$radtipo.$rasitio;
        }elseif ($valorsecuencia == 11) {
           $ordencodigoradicacion = $ratipodoc.$tiposol.$code.$fecha.$rasitio.$radtipo;
        }elseif ($valorsecuencia == 12) {
           $ordencodigoradicacion = $tiposol.$ratipodoc.$code.$fecha.$rasitio.$radtipo;
        }elseif ($valorsecuencia == 13) {
           $ordencodigoradicacion = $tiposol.$code.$ratipodoc.$fecha.$rasitio.$radtipo;
        }elseif ($valorsecuencia == 14) {
           $ordencodigoradicacion = $tiposol.$code.$fecha.$ratipodoc.$rasitio.$radtipo;
        }elseif ($valorsecuencia == 15) {
           $ordencodigoradicacion = $tiposol.$code.$fecha.$rasitio.$ratipodoc.$radtipo;
        }elseif ($valorsecuencia == 16) {
           $ordencodigoradicacion = $tiposol.$code.$fecha.$rasitio.$radtipo.$ratipodoc;
        }elseif ($valorsecuencia == 17) {
           $ordencodigoradicacion = $code.$tiposol.$fecha.$rasitio.$radtipo.$ratipodoc;
        }elseif ($valorsecuencia == 18) {
           $ordencodigoradicacion = $code.$fecha.$tiposol.$rasitio.$radtipo.$ratipodoc;
        }elseif ($valorsecuencia == 19) {
           $ordencodigoradicacion = $code.$fecha.$rasitio.$tiposol.$radtipo.$ratipodoc;
        }elseif ($valorsecuencia == 20) {
           $ordencodigoradicacion = $code.$fecha.$rasitio.$radtipo.$tiposol.$ratipodoc;
        }elseif ($valorsecuencia == 21) {
           $ordencodigoradicacion = $code.$fecha.$rasitio.$radtipo.$ratipodoc.$tiposol;
        }elseif ($valorsecuencia == 22) {
           $ordencodigoradicacion = $fecha.$code.$rasitio.$radtipo.$ratipodoc.$tiposol;
        }elseif ($valorsecuencia == 23) {
           $ordencodigoradicacion = $fecha.$rasitio.$code.$radtipo.$ratipodoc.$tiposol;
        }elseif ($valorsecuencia == 24) {
           $ordencodigoradicacion = $fecha.$rasitio.$radtipo.$code.$ratipodoc.$tiposol;
        }elseif ($valorsecuencia == 25) {
           $ordencodigoradicacion = $fecha.$rasitio.$radtipo.$ratipodoc.$code.$tiposol;
        }elseif ($valorsecuencia == 26) {
           $ordencodigoradicacion = $fecha.$rasitio.$radtipo.$ratipodoc.$tiposol.$code;
        }elseif ($valorsecuencia == 27) {
           $ordencodigoradicacion = $radtipo.$ratipodoc.$code.$fecha;
        }

        return $ordencodigoradicacion;
    }

    public function fechaVencimiento($id){
        $oTipoVenci = DB::table('tipos_solicitudes')->WHERE('id', '=',$id)->pluck('TsTiempoSolicitud','id')->first();
        $RaFechaVen = date("Y-m-d",strtotime(date("Y-m-d")."+ $oTipoVenci days"));
        $RaFechaVen2 = date("Y-m-d",strtotime(date($RaFechaVen)."- 3 days"));

        $response = [
            'oTipoVenci' => $oTipoVenci,
            'RaFechaVen2' => $RaFechaVen2
        ];
   
        return $response;
    }
 }
