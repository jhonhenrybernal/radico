<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('vendor.adminlte.auth.login');
});

Route::get('/home', function () {
    return view('home');
});

Route::middleware(['auth'])->group(function () {

    //paises
    Route::GET('paises', 'PaisController@index')->name('paises.index')
        ->middleware('permission:paises.index');
    Route::POST('paises', 'PaisController@store')->name('paises.store')
        ->middleware('permission:paises.index');
    Route::GET('paises/create', 'PaisController@create')->name('paises.create')
        ->middleware('permission:paises.index ');
    Route::PUT('paises/{paise}', 'PaisController@update')->name('paises.update')
        ->middleware('permission:paises.index');
    Route::GET('paises/{paise}', 'PaisController@show')->name('paises.show')
        ->middleware('permission:paises.index');
    Route::DELETE('paises/{paise}', 'PaisController@destroy')->name('paises.destroy')
        ->middleware('permission:paises.index');
    Route::GET('paises/{paise}/edit', 'PaisController@edit')->name('paises.edit')
        ->middleware('permission:paises.index');
    Route::GET('paises/{paise}', 'PaisController@show')->name('paises.show')
        ->middleware('permission:paises.index');
    Route::GET('paises/{paise}', 'PaisController@show')->name('paises.show')
        ->middleware('permission:paises.index');

    //roles
    Route::GET('roles', 'RolController@index')->name('roles.index')
        ->middleware('permission:roles.index');
    Route::POST('roles', 'RolController@store')->name('roles.store')
        ->middleware('permission:roles.store ');
    Route::GET('roles/create', 'RolController@create')->name('roles.create')
        ->middleware('permission:roles.create ');
    Route::PUT('roles/{role}', 'RolController@update')->name('roles.update')
        ->middleware('permission:roles.update ');
    Route::GET('roles/{role}', 'RolController@show')->name('roles.show')
        ->middleware('permission:roles.show');
    Route::DELETE('roles/{role}', 'RolController@destroy')->name('roles.destroy')
        ->middleware('permission:roles.destroy');
    Route::GET('roles/{role}/edit', 'RolController@edit')->name('roles.edit')
        ->middleware('permission:roles.edit');
    Route::GET('roles/{role}', 'RolController@show')->name('roles.show')
        ->middleware('permission:roles.show');
    Route::GET('roles/{role}', 'RolController@show')->name('roles.show')
        ->middleware('permission:roles.show');

    //tiposusuario
    Route::GET('tiposusuario', 'TipoUsuarioController@index')->name('tiposusuario.index')
        ->middleware('permission:tiposusuario.index');
    Route::POST('tiposusuario', 'TipoUsuarioController@store')->name('tiposusuario.store')
        ->middleware('permission:tiposusuario.store ');
    Route::GET('tiposusuario/create', 'TipoUsuarioController@create')->name('tiposusuario.create')
        ->middleware('permission:tiposusuario.create ');
    Route::PUT('tiposusuario/{role}', 'TipoUsuarioController@update')->name('tiposusuario.update')
        ->middleware('permission:tiposusuario.update ');
    Route::GET('tiposusuario/{role}', 'TipoUsuarioController@show')->name('tiposusuario.show')
        ->middleware('permission:tiposusuario.show');
    Route::DELETE('tiposusuario/{role}', 'TipoUsuarioController@destroy')->name('tiposusuario.destroy')
        ->middleware('permission:tiposusuario.destroy');
    Route::GET('tiposusuario/{role}/edit', 'TipoUsuarioController@edit')->name('tiposusuario.edit')
        ->middleware('permission:tiposusuario.edit');
    Route::GET('tiposusuario/{role}', 'TipoUsuarioController@show')->name('tiposusuario.show')
        ->middleware('permission:tiposusuario.show');
    Route::GET('tiposusuario/{role}', 'TipoUsuarioController@show')->name('tiposusuario.show')
        ->middleware('permission:tiposusuario.show');

//estado usuarios

    Route::GET('estadosusuario', 'EstadoUsuarioController@index')->name('estadosusuario.index')
        ->middleware('permission:estadosusuario.index');
    Route::POST('estadosusuario', 'EstadoUsuarioController@store')->name('estadosusuario.store')
        ->middleware('permission:estadosusuario.index');
    Route::GET('estadosusuario/create', 'EstadoUsuarioController@create')->name('estadosusuario.create')
        ->middleware('permission:estadosusuario.index');
    Route::post('estadosusuario/{role}', 'EstadoUsuarioController@update')->name('estadosusuario.update')
        ->middleware('permission:estadosusuario.index ');
    Route::GET('estadosusuario/{role}', 'EstadoUsuarioController@show')->name('estadosusuario.show')
        ->middleware('permission:estadosusuario.index');
    Route::DELETE('estadosusuario/{role}', 'EstadoUsuarioController@destroy')->name('estadosusuario.destroy')
        ->middleware('permission:estadosusuario.index');
    Route::GET('estadosusuario/{role}/edit', 'EstadoUsuarioController@edit')->name('estadosusuario.edit')
        ->middleware('permission:estadosusuario.index');
    Route::GET('estadosusuario/{role}', 'EstadoUsuarioController@show')->name('estadosusuario.show')
        ->middleware('permission:estadosusuario.index');
    Route::GET('estadosusuario/active/{role}', 'EstadoUsuarioController@activeUser')->name('estadosusuario.active')->middleware('permission:estadosusuario.active');
    Route::GET('estadosusuario/desactive/{role}', 'EstadoUsuarioController@desactiveUser')->name('estadosusuario.desactive')->middleware('permission:estadosusuario.desactive');    
   //crud    personas
    Route::resource('personas', 'PersonaController')->middleware('permission:personas.index');

// radicados
    Route::GET('radicaciones', 'RadicacionUsuariosController@create')->name('radicaciones.create')
        ->middleware('permission:radicaciones.create');
    Route::POST('radicaciones', 'RadicacionUsuariosController@store')->name('radicaciones.store')
         ->middleware('permission:radicaciones.store');
    Route::GET('radicaciones/{id_radicado}', 'RadicacionUsuariosController@show')->name('radicaciones.show')
    ->middleware('permission:radicaciones.show');  
     Route::GET('radipdf/{id}', 'PdfController@Reporteuno')->name('radipdf.Reporteuno')
    ->middleware('permission:radipdf.Reporteuno');
    Route::GET('radicados/usuarios/asig', 'RadicacionUsuariosController@getRadicados')->name('radicados.getRadicados')
    ->middleware('permission:radicados.getRadicados');
    Route::GET('remitente/personas/asig', 'RadicacionUsuariosController@getRemitente')->name('radicados.getRemitente')
    ->middleware('permission:radicados.getRemitente');
     Route::GET('radicados/asociados/asig', 'RadicacionUsuariosController@getRadicadoAsociado')->name('radicados.getRadicadoAsociado')
    ->middleware('permission:radicados.getRadicadoAsociado');

// formato codigo

     Route::GET('parcod', 'ParaCodigoController@index')->name('parcod.index')
    ->middleware('permission:parcod.index');

    Route::POST('parcod','ParaCodigoController@store')->name('parcod.store')
    ->middleware('permission:parcod.index');



//Route::resource('articles','ArticleController');
    Route::resource('departamentos', 'DepartamentoController')->middleware('permission:departamentos.index');
    Route::resource('ciudades', 'CiudadController')->middleware('permission:departamentos.index');
    Route::resource('tiposidentificacion', 'TipoIdentificacionController')->middleware('permission:tiposidentificacion.index');

    Route::resource('organizaciones', 'OrganizacionController');
    Route::resource('dependencias', 'DependenciaController')->middleware('permission:dependencias.index');
    Route::resource('cargos', 'CargoController');

    Route::resource('rolesusuarios', 'RolUsuarioController');
    Route::resource('tiposaccion', 'TiposAccionController')->middleware('permission:tiposaccion.index');
    Route::resource('sitioradicacion', 'SitioRadicacionController');
    Route::resource('tiposdocumentales', 'TipoDocumentalesController');
    Route::resource('estados', 'EstadosRadicacionController');
    Route::resource('tiporadicacion', 'TipoRadicacionController');
    Route::resource('tiposolicitud', 'TipoSolicitudController');
    Route::resource('tipocourie', 'TipoCourieController');
    
   

 //organizaciones
 
     Route::GET('organizaciones', 'OrganizacionController@index')->name('organizaciones.index')
        ->middleware('permission:organizaciones.index');
    Route::POST('organizaciones', 'OrganizacionController@store')->name('organizaciones.store')
        ->middleware('permission:organizaciones.index');
    Route::GET('organizaciones/create', 'OrganizacionController@create')->name('organizaciones.create')
        ->middleware('permission:organizaciones.index');
    Route::PUT('organizaciones/{organizacione}', 'OrganizacionController@update')->name('organizaciones.update')
        ->middleware('permission:organizaciones.index');
    Route::GET('organizaciones/{organizacione}', 'OrganizacionController@show')->name('organizaciones.show')
        ->middleware('permission:organizaciones.index');
    Route::DELETE('organizaciones/{organizacione}', 'OrganizacionController@destroy')->name('organizaciones.destroy')
        ->middleware('permission:organizaciones.index');
    Route::GET('organizaciones/{organizacione}/edit', 'OrganizacionController@edit')->name('organizaciones.edit')
        ->middleware('permission:organizaciones.index');

        //busqueda de radicados
    Route::GET('radbusqueda', 'BusquedaRadicadosController@index')->name('radbusqueda.index')->middleware('permission:radbusqueda.index');
    Route::GET('radbusqueda.erm','BusquedaRadicadosController@mostrarTabla')->name('radbusqueda.erm')
        ->middleware('permission:radbusqueda.index');

    Route::GET('radbusqueda/{id}', 'BusquedaRadicadosController@show')->name('radbusqueda.show')->middleware('permission:radbusqueda.index'); 
    
    Route::get('documentos/descargar/{type}', 'BusquedaRadicadosController@descargarDocumento');


        //gestion de correspondencia
    Route::GET('correspondencia', 'CorrespondenciaRadicadosController@index')->name('correspondencia.index')->middleware('permission:correspondencia.index');
     Route::GET('correspondencia-admin', 'CorrespondenciaRadicadosController@tablaAdmin')->name('correspondencia.tablaAdmin')->middleware('permission:correspondencia.erm');
    Route::GET('correspondencia.erm','CorrespondenciaRadicadosController@mostrarTabla')->name('correspondencia.erm')->middleware('permission:correspondencia.erm');

    Route::GET('correspondencia.ermadmin','CorrespondenciaRadicadosController@mostrarTablaAdmin')->name('correspondencia.ermadmin')->middleware('permission:correspondencia.erm');
    Route::GET('corresver/{id}', 'CorrespondenciaRadicadosController@show')->name('corebusqueda.show')->middleware('permission:correspondencia.erm');

    Route::GET('corresver/descargar/{id}', 'CorrespondenciaRadicadosController@descargarArchivo')->name('corebusqueda.descargarArchivo')->middleware('permission:corebusqueda.show');

    Route::GET('ejemplo/descargar', 'CorrespondenciaRadicadosController@descargarEjemploArchivo')->name('corebusqueda.descargarArchivo')->middleware('permission:corebusqueda.show');

    Route::GET('coregestion/crear/{id}', 'CorrespondenciaRadicadosController@gestion')->name('gestion.index')->middleware('permission:gestion.index'); 
    Route::POST('coregestion/crear', 'CorrespondenciaRadicadosController@gestionar')->name('gestion.gestionar')->middleware('permission:gestion.index');
    Route::GET('coregestion/ver/{id}', 'CorrespondenciaRadicadosController@showtwo')->name('coregestion.showtwo')->middleware('permission:gestion.index'); 
    Route::GET('coregestion/{id}', 'CorrespondenciaRadicadosController@showmodal')->name('coregestion.modal')->middleware('permission:coregestion.modal');
   
    //perfil
   Route::GET('miperfil', 'perfilController@index')->name('miperfil.index')->middleware('permission:miperfil.index');
   Route::POST('miperfil/update', 'perfilController@update')->name('miperfil.cambiar')->middleware('permission:miperfil.index');

   //documentos 


   Route::GET('documentos/cargar', 'documentosController@cargar')->name('documento.cargar')->middleware('permission:miperfil.index');
   Route::POST('documentos/upload', 'documentosController@upload');
   Route::get('documentos/consulta', 'documentosController@consultaDB');

   //configuracion del sistema

   Route::GET('conf/general','ConfSistemaController@conf')->name('configuracion.sistema')->middleware('permission:configuracion.sistema');
   Route::post('actualizar/general','ConfSistemaController@actualizarConf')->name('actualizar.sistema')->middleware('permission:configuracion.sistema');
});


Route::GET('radiperso', 'RadicacionUsuariosController@emerpersona')->name('radicaciones.vistauno');
Route::GET('radidesti', 'RadicacionUsuariosController@emerdesti')->name('radicaciones.emerdesti');
Route::POST('radicacionesperson', 'RadicacionUsuariosController@storepersona')->name('radicaciones.storepersona');
Route::POST('radicacionesdesti', 'RadicacionUsuariosController@storedesti')->name('radicaciones.storedesti');

Route::GET('dia','fechaController@consultaEstado');

