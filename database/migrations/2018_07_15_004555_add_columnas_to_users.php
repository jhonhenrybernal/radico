<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->integer('UsEstado')->unsigned();
            $table->foreign('UsEstado')->references('id')->on('estadosusuario')->onDelete('no action')->onUpdate('no action');
            $table->integer('UsTipoIdentificacion')->unsigned();
            $table->foreign('UsTipoIdentificacion')->references('id')->on('tiposidentificacion')->onDelete('no action')->onUpdate('no action');
            $table->integer('UsTipo')->unsigned();
            $table->foreign('UsTipo')->references('id')->on('tiposusuario')->onDelete('no action')->onUpdate('no action');
             $table->integer('UsCargos')->unsigned();
            $table->foreign('UsCargos')->references('id')->on('cargos')->onDelete('no action')->onUpdate('no action');
           
           
            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('tipo_role');
            $table->dropColumn('UsEstado');
            $table->dropColumn('UsTipoIdentificacion');
            $table->dropColumn('UsCargos');
        });
    }
}
