@extends('layout')


@section('content')
 <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>tipo de solicitud</h2>
                
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tiposolicitud.create') }}">Generar nuevo tipo</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive" style="overflow: auto">
        <table class="table table-bordered">
            <tr class="thead-inverse" style="font-size:16px">
                <th>id</th>
                <th>Tipo</th>
                <th>Tiempo</th>
                <th>Descripcion</th>
                <th>Sigla</th>
               <th width="280px">Operacion</th>
            </tr>
        @foreach ($tsolicitud as $dato)
            <tr style="font-size:14px">
               <td>{{ $dato-> id}}</td>
                <td>{{ $dato-> TsTipoSolicitud}}</td>
                 <td>{{ $dato-> TsTiempoSolicitud}}</td>
                  <td>{{ $dato-> descripcion}}</td>
                   <td>{{ $dato-> TsSigla}}</td>
                <td>
                   
                    <a class="btn btn-primary btn-sm" href="{{ route('tiposolicitud.edit',$dato->id) }}">Editar</a>
   {!! Form::open(['method' => 'DELETE','route' => ['tiposolicitud.destroy', $dato->id],'style'=>'display:inline']) !!}
   {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    </div>

@endsection