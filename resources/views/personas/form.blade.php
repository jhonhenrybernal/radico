<div class="row">
    <table WIDTH="1000">
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Primer Nombre:</strong>
                        {!! Form::text('primer_nombre', null, array('placeholder' => 'Primer Nombre','class' => 'form-control' )) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Segundo Nombre:</strong>
                        {!! Form::text('segundo_nombre', null, array('placeholder' => 'Segundo Nombre','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Primer Apellido:</strong>
                        {!! Form::text('primer_apellido', null, array('placeholder' => 'Primer Apellido','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Segundo Apellido:</strong>
                        {!! Form::text('segundo_apellido', null, array('placeholder' => 'Segundo Apellido','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table WIDTH="1000">
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tipo Identificacion:</strong>
                       {!! Form::select('tipoidentificacion_id', array_pluck($tipoidentificacion, 'nombre', 'id')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Identificacion:</strong>
                        {!! Form::text('identificacion', null, array('placeholder' => 'Identificacion','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefono Fijo:</strong>
                        {!! Form::text('telefono_fijo', null, array('placeholder' => 'Telefono Fijo','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Telefono Movil:</strong>
                   <!--      <input type="text" name="telefono_movil" size="12" placeholder='Telefono Movil' class="form-control"> -->
                            {!! Form::text('telefono_movil', null, array('placeholder' => 'Telefono Movil','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>            
        </tr>
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
   <!--                      <input type="email" name="email" placeholder='Email' class="form-control"> -->
                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Direccion:</strong>
              <!--           <input type="text" name="direccion" placeholder='Direccion' class="form-control"> -->
                        {!! Form::text('direccion', null, array('placeholder' => 'Direccion','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table width="1000">
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Pais:</strong>
                       {!! Form::select('pais_id', array_pluck($pais, 'painombre', 'id')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Departamento:</strong>
                       {!! Form::select('departamento_id', array_pluck($departamento, 'dtonombre', 'id')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ciudad:</strong>
                       {!! Form::select('ciudad_id', array_pluck($ciudad, 'nombre', 'id')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Ocupacion o Cargo:</strong>
                        <!-- <input type="text" name="ocupacion" placeholder='Ocupacion o Cargo' class="form-control"> -->
                        {!! Form::text('ocupacion', null, array('placeholder' => 'Ocupacion o Cargo','class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>