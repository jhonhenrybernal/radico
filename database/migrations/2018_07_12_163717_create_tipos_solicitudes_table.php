<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('TsTipoSolicitud',100);
            $table->integer('TsTiempoSolicitud');
            $table->string('descripcion',100);
            $table->string('TsSigla',3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tipos_solicitudes');
    }
}
