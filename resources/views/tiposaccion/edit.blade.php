@extends('layout')


@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar tipo de acción</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiposaccion.index') }}"> Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> No se pudo realizar.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($tipoaccion, ['method' => 'PATCH','route' => ['tiposaccion.update', $tipoaccion->id]]) !!}
        @include('tiposaccion.form')
    {!! Form::close() !!}

@endsection
