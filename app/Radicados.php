<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\TiposRadicados;
use App\SitioRadicado;
use App\RadicadosAsociado;
use App\TiposCourier;
use App\TiposSolicitudes;
use App\EstadosRadicados;
use App\Persona;
use App\Archivosdoc;
use App\Estasdosgestion;
use App\Gestiones;
use App\status;


class Radicados extends Model
{
   protected $table = 'radicados';
   public $timestamps = false;
   protected $fillable = [
    'id',
   	'RaNumero',
   	'RaUsuarioAsignar',
   	'RaNoGuia',
   	'RaNoElementos',
   	'RaAsunto',
   	'RaObservaciones',
   	'RaFechaHoraEsperadaTramite',
   	'RaUsuario',
   	'RaEstado',
   	'RaTipo',
   	'RaSitio',
   	'RaTipoDocumental',
   	'RaIdReferencia',
   	'RaCourier',
   	'RaTiposSolicitud',
    'RaDesPersona',
    'RaGestionId',
    'RaFechaVencimiento',
    'RaFechaVencimiento2',
    'RaTipoTiempo',
    'created_at',
    'RaTipoTiempoNnombre'
   	];


public function usuario()
   {
       return $this->belongsTo(User::class,'RaUsuarioAsignar','id','name','lastname' );
    
   }
   

public function usuariodos()
   {
       return $this->belongsTo(User::class,'RaUsuario','id','name' );
    
   }   

 
  


public function estado()
   {
       return $this->belongsTo(EstadosRadicados::class,'RaEstado','id','ErDescripcion' );
    
   }   

 

public function tiporadicacion()
   {
       return $this->belongsTo(TiposRadicados::class,'RaTipo' ,'id','TrDescripcion' );
 
   }   

public function sitioradicacion()
   {
       return $this->belongsTo(SitioRadicado::class,'RaSitio','id','SrDescripcion');
 
   }  

public function radicadoasociado()
   {
       return $this->belongsTo(RadicadosAsociado::class,'RaIdReferencia','id','RasRadcadoAsociado');
 
   }  

public function tipocurrie()
   {
       return $this->belongsTo(TiposCourier::class,'RaCourier','id','TcoDescripcion');
 
   }    

public function tipossolicitudes()
   {
       return $this->belongsTo(TiposSolicitudes::class,'RaTiposSolicitud','id','TsTipoSolicitud','TsTiempoSolicitud');
 
   }   

public function destinatario()
   {
        return $this->belongsTo(Persona::class,'RaDesPersona','id','primer_nombre');
 
   }     


   public function archivoslista()
   {
       return $this->belongsTo(Archivosdoc::class,'id','ArRadicado','archivos');
 
   }      

     public function gestionlista()
   {
       return $this->belongsTo(Estasdosgestion::class,'RaGestionId','id','EgNombreEstado');
 
   }

    public function codgestion()
   {
       return $this->belongsTo(Gestiones::class,'id','Radicados_RaId' );
    
   }

      public function status()
   {
       return $this->belongsTo(status::class,'RaTipoTiempo','id','datotres');
    
   }
   
}



