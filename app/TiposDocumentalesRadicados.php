<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposDocumentalesRadicados extends Model
{
   protected $table = 'tipos_documentales_radicados';
   protected $fillable = [
   	'TdrDescripcion',
   	'TdrSigla',
   ];
}

