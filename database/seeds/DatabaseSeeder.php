<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(EstadosRadicadosTableSeeder::class);
         $this->call(EstasdosgestionTableSeeder::class);
         $this->call(PermissionsTableSeeder::class);
         $this->call(tiposCurrierTableSeeder::class);
         $this->call(TiposIdentificacionTableSeeder::class);
         $this->call(tiposRadicadosTableSeeder::class);
         $this->call(TiposSolicitudesTableSeeder::class);
         $this->call(permission_roleTableSeeder::class);
      
    }
}
