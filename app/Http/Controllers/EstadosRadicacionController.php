<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EstadosRadicados;

class EstadosRadicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $estadorad  = EstadosRadicados::all();
       return view('estadoradicacion.index',compact('estadorad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estadorad = EstadosRadicados::all();
        return view('estadoradicacion.create', compact('estadorad'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
        'ErDescripcion' => 'required',
        
        ]);
        EstadosRadicados::create($request->all());
        return redirect()->route('estados.index')
                        ->with('success','Estado creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     EstadosRadicados::find($id)->delete();
        return redirect()->route('estados.index')
                        ->with('success','Eliminado');
    }
}
