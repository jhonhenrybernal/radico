<?php

use Illuminate\Database\Seeder;
use App\TiposSecuenciaRadicacion;

class TiposSecuenciaRadicacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         TiposSecuenciaRadicacion::create([
            'id'   => '1',
            'secuencia' => 'Sitio-Tipo radicacion-Tipo Documental-Tipo solicitud-numero consecutivo-Año',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '2',
            'secuencia' => 'Tipo radicacion-Sitio-Tipo Documental-Tipo solicitud-numero consecutivo-Año',
         ]);
           TiposSecuenciaRadicacion::create([
            'id'   => '3',
            'secuencia' => 'Tipo radicacion-Tipo Documental-Sitio-Tipo solicitud-numero consecutivo-Año',
         ]);
            TiposSecuenciaRadicacion::create([
            'id'   => '5',
            'secuencia' => 'Tipo radicacion-Tipo Documental-Tipo solicitud-Sitio-numero consecutivo-Año',
         ]);
             TiposSecuenciaRadicacion::create([
            'id'   => '6',
            'secuencia' => 'Tipo radicacion-Tipo Documental-Tipo solicitud-numero consecutivo-Sitio-Año',
         ]);
              TiposSecuenciaRadicacion::create([
            'id'   => '7',
            'secuencia' => 'Tipo radicacion-Tipo Documental-Tipo solicitud-numero consecutivo-Año-Sitio',
         ]);
               TiposSecuenciaRadicacion::create([
            'id'   => '8',
            'secuencia' => 'Tipo Documental-Tipo solicitud-Tipo radicacion-numero consecutivo-Año-Sitio',
         ]);
                TiposSecuenciaRadicacion::create([
            'id'   => '9',
            'secuencia' => 'Tipo Documental-Tipo solicitud-numero consecutivo-Tipo radicacion-Año-Sitio',
         ]);
                 TiposSecuenciaRadicacion::create([
            'id'   => '10',
            'secuencia' => 'Tipo Documental-Tipo solicitud-numero consecutivo-Año-Tipo radicacion-Sitio',
         ]);
                  TiposSecuenciaRadicacion::create([
            'id'   => '11',
            'secuencia' => 'Tipo Documental-Tipo solicitud-numero consecutivo-Año-Sitio-Tipo radicacion',
         ]);
                  TiposSecuenciaRadicacion::create([
            'id'   => '12',
            'secuencia' => 'Tipo solicitud-Tipo Documental-numero consecutivo-Año-Sitio-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '13',
            'secuencia' => 'Tipo solicitud-numero consecutivo-Tipo Documental-Año-Sitio-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '14',
            'secuencia' => 'Tipo solicitud-numero consecutivo-Año-Tipo Documental-Sitio-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '15',
            'secuencia' => 'Tipo solicitud-numero consecutivo-Año-Sitio-Tipo Documental-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '16',
            'secuencia' => 'Tipo solicitud-numero consecutivo-Año-Sitio-Tipo radicacion-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '17',
            'secuencia' => 'numero consecutivo-Tipo solicitud-Año-Sitio-Tipo radicacion-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '18',
            'secuencia' => 'numero consecutivo-Año-Tipo solicitud-Sitio-Tipo radicacion-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '19',
            'secuencia' => 'numero consecutivo-Año-Sitio-Tipo solicitud-Tipo radicacion-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '20',
            'secuencia' => 'numero consecutivo-Año-Sitio-Tipo radicacion-Tipo solicitud-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '21',
            'secuencia' => 'numero consecutivo-Año-Sitio-Tipo solicitud-Tipo radicacion-Tipo Documental',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '22',
            'secuencia' => 'numero consecutivo-Año-Sitio-Tipo solicitud-Tipo Documental-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '23',
            'secuencia' => 'Año-numero consecutivo-Sitio-Tipo solicitud-Tipo Documental-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '24',
            'secuencia' => 'Año-Sitio-numero consecutivo-Tipo solicitud-Tipo Documental-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '25',
            'secuencia' => 'Año-Sitio-Tipo solicitud-numero consecutivo-Tipo Documental-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '26',
            'secuencia' => 'Año-Sitio-Tipo solicitud-Tipo Documental-numero consecutivo-Tipo radicacion',
         ]);
          TiposSecuenciaRadicacion::create([
            'id'   => '27',
            'secuencia' => 'Año-Sitio-Tipo solicitud-Tipo Documental-Tipo radicacion-numero consecutivo',
         ]); 
    }
}
