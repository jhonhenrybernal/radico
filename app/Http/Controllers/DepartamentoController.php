<?php

namespace App\Http\Controllers;
use App\Departamento;
use App\Pais;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises=Pais::all();
        $departamentos = Departamento::latest()->paginate(5);
        return view('departamentos.index',compact('departamentos', 'paises'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Pais::all();
        return view('departamentos.create', compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'dtocodigo' => 'required',
            'dtonombre' => 'required',
            'pais_id' => 'required',
        ]);
        Departamento::create($request->all());
        return redirect()->route('departamentos.index')
                        ->with('success','Departaments created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departamento = Departamento::find($id);
        $pais = $departamento->paises;
        return view('departamentos.show',compact('departamento','pais'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paises = Pais::all();
        $departamento = Departamento::find($id);
        return view('departamentos.edit',compact('departamento','paises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'dtocodigo' => 'required',
            'dtonombre' => 'required',
            'pais_id' => 'required',
        ]);
        Departamento::find($id)->update($request->all());
        return redirect()->route('departamentos.index')
                        ->with('success','Departament updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Departamento::find($id)->delete();
        return redirect()->route('departamentos.index')
                        ->with('success','Departament deleted successfully');
    }
}
