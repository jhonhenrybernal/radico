<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadicadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radicados', function (Blueprint $table) {
            $table->increments('RaId');
            $table->string('RaNumero',30);
            $table->string('RaFechaHora');
            $table->string('RaNoGuia');
            $table->string('RaNoElementos');
            $table->string('RaAsunto');
            $table->string('RaObservaciones');
            $table->string('RaFechaHoraEsperadaTramite');
            $table->integer('Archivos_Id');
            $table->integer('RaUsuario');
            $table->integer('RaEstado');
            $table->integer('RaTipo');
            $table->integer('RaSitio');
            $table->integer('RaTipoDocumental');
            $table->integer('RaIdReferencia');
            $table->integer('RaCourier');
            $table->integer('RaTiposSolicitud');
            $table->integer('RaDesPersona');
            $table->string('RaFechaVencimiento'30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_radicados');
    }
}
