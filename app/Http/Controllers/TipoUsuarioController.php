<?php

namespace App\Http\Controllers;
use App\TipoUsuario;
use Illuminate\Http\Request;

class TipoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposusuario = TipoUsuario::latest()->paginate(5);
        return view('tiposusuario.index',compact('tiposusuario'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposusuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        TipoUsuario::create($request->all());
        return redirect()->route('tiposusuario.index')
                        ->with('success','Type User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipousuario = TipoUsuario::find($id);
        return view('tiposusuario.show',compact('tipousuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipousuario = TipoUsuario::find($id);
        return view('tiposusuario.edit',compact('tipousuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        TipoUsuario::find($id)->update($request->all());
        return redirect()->route('tiposusuario.index')
                        ->with('success','Type User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TipoUsuario::find($id)->delete();
        return redirect()->route('tiposusuario.index')
                        ->with('success','Type User deleted successfully');
    }
}
