<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/avatar-default-thumb.jpg" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <p>{{ Auth::user()->role['name'] }}</p>
                    <small> {{ Auth::user()->roleusers['name'] }} </small>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                    <p>{{ Auth::user()->role['name'] }}</p>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU NAVEGACION</li>
            @can('correspondencia')
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt" ></i> <span>{{ trans('message.CorrespondenceManagement') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('correspondencia.index')}}"><i class="fa fa-circle-o"></i> {{ trans('message.CorrespondenceManagement') }}</a></li>
                </ul>
            </li>
            @endcan
            @can('correspondenciaadmin')
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt" ></i> <span>{{ trans('message.CorrespondenceManagement') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('correspondencia.tablaAdmin')}}"><i class="fa fa-circle-o"></i> {{ trans('message.CorrespondenceManagement') }}</a></li>
                </ul>
            </li>
            @endcan

            @can('radicacion')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-barcode"></i> <span>{{ trans('message.Radication') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('radicaciones.create')}}"><i class="fa fa-align-justify"></i> {{ trans('message.Management_of_radication') }}</a></li>
                    <li><a href="{{ route('documento.cargar')}}"><i class="fa fa-upload"></i> Cargar documentos</a></li>
                    <li><a href="{{ route('radbusqueda.index')}}"><i class="fa fa-search"></i> {{ trans('message.Search_for_residents') }}</a></li>
                    <li><a href="personas"><i class="fa fa-male"></i> {{ trans('message.People') }}</a></li>
                </ul>
            </li>
            @endcan

            @can('usuarios')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>{{ trans('message.Users') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @can('estadosusuario.create')
                    <li><a href="{{ route('estadosusuario.index')}}"><i class="fa fa-plus-circle"></i>Gestionar usuarios</a></li>
                    @endcan
                   @can('roles.index')
                    <li><a href="{{ route('roles.index')}}"><i class="fa fa-female"></i> {{ trans('message.Roles') }}</a></li>
                    @endcan
                    
                </ul>
            </li>
            @endcan


            @can('parageneral')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>{{ trans('message.General_Parameterization') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('organizaciones.index')}}"><i class="fa fa-building"></i>Organizaciones</a></li>
                    <li><a href="{{ route('dependencias.index')}}"><i class="fa fa-sitemap"></i> {{ trans('message.Dependencies') }}</a></li>
                    <li><a href="{{ route('cargos.index')}}"><i class="fa fa-tasks"></i> {{ trans('message.Charges') }}</a></li>
                    <li><a href="paises"><i class="fa fa-globe"></i> {{ trans('message.Countries') }}</a></li>
                    <li><a href="{{ route('departamentos.index')}}"><i class="fa fa-map-marker"></i> {{ trans('message.Departments') }}</a></li>
                    <li><a href="ciudades"><i class="fa fa-home"></i> {{ trans('message.Cities') }}</a></li>
                    <li><a href="{{ route('tiposidentificacion.index')}}"><i class="fa  fa-child"></i> {{ trans('message.Identification_Types') }}</a></li>
                    <!--<li><a href="{{ route('tiposaccion.index')}}"><i class="fa fa-id-card-o"></i> {{ trans('message.Action_Types') }}</a></li>-->
                    @can('parageneralConfi')
                    <li><a href="{{ route('configuracion.sistema')}}"><i class="fa fa-wrench"></i> Configuracion del sistema</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('pararadicacion')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Parametrizar radicaciones</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('sitioradicacion.index')}}"><i class="fa fa-university"></i>Sitios de radicacion</a></li>
                    <li><a href="{{ route('tiposdocumentales.index')}}"><i class="fa fa-file"></i> {{ trans('message.Documentary_type') }}</a></li>
                    <li><a href="{{ route('estados.index')}}"><i class="fa  fa-child"></i>Estado Radicacion</a></li>
                    <li><a href="{{ route('tiporadicacion.index')}}"><i class="fa fa-navicon"></i>Tipo Radicacion</a></li>
                    <li><a href="{{ route('tiposolicitud.index')}}"><i class="fa fa-tag"></i>Tipo solicitud</a></li>
                    <li><a href="{{ route('tipocourie.index')}}"><i class="fa fa-road"></i>Tipo Courier</a></li>
                    <li><a href="{{ route('parcod.index')}}"><i class="fa fa-gears"></i>Formato codigo</a></li>
                </ul>
            </li>
            @endcan
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>{{ trans('message.My_user') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('miperfil.index') }}"><i class="fa fa-edit"></i> {{ trans('message.Update_data') }}</a></li>
               </ul>
            </li>


        </ul>

            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
