<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {!! Form::text('nombre', null, array('placeholder' => 'Nombre','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tipo Identificacion:</strong>
                {!! Form::select('tipoidentificacion_id', array_pluck($tiposidentificacion, 'tipo', 'id','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Identificacion:</strong>
            {!! Form::text('identificacion', null, array('placeholder' => 'Identificacion','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Telefono:</strong>
           {!! Form::text('telefono', null, array('placeholder' => 'Telefono','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Direccion:</strong>
           {!! Form::text('direccion', null, array('placeholder' => 'Direccion','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>URL:</strong>
           {!! Form::text('url', null, array('placeholder' => 'URL','class' => 'form-control','required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">crear</button>
    </div>
</div>