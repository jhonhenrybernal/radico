<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposRadicados extends Model
{
    protected $table = 'tipos_radicados';
    public $timestamps = false;
    protected $fillable = ['id','TrDescripcion', 'TrFuncionNumeracion','TrSigla',
        ];
}
