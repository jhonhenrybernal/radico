<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('active')->unsigned();
            $table->rememberToken();
            $table->timestamps();
            $table->string('UsIdentificacion');
            $table->string('UsTelefono',40);
            $table->string('UsCelular',40);
            $table->string('UsUltimoLogin');
            $table->integer('UsIntentos');
            $table->string('UsUltimaIP',20);
            $table->string('UsFechaHoraCambioPW');
            $table->string('UsRol');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
