<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosRadicados extends Model
{
    protected $table = 'estados_radicados';
    public $timestamps = false;
    protected $fillable = ['id', 'ErDescripcion'
        ];
}
