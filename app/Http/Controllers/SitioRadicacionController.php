<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SitioRadicado;

class SitioRadicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sradicado = SitioRadicado::all();
        return view('sitioradicacion.index',compact('sradicado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $sradicado = SitioRadicado::all();
        return view('sitioradicacion.create', compact('sradicado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          request()->validate([
        'SrDescripcion' => 'required',
        'SrSecuenciaNoRad'=> 'unique:personas,identificacion',
        'SrFuncionNumeracion'=> 'unique:personas,identificacion',  
        ]);
        SitioRadicado::create([
            'SrDescripcion' => request()->SrDescripcion,
            'SrSecuenciaNoRad' => request()->SrSecuenciaNoRad,
            'SrFuncionNumeracion' => request()->SrFuncionNumeracion,
            'SrSigla' => strtoupper(request()->SrSigla),
        ]);
        return redirect()->route('sitioradicacion.index')
                        ->with('success','Sitio creado exitosamente');
    }

    
    public function edit($id)
    {
        $sradicado = SitioRadicado::find($id);
        return view('sitioradicacion.edit',compact('sradicado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          request()->validate([
            'SrDescripcion' => 'required',
            'SrSecuenciaNoRad'=> 'unique:personas,identificacion',
            'SrFuncionNumeracion'=> 'unique:personas,identificacion',  
        ],[
            'SrDescripcion.required' => 'El campo Descripcion es obligatorio',
            'SrSecuenciaNoRad.required' => 'Secuencia numero radicacion',
            'SrFuncionNumeracion.required' => 'Funcion numeracion',
         ]);
   
        SitioRadicado::find($id)->update($request->all());
        return redirect()->route('sitioradicacion.index')
                        ->with('success','Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SitioRadicado::find($id)->delete();
        return redirect()->route('sitioradicacion.index')
                        ->with('success','Eliminado');
    }
}
