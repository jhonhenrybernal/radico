@extends('layout')


@section('content')

<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Parametrizacion para secuencia de radicacion</h3>
            </div>
          {!! Form::open(array('route' => 'parcod.store','method'=>'POST')) !!}  
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                <label>Año</label>
                  <input type="number" class="form-control" placeholder="" name="SecrAño" style="width:80px">
                </div>
                <div class="form-group">
               <label>Parametrizacion</label>
                 <select class="form-control" name="SecrNumero"> 
                     <option>Seleccione</option>
                     @foreach ($secod as $dato)
                     <option value="{{ $dato['id'] }}">{{ $dato['secuencia'] }}</option>
                      @endforeach
                 </select>
                </div>
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Procesar</button>
              </div>
             {!! Form::close() !!}
            </form>
          </div>
       </div>
      
      <!-- /.row -->
    </section>
  

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Estado actual</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <th>No</th>
                <th>secuencia</th>
                <th>Año</th>
                </thead>
                <tbody>
                 @foreach ($cod as $valor)
                 <tr role="row" class="odd">
                  <td class="sorting_1">{{ $valor->id}}</td>
                  <td class="sorting_1">{{ $valor->secuenciaradicados->secuencia}}</td>
                  <td>{{ $valor->SecrAño}}</td>
                  @endforeach
                </tr></tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

 
@endsection