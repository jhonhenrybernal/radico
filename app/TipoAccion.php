<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAccion extends Model
{
    protected $table = 'tiposaccion';
    public $timestamps = false;
    protected $fillable = ['tipo', 'nombre',
        ];
}
