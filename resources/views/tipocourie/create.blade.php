@extends('layout')


@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Generar Nuevo Courier</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tipocourie.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>no se pudo generar courie.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'tipocourie.store','method'=>'POST')) !!}
         @include('tipocourie.form')
    {!! Form::close() !!}

@endsection