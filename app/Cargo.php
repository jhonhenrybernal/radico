<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'cargos';
    protected $fillable = ['codigo', 'nombre', 'dependencia_id'
        ];

    /**
     * A departamento can have many comments
     */
	public function dependencias()
	{
	    return $this->hasMany(Dependencia::class);
	    // return $this->hasMany('App\Departamento');
	}
}
