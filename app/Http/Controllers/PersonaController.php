<?php
 
namespace App\Http\Controllers;
use App\TipoIdentificacion;
use App\Pais;
use App\Departamento;
use App\Ciudad;
use App\Persona;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises = Pais::all();
        $departamentos = Departamento::all();
        $ciudades = Ciudad::all();
        $tiposidentificacion = TipoIdentificacion::all();
        $personas = Persona::latest()->paginate(5);
        return view('personas.index',compact('personas', 'paises', 'departamentos', 'ciudades', 'tiposidentificacion'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pais = Pais::all();
        $departamento = Departamento::all();
        $ciudad = Ciudad::all();
        $tipoidentificacion = TipoIdentificacion::all();
        return view('personas.create', compact('pais', 'departamento', 'ciudad', 'tipoidentificacion' ));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
        'primer_nombre' => 'required',
        'identificacion'=> 'unique:personas,identificacion', 
        ]);
        Persona::create($request->all());
        return redirect()->route('personas.index')
                        ->with('success','Persona creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $persona = Persona::find($id);
        $pais = $persona->pais;
        $departamento = $persona->departamento;
        $ciudad = $persona->ciudad;
        $tipoidentificacion = $persona->tipoidentificacion;
        return view('personas.show',compact('persona','pais','departamento','ciudad','tipoidentificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $persona = Persona::find($id);
        $pais = Pais::all();
        $departamento = Departamento::all();
        $ciudad = Ciudad::all();
        $tipoidentificacion = TipoIdentificacion::all();
        return view('personas.edit',compact('persona','pais','departamento','ciudad','tipoidentificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'primer_nombre' => 'required',
            // 'identificacion'=> 'unique:personas,identificacion', 
        ]);
        Persona::find($id)->update($request->all());
        return redirect()->route('personas.index')
                        ->with('success','Persona actualizadas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Persona::find($id)->delete();
        return redirect()->route('personas.index')
                        ->with('success','Persona eliminada');
    }
}
