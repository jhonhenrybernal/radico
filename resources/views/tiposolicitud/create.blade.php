@extends('layout')


@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Generar Nuevo tipo de solicitud</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiposolicitud.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>no se pudo generar tipo.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'tiposolicitud.store','method'=>'POST')) !!}
         @include('tiposolicitud.form')
    {!! Form::close() !!}

@endsection