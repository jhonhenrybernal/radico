<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function ctlCodigo()
    {
    	return CodeController;
    }

    public function semaforo()
    {
    	return fechaController;
    }

    public function radicaciones()
    {
    	return new RadicacionUsuariosController;
    }

    public function ConfSistema()
    {
        return new ConfSistemaController;
    }

}
