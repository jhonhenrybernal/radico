<?php

namespace App\Http\Controllers;
use App\Cargo;
use App\Dependencia;
use Illuminate\Http\Request;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependencias=Dependencia::all();
        $cargos = Cargo::latest()->paginate(5);
        return view('cargos.index',compact('cargos', 'dependencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dependencias=Dependencia::all();
        return view('cargos.create', compact('dependencias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'dependencia_id'=> 'required',
        ]);
        Cargo::create($request->all());
        return redirect()->route('cargos.index')
                                ->with('success','Charge created successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cargo = Cargo::find($id);
        $dependencia = $cargo-> dependencia;
        return view('cargos.show',compact('cargo','dependencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dependencias = Dependencia::all();
        $cargo = Cargo::find($id);
        return view('cargos.edit',compact('dependencias','cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'dependencia_id'=> 'required',
            ]);
            Cargo::find($id)->update($request->all());
            return redirect()->route('cargos.index')
                                ->with('success','Charge updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cargo::find($id)->delete();
        return redirect()->route('cargos.index')
                        ->with('success','Charge deleted successfully');
    }
}
