<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Advanced form elements</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <aside class="main-sidebar">
  @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

      @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
{!! Form::open(array('route' => 'radicaciones.storepersona','method'=>'POST')) !!}
            {{ csrf_field()}}  {{ method_field('POST')}}
      <div class="modal-body">
       <div class="row">
                <input type="hidden" name="id" id="id">
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Primer Nombre</label>
                  <input type="text" class="form-control" id="primer_nombre" name="primer_nombre" value="{{ old('primer_nombre') }}">
                </div>
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Segundo Nombre</label>
                  <input type="text" class="form-control" id="segundo_nombre" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                </div>
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Primer Apellido</label>
                  <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ old('primer_apellido') }}">
                </div>
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Segundo Apellido</label>
                  <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                </div>
              </div>
               <div class="row">
                <div class="col-xs-5">
               <label for="recipient-name" class="col-form-label">Tipo Identificacion</label>
              <select name='tipoidentificacion_id' id='tipoidentificacion_id' class="form-control" value="{{ old('tipoidentificacion_id') }}">
              <option value="">Seleccione por favor</option>
              @foreach ($tipoidentificacion as $tipo)
              <option value="{{ $tipo['id'] }}">{{ $tipo['nombre'] }}</option>
              @endforeach
            </select>
                </div>
               <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Direccion</label>
                  <input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion') }}">
             </div>
             </div>
             <div class="row">
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Identificacion</label>
                  <input type="text" class="form-control" id="identificacion" name="identificacion" value="{{ old('identificacion') }}">
                </div>
                <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Pais</label>
                    <select name='pais_id' id='pais_id' class="form-control" value="{{ old('pais_id') }}">
                    <option value="">Seleccione por favor</option>
                    @foreach ($pais as $tipo)
                    <option value="{{ $tipo['id'] }}">{{ $tipo['painombre'] }}</option>
                    @endforeach
                  </select>
             </div>
              <div class="col-xs-3">
                   <label for="recipient-name" class="col-form-label">Departamento</label>
                    <select name='departamento_id' id='departamento_id' class="form-control" value="{{ old('departamento_id') }}">
                    <option value="">Seleccione por favor</option>
                    @foreach ($departamento as $tipo)
                    <option value="{{ $tipo['id'] }}">{{ $tipo['dtonombre'] }}</option>
                    @endforeach
                  </select>
             </div>
              <div class="col-xs-3">
                 <label for="recipient-name" class="col-form-label">Ciudad</label>
                    <select name='ciudad_id' id='ciudad_id' class="form-control" value="{{ old('ciudad_id') }}">
                    <option value="">Seleccione por favor</option>
                    @foreach ($ciudad as $tipo)
                    <option value="{{ $tipo['id'] }}">{{ $tipo['nombre'] }}</option>
                    @endforeach
                  </select>
             </div>

         </div>
              <div class="row">
              <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Ocupacion o Cargo</label>
                  <input type="text" class="form-control" id="ocupacion" name="ocupacion" value="{{ old('primer_nombre') }}">
             </div>
              <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Telefono Fijo</label>
                  <input type="text" class="form-control" id="telefono_fijo" name="telefono_fijo" value="{{ old('primer_nombre') }}">
             </div>
             <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Telefono Movil</label>
                  <input type="text" class="form-control" id="telefono_movil" name="telefono_movil" value="{{ old('primer_nombre') }}">
             </div>
             <div class="col-xs-3">
                  <label for="recipient-name" class="col-form-label">Email</label>
                  <input type="text" class="form-control" id="email" name="email" value="{{ old('primer_nombre') }}">
             </div>
             </div>
        </div>
      <div class="modal-footer">
        <input class="btn btn-secondary" name="button" type="button" onclick="window.close();" value="Cerrar" />
        <button type="submit" class="btn btn-primary btn-save" >Generar solicitud</button>
      </div>
       </div>
       {!! Form::close() !!}
</aside>
 </div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
