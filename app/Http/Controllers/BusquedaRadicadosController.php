<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Radicados;
use Yajra\DataTables\Datatables;
use Excel;
use App\imp_exp_doc;
use Auth;
use App\trazabilidad; 

class BusquedaRadicadosController extends Controller
{
    public function index(){

    	return view('radicaciones.tablas.busquedaradicado'); 
    }

     public function mostrarTabla()
    {   	   
        $Radicados = Radicados::with('usuario','estado','tiporadicacion','sitioradicacion','radicadoasociado','tipocurrie','tipossolicitudes','tipossolicitudes','destinatario')->select('Radicados.*');
		
		return Datatables::of($Radicados)->addColumn('action', function($Radicados)
			{
		 
		 return	'<a type="button" class="btn btn-info" href="/radbusqueda/'.$Radicados->id.'">Mas detalles</a>';

		   })->make(true);
	}

	public function show($id)
    {
        $radicado = Radicados::find($id);
        $usuario = $radicado->usuario;
        $estado = $radicado->estado;
        $tiporadicacion = $radicado->tiporadicacion;
        $sitioradicacion = $radicado->sitioradicacion;
        $radicadoasociado = $radicado->radicadoasociado;
        $tipocurrie = $radicado->tipocurrie;
        $tipossolicitudes = $radicado->tipossolicitudes;
        $usuariodos = $radicado->usuariodos;
        $radicadoprincipal = Radicados::all();

        $aRadiHistorial = trazabilidad::
        join('users as UsId', 'trazabilidad.Usuarios_UsId','=','UsId.id')
        ->join('users as asig','trazabilidad.Usuario_asig_id','=','asig.id')
        ->join('estasdosgestion','estasdosgestion.id','=','trazabilidad.status_gestion')
        ->join('radicados','radicados.id','=','trazabilidad.Radicados_RaId')
        ->select('UsId.name','UsId.lastname','trazabilidad.fecha_gestion','estasdosgestion.EgNombreEstado','radicados.RaNumero','trazabilidad.observaciones','asig.name as asig_name')
        ->where('Radicados_RaId',$id)->get();

        $historial = (empty($aRadiHistorial))? []: $aRadiHistorial;
        $radicado_principal = (!empty($Radicadosasociado['RasRadcadoAsociado']))?Radicados::find($Radicadosasociado['RasRadcadoAsociado'])->first():'No tiene asociado';
        (!empty($radicado_principal['RaNumero']))?$radicado_principal['RaNumero']:'';

        return view('estadoradicacion.show', compact('radicado', 'usuario', 'tiporadicacion', 'sitioradicacion', 'radicadoasociado', 'tipocurrie', 'tipossolicitudes', 'radicadoprincipal', 'estado', 'usuariodos','historial','radicado_principal'));
    }

    public function descargarDocumento($type){

        $Radicados = Radicados::with('usuario','estado','tiporadicacion','sitioradicacion','radicadoasociado','tipocurrie','tipossolicitudes','tipossolicitudes','destinatario')->get();
   
        foreach ($Radicados as $val) {
            $data[] = [
                'Numero de radicado' => $val['RaNumero'],
                'Quien se asigna' => $val->usuario['name'],
                'Quien lo realiza' => $val->usuariodos['name'],
                'Asunto' => $val['RaAsunto'],
                'Observaciones' => $val['RaObservaciones'],
                'Fecha de creacion' => $val['created_at'],
                'Estado radicacion' => $val->estado['ErDescripcion'],
                'Tipo radicaciones' => $val->tiporadicacion['TrDescripcion'],
                'Sitio radicacion' => $val->sitioradicacion['SrDescripcion'],
                'Tipo courrie' => $val->tipocurrie['TcoDescripcion'],
                'Destinatario' => $val->destinatario['primer_nombre'],
                'Fecha de vencimiento'=> $val['RaFechaVencimiento2']
            ]; 
        }

            $img = new imp_exp_doc();
            $img->id_usuario = Auth::user()->id;
            $img->create_at = date('Y-m-d');
            $img->nombre_documento = 'export_radicado';
            $img->tipo = 'export';
            $img->save();    
        return Excel::create('export_radicado', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
}
