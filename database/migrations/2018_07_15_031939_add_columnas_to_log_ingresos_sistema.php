<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToLogIngresosSistema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('log_ingresos_sistema', function (Blueprint $table)
         {
            $table->integer('LiUsuario')->unsigned();
            $table->foreign('LiUsuario')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
           
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('log_ingresos_sistema', function (Blueprint $table) {

            $table->dropColumn('LiUsuario');
         
            
           
        });
    }
}
