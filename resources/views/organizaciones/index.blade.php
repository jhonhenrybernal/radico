@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Organizaciones</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('organizaciones.create') }}"> Crear nueva organizacion</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>Tipo Identificacion</th>
            <th>Identificacion</th>
            <th>Telefono</th>
            <th>Direccion</th>
            <th>URL</th>
            <th width="280px">Operacion</th>
        </tr>
        @foreach ($organizaciones as $organizacion)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{$organizacion->nombre}}</td>
            <td>
                @foreach ($tiposidentificacion as $tipoidentificacion)
                    @if ($tipoidentificacion->id === $organizacion->tipoidentificacion_id)
                        {{$tipoidentificacion->nombre}}
                    @endif
                @endforeach
            </td>
            <td>{{$organizacion->identificacion}}</td>
            <td>{{$organizacion->telefono}}</td>
            <td>{{$organizacion->direccion}}</td>
            <td>{{$organizacion->url}}</td>
            <td>
                <a class="btn btn-info" href="{{ route('organizaciones.show',$organizacion->id) }}">Ver</a>
                <a class="btn btn-primary" href="{{ route('organizaciones.edit',$organizacion->id) }}">Editar</a>
                {!! Form::open(['method' => 'DELETE','route' => ['organizaciones.destroy', $organizacion->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
    {!! $organizaciones->render() !!}
@endsection