@extends('layout')


@section('content')
 <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Estado radicaciones</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('estados.create') }}">Generar nuevo estado</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive" style="overflow: auto">
        <table class="table table-bordered">
            <tr class="thead-inverse" style="font-size:16px">
                <th>id</th>
                <th>Descripcion</th>
                <th width="280px">Operacion</th>
            </tr>
        @foreach ($estadorad as $dato)
            <tr style="font-size:14px">
                <td>{{ $dato-> id}}</td>
                <td>{{ $dato-> ErDescripcion}}</td>
                 <td>
                  {!! Form::open(['method' => 'DELETE','route' => ['estados.destroy', $dato->id],'style'=>'display:inline']) !!}
   {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    </div>

@endsection