<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogIngresosSistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_ingresos_sistema', function (Blueprint $table) {
            $table->increments('LiId');
            $table->string('LiExitoso');
            $table->string('LiDescripcionProblema');
            $table->string('LiIpIntento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ingresos_sistema');
    }
}
