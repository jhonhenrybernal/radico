@extends('layout')


@section('content')
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h2 >Detalles</h2>
            <div class="col-lg-12 margin-tb">

                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('radbusqueda.index') }}"> Volver</a>
                </div>
            </div>


        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="row">

            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>fecha de radicacion:</strong>
                        <?php echo e($radicado['created_at']); ?>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>fecha de vencimiento:</strong>
                        <?php echo e($radicado['RaFechaVencimiento']); ?>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Asignado a:</strong>
                        {{(empty($usuario['name']))?'Sin asignar': $usuario['name']}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Asunto:</strong>
                        {{$radicado['RaAsunto']}}


                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Observaciones:</strong>
                        {{$radicado['RaObservaciones']}}


                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Usuario quien lo radica:</strong>
                        {{$usuariodos['name']}}  {{$usuariodos['lastname']}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Estado actual:</strong>
                        {{$estado['ErDescripcion']}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tipo de radicacion:</strong>
                        {{$tiporadicacion['TrDescripcion']}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Sitio de radicacion:</strong>
                        {{$sitioradicacion['SrDescripcion']}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tipo de currie:</strong>
                        {{(empty($tipocurrie['TcoDescripcion']))?'':$tipocurrie['TcoDescripcion']}}               
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tipo de solicitud:</strong>
                        {{$radicado['tipossolicitudes']['TsTipoSolicitud']}}               
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Numero de guia:</strong>
                        {{$radicado['RaNoGuia']}}               
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Numero de folios:</strong>
                        {{$radicado['RaNoElementos']}}               
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Numero de radicado: {{ $radicado['RaNumero'] }}</strong>
                        {!! DNS1D::getBarcodeHTML($radicado['RaNumero'], 'C39+') !!}

                    </div>
                </div>

            </div>
            <div class="box-footer">
            </div>
            <div class="box-footer">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a class="btn btn-primary" target="_blank" href="/radipdf/{{ $radicado->id }}">Generar Pdf</a>

                </div>
            </div>
        </div>
         <div class="box">
        <div class="box-header with-border">
              @if(!$historial)
              <h3 class="box-title">Trazabilidad</h3>
              @else
              <h3 class="box-title">Trazabilidad al radicado asociado <dt>Numero:{{$radicado_principal}}</dt></h3> 
              @endif
         </div>
         <div class="box">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Quien lo realiza</th>
                    <th>fecha de gestion </th>
                    <th>Asignado a </th>
                    <th>Estado</th>
                  
                   
                  </tr>
                <tr>
                @foreach($historial as $valor)
                   <tr>
                  <td>{{$valor->name}} {{$valor->lastname}}</td>
                  <td>{{$valor->fecha_gestion}} </td>
                  <td>{{$valor->asig_name}}</td>
                  <td>{{$valor->EgNombreEstado}}</td>
                  
                 <td> <a  class="show-modals btn btn-info btn-sm" data-detalle-gestion='{{$valor->observaciones}}' >Mas detalles</a></td>
                  </tr>
                  @endforeach
              </tr>
              </tbody>
            </table>
           </div>

    </section> 
            <!-- Modal -->
  <div class="modal fade" id="shows" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mas detalles</h4>
        </div>
        <div class="modal-body">
          <p id="text-namegestion"></p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  

    @endsection