@extends('layout')


@section('content')
  @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     @if ($message = Session::get('error'))
        <div class="alert alert-warning">
            <p>{{ $message }}</p>
        </div>
    @endif
<form class="needs-validation" method="post" action="{{route('miperfil.cambiar')}}"  novalidate>
 {{ csrf_field() }}
 <input type="hidden" name="id" value="{{$perfil->id}}">
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationCustom01">Nombre</label>
      <input type="text" class="form-control" id="validationCustom01"  value="{{$perfil->name}}" readonly="readonly">
      <div class="valid-feedback">
    </div>
      <div class="invalid-feedback">
     
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustom02">Apellido</label>
      <input type="text" class="form-control" id="validationCustom02"  value="{{$perfil->lastname}}" readonly="readonly">
      <div class="valid-feedback">

      </div>
      <div class="invalid-feedback">
      
      </div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationCustomUsername">Usuario de red</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend"></span>
        </div>
        <input type="text" class="form-control" id="validationCustomUsername"  value="{{$perfil->username}}"aria-describedby="inputGroupPrepend" readonly="readonly">
        <div class="invalid-feedback">
     
        </div>
      </div>
    </div>
  </div>
  <div class="form-row">
     <div class="col-md-4 mb-3">
      <label for="validationCustomUsername">email</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend"></span>
        </div>
        <input type="text" class="form-control" id="validationCustomUsername"  value="{{$perfil->email}}"aria-describedby="inputGroupPrepend" readonly="readonly">
        <div class="invalid-feedback">
     
        </div>
      </div>
    </div>
     <div class="col-md-3 mb-3">
      <label for="validationCustom04">Cambiar clave</label>
      <input type="password" class="form-control" id="password" name="password" required>
      <div class="invalid-feedback">
  
      </div>
    </div>  
     <div class="col-md-3 mb-3">
      <label for="validationCustom04">Confirmar clave</label>
      <input type="password" class="form-control" id="password" name="password-confir" required>
      <div class="invalid-feedback">
  
      </div>
    </div>   
  </div>
 
  <button class="btn btn-primary" type="submit">Actualizar</button>
</form>
@endsection