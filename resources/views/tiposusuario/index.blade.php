@extends('layout')


@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tipos Usuario</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tiposusuario.create') }}"> Crear nuevo tipo de usuario</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th width="280px">Operacion</th>
        </tr>
    @foreach ($tiposusuario as $tipousuario)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $tipousuario->nombre}}</td>
        <td>{{ $tipousuario->descripcion}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('tiposusuario.show',$tipousuario->id) }}">Ver</a>
            <a class="btn btn-primary" href="{{ route('tiposusuario.edit',$tipousuario->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['tiposusuario.destroy', $tipousuario->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $tiposusuario->render() !!}
@endsection