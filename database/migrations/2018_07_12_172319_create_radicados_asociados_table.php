<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadicadosAsociadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radicados_asociados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('RasRadicados');
            $table->foreign('id')->references('id')->on('radicados')->onDelete('no action')->onUpdate('no action');
            $table->string('RasRadcadoAsociado',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_radicados_asociados');
    }
}
