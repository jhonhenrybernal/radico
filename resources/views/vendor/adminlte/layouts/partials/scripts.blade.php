<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->

<script src="{{ asset('jquery/jquery.min.js') }}"></script>
<!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
    dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
   3.3.x versions without popper.min.js. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<!-- the main fileinput plugin file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/themes/fa/theme.js"></script>
<!-- optionally if you need translation for your language then include  locale file as mentioned below -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/locales/es.js"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script >
    $("#RasRadcadoAsociado").select2({
      placeholder:"seleccione",
        ajax: {
          url: 'radicados/asociados/asig',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.RaNumero,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });


    $("#RaUsuarioAsignar").select2({
     
       ajax: {
          url: 'radicados/usuarios/asig',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                if (item.lastname === null) {return {
                        text: item.name+' '+item.UsIdentificacion,
                        id: item.id
                       }} else {return {
                        text: item.name+' '+item.lastname+' '+item.UsIdentificacion,
                        id: item.id
                       }}
                 if (item.UsIdentificacion === null) {return {
                        text: item.name+' '+item.lastname,
                        id: item.id
                       }} else {return {
                       text: item.name+' '+item.lastname+' '+item.UsIdentificacion,
                        id: item.id
                       }}
                  if (item.UsIdentificacion === null && item.lastname === null) {return {
                        text: item.name+' '+item.lastname,
                        id: item.id
                       }} else {return {
                       text: item.name+' '+item.lastname+' '+item.UsIdentificacion,
                        id: item.id
                       }}      
                })
            };
          },
          cache: true
        }
    });

      
    $("#RePersona").select2({
     
       ajax: {
                url: 'remitente/personas/asig',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                  return {
                    results:  $.map(data, function (item) {
                          if (item.identificacion === null && item.primer_apellido === null) {return {
                                  text: item.primer_nombre,
                                  id: item.id
                              }} else {return {
                                  text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                                  id: item.id
                              }}
                          if (item.primer_apellido === null) {return {
                              text: item.primer_nombre+' '+item.identificacion,
                              id: item.id
                          }} else {return {
                             text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                              id: item.id
                          }}
                          if (item.identificacion === null) {return {
                                  text: item.primer_nombre+' '+item.primer_apellido,
                                  id: item.id
                              }} else {return {
                                  text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                                  id: item.id
                              }}         
                         
                      })
                  };
                },
                cache: true
              }
    });

    $("#DesPersona").select2({
      
       ajax: {
                url: 'remitente/personas/asig',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                  return {
                    results:  $.map(data, function (item) {
                          if (item.identificacion === null && item.primer_apellido === null) {return {
                                  text: item.primer_nombre,
                                  id: item.id
                              }} else {return {
                                  text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                                  id: item.id
                              }}
                          if (item.primer_apellido === null) {return {
                              text: item.primer_nombre+' '+item.identificacion,
                              id: item.id
                          }} else {return {
                             text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                              id: item.id
                          }}
                          if (item.identificacion === null) {return {
                                  text: item.primer_nombre+' '+item.primer_apellido,
                                  id: item.id
                              }} else {return {
                                  text: item.primer_nombre+' '+item.primer_apellido+' '+item.identificacion,
                                  id: item.id
                              }}       
                      })
                  };
                },
                cache: true
              }
    });

    $("#RaTipo").select2({
     
       width:'100%',
    });

    $("#RaTipoDocumental").select2({
      
       width:'100%'
    });


</script>
<script type="text/javascript">
//busqueda
  $('#raditable').DataTable({
  "order": [[0, "desc" ]],
  language: {
    
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    ajax: "{{ route('radbusqueda.erm') }}",
    columns:[
    {data: 'RaAsunto', name: 'RaAsunto'},
    {data: 'created_at', name: 'created_at'},
    {data: 'RaFechaVencimiento', name: 'RaFechaVencimiento'},
    {data: 'RaFechaCierre', name: 'RaFechaCierre'},
    {data: 'tipossolicitudes.TsTipoSolicitud', name: 'tipossolicitudes.TsTipoSolicitud'},
   
    {data: 'RaNumero', name: 'RaNumero'},
    {data: 'sitioradicacion.SrDescripcion', name: 'sitioradicacion.SrDescripcion'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });

  //correspondencia
  $.fn.dataTable.ext.errMode = 'none';
  $(document).ready(function (){
  $('#raditable2').DataTable({
  "order": [[0, "ACS" ]],
  language: {
    
        "decimal": "",
        "emptyTable": "Sin radicados",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Radicado no encontrado",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: false,
    serverSide: false,
    ajax: "{{ route('correspondencia.erm') }}",
    columns:[
    {data: 'id', name: 'id'},
    {data: 'RaNumero', name: 'RaNumero'},
    {data: 'created_at', name: 'created_at'},
    {data: 'RaFechaVencimiento', name: 'RaFechaVencimiento'},
    {data: 'tipossolicitudes.TsTiempoSolicitud', name: 'tipossolicitudes.TsTiempoSolicitud'},
    {},
    {data: 'sitioradicacion.SrDescripcion', name: 'sitioradicacion.SrDescripcion'},
    {data: 'tipossolicitudes.TsTipoSolicitud', name: 'tipossolicitudes.TsTipoSolicitud'},
    {data: 'action', name: 'action', orderable: false, searchable: false},

    ],
     "columnDefs": [ {
    "targets": 5,
    "data": "RaTipoTiempoNnombre",
    "render": function (data) {
       if (data == 'vencido'){

         return '<spam class="label label-danger">Vencido</spam>';
          }
      if (data == 'a punto de vencer'){
         return '<spam class="label label-warning">A punto de vencer</spam>';
          }    
      if (data == 'iniciado'){
         return '<spam class="label label-success">Iniciado</spam>';
          }
      if (data  == 'respondido'){
         return '<spam class="label label-primary">Respondido</spam>';
          }
      if (data == 'cerrado'){
       return '<spam class="label label-info">Cerrado</spam>';
        }      
      
    }
   }],
  initComplete: function () {
      this.api().columns([5,7]).every( function () {
        var column = this;
        var select = $('<select><option value=""></option></select>'); 
        column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
          .appendTo($(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        ); 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                    
 
        } );
      } );
      
     //$("#FilTipo,#FilEstado").material_select();
   }
   
  });
  
});




  //tabla de aministrador
   $('#raditableadmin').DataTable({
  "order": [[0, "ACS" ]],
  language: {
    
        "decimal": "",
        "emptyTable": "Sin radicados",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Radicado no encontrado",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: false,
    serverSide: false,
    ajax: "{{ route('correspondencia.ermadmin') }}",
    columns:[
    {data: 'id'},
    {data: 'RaNumero'},
    {data: 'created_at'},
    {data: 'RaFechaVencimiento'},
    {data: 'tipossolicitudes.TsTiempoSolicitud'},
    {},
    {data: 'sitioradicacion.SrDescripcion'},
    {data: 'tipossolicitudes.TsTipoSolicitud'},
    {data: 'action', orderable: false, searchable: false},

    ],
     "columnDefs": [ {
    "targets": 5,
    "data": "RaTipoTiempoNnombre",
   "render": function (data) {
       if (data == 'vencido'){

         return '<spam class="label label-danger">Vencido</spam>';
          }
      if (data == 'a punto de vencer'){
         return '<spam class="label label-warning">A punto de vencer</spam>';
          }    
      if (data == 'iniciado'){
         return '<spam class="label label-success">Iniciado</spam>';
          }
      if (data  == 'respondido'){
         return '<spam class="label label-primary">Respondido</spam>';
          }
      if (data == 'cerrado'){
       return '<spam class="label label-info">Cerrado</spam>';
        }      
      
    }
   }],
   initComplete: function () {
      this.api().columns([5,7]).every( function () {
        var column = this;
        var select = $('<select><option value=""></option></select>'); 
        column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
          .appendTo($(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
        } );
      } );
      
     //$("#FilTipo,#FilEstado").material_select();
   }
  }); 

</script> 
<script type="text/javascript">
  $(document).on('click', '.show-modals', function(){
    var detallegestion = $(this).data('detalle-gestion');
    $("#text-namegestion").html(detallegestion);
     $("#shows").modal('show');
});

</script>

<script>
$(document).on('click', '.show-modal', function(){
    var detalle = $(this).data('detalle');
   $("#text-name").html(detalle);
   $("#show").modal('show');
});


</script>

<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
 <script type="text/javascript">
    $('#select-Reasignar-si').on('click', function(){
        if ( $(this).val() == 2 ) {
            $('#campo-delegar').hide();
            $('#campo-devolver').hide();
            $('#campo-parcial').hide();
            $('#campo-definitiva').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select-delegar').hide();
           $('#campo-select-reasignar').show();
           $('#campo-dia').hide();
           $('#campo-aparecer').show();    
     
        } else {
                         
        } 
    });
    $('#select-Delegar-si').on('click', function(){
        if ( $(this).val() == 3 ) {
            $('#campo-reasignar').hide();
            $('#campo-devolver').hide();
            $('#campo-parcial').hide();
            $('#campo-definitiva').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select-delegar').show();
           $('#campo-select-reasignar').hide();
            $('#campo-dia').hide(); 
            $('#campo-aparecer').show();     
     
        } else {
                         
        } 
    });
    $('#select-Devolver-si').on('click', function(){
        if ( $(this).val() == 4 ) {
            $('#campo-delegar').hide();
            $('#campo-reasignar').hide();
            $('#campo-parcial').hide();
            $('#campo-definitiva').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select').hide();
            $('#campo-dia').hide();   
            $('#campo-aparecer').show();   
     
        } else {
                         
        } 
    });
    $('#select-parcial-si').on('click', function(){
        if ( $(this).val() == 5 ) {
            $('#campo-delegar').hide();
            $('#campo-devolver').hide();
            $('#campo-reasignar').hide();
            $('#campo-definitiva').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select').hide();
            $('#campo-dia').hide();
            $('#campo-aparecer').show();      
     
        } else {
                         
        } 
    });
    $('#select-Definitiva-si').on('click', function(){
        if ( $(this).val() == 6 ) {
            $('#campo-delegar').hide();
            $('#campo-devolver').hide();
            $('#campo-parcial').hide();
            $('#campo-reasignar').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select').hide();
            $('#campo-dia').hide(); 
            $('#campo-aparecer').show();     
     
        } else {
                         
        } 
    });
    $('#select-enviado-mensajeria-si').on('click', function(){
        if ( $(this).val() == 9 ) {
            $('#campo-delegar').hide();
            $('#campo-devolver').hide();
            $('#campo-parcial').hide();
            $('#campo-definitiva').hide();
            $('#campo-reasignar').hide();
           $('#campo-enviado').hide();
            $('#campo-recibido').hide();
           $('#campo-select').show();
            $('#campo-dia').hide();
            $('#campo-aparecer').show();     
     
        } else {
                         
        } 
    });
    $('#select-recibido-mensajeria-si').on('click', function(){
        if ( $(this).val() == 10 ) {
            $('#campo-delegar').hide();
            $('#campo-devolver').hide();
            $('#campo-parcial').hide();
            $('#campo-definitiva').hide();
            $('#campo-enviado').hide();
            $('#campo-recibido').hide();
            $('#campo-reasignar').hide();
           $('#campo-select').show();
            $('#campo-dia').show();
            $('#campo-aparecer').show();      
     
        } else {
                         
        } 
    });
     $('input[name="select-aparecer"]').on('click', function(){
        if ( $(this).val() == 'Reiniciar') {
            $('#campo-delegar').show();
            $('#campo-devolver').show();
            $('#campo-parcial').show();
            $('#campo-enviado').show();
            $('#campo-recibido').show();
            $('#campo-definitiva').show();
            $('#campo-reasignar').show();
           $('#campo-select').hide();
           $('#campo-dia').hide();
           $('#campo-aparecer').hide();    
     
        } else {
                         
        } 
    });

     //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
    });

   var estado = $('#estado').val()
   if (estado == 10 || estado == 5 || estado == 6) {
     $('#acciones').hide();
     $('#archivos').hide();
     $('#observaciones').hide();
   }
  </script>

  <script>

      $("#input-b6").fileinput({
          language: 'es',
          uploadUrl: "/documentos/upload",
          uploadAsync: false,
          showPreview: false,
          allowedFileExtensions: ['xls'],
          maxFileCount: 5,
          elErrorContainer: '#kv-error-2'
      }).on('filebatchpreupload', function(event, data, id, index) {
          $('#kv-success-2').html('<h4>Upload Status</h4><ul></ul>').hide();
      }).on('filebatchuploadsuccess', function(event, data) {
           if (data.response.status) { 
            //Función para actualizar cada 4 segundos(4000 milisegundos)
            $(".alert").toggleClass('in out'); 
           $('#list_cvs').DataTable().ajax.reload();
        }else{
           $(".alert2").toggleClass('in ups'); 
           $("#msm").html(data.response.msm);
        }

        
      });

      
           $('#list_cvs').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url('documentos/consulta') }}",
            "columns": [
                {data: 'RaEstado', name: 'RaEstado'},
                {data: 'RaTipo', name: 'RaTipo'},
                {data: 'RaTiposSolicitud', name: 'RaTiposSolicitud'},
                {data: 'RaSitio', name: 'RaSitio'},
                {data: 'RaNoElementos', name: 'RaNoElementos'},
                {data: 'RaTipoDocumental', name: 'RaTipoDocumental'},
                {data: 'RaAsunto', name: 'RaAsunto'},
                {data: 'RaObservaciones', name: 'RaObservaciones'},
                {data: 'RaUsuarioAsignar', name: 'RaUsuarioAsignar'},
            ]
        });


  </script>

