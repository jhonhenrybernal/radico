@extends('layout')


@section('content')
      <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h2 >Detalles</h2>
           <div class="col-lg-12 margin-tb">
           
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('correspondencia.index') }}"> Volver</a>
            </div>
        </div>

         
        </div>
        <div class="box-body">
           @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif    
      <div class="box-header with-border">
          <h2>Informacion de gestion </h2>
 </div>

   <div class="row">
    
    </div>
    <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-12">
            @foreach($aRadiDate as $val) 
            <div class="form-group">
                <strong>fecha de gestion:</strong>
                  <?php echo e($val); ?>
             </div>
            @endforeach 
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            @foreach($aRadiUsuario as $val) 
            <div class="form-group">
                <strong>Quien realizo gestion:</strong>
                  <?php echo e($val); ?>
             </div>
            @endforeach 
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            @foreach($aRadiObservacines as $val) 
            <div class="form-group">
                <strong>Obervaciones:</strong>
                  <?php echo e($val); ?>
             </div>
            @endforeach 
        </div>


         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Numero de gestion: {{ $aRadiNumGes }}</strong>
                {!! DNS1D::getBarcodeHTMLtwo($aRadiNumGes, 'C39+') !!}
                              
            </div>
        </div>
 
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
  <table class="table table-bordered">
                <tbody><tr>
                  <th>Numero</th>  
                  <th>Quien lo realiza</th>
                  <th>Creado</th>
                 
                </tr>
                <tr>
                  @foreach($aRadiestadohisto as $valor)
                   <tr>
                  <td>{{$valor->RaNumero}}</td>
                  <td>{{$valor->name}}</td>
                  <td>{{$valor->created_at}}</td>
                 
                   <td> <a  class="show-modal btn btn-info btn-sm" data-detalle='{{$valor->observaciones}}'>Mas detalles</a></td>
                  </tr>
                  @endforeach
                </tr>
               
              </tbody></table>
            </div>
        
     </div>
      
    </section>
       <!-- Modal -->
  <div class="modal fade" id="show" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mas detalles</h4>
        </div>
        <div class="modal-body">
          <p id="text-name"></p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

 
@endsection