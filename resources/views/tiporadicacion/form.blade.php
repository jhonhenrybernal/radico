<div class="row">
    <table WIDTH="1000">
        <tr>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Descripcion:</strong>
                        {!! Form::text('TrDescripcion', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Funcion de numeracion:</strong>
                        {!! Form::text('TrFuncionNumeracion', null, array('class' => 'form-control')) !!}
                    </div>
                </div>
            </td>
          <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Sigla:</strong>
                        {!! Form::text('TrSigla', null, array('class' => 'form-control','placeholder'=>'Maximo 1 letras y minusculas')) !!}
                    </div>
                </div>
            </td>
        </tr>
    </table>
     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Generar</button>
    </div>
</div>