@extends('layout')


@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Courier</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('sitioradicacion.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Suele suceder,por favor verifique.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($sradicado, ['method' => 'PUT','route' => ['sitioradicacion.update', $sradicado->id]]) !!}
        @include('sitioradicacion.form')
    {!! Form::close() !!}

@endsection