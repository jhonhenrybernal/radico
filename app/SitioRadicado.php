<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitioRadicado extends Model
{
    protected $table = 'sitios_radicacion';
    public $timestamps = false;
    protected $fillable = [
    'id',	
   	'SrDescripcion',
   	'SrFuncionNumeracion',
   	'SrSecuenciaNoRad',
   	'SrSigla',
   	
   	];
}
