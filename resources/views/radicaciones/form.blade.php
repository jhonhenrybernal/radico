  <form action="{{ route('radicaciones.store')}}" method="post" class="formulario" name="frm" enctype="multipart/form-data">
    {{ csrf_field() }}



    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <strong>Informacion radicacion</strong>
      </div>
      <div class="box-body">
        <div class="row">
          <table WIDTH="1000">
            <tr> 
              <td>     
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                   <h4>Estado</h4>
                   <select name='RaEstado' id='RaEstado' class="form-control" style="width:260px" required>
                    <option value="">Seleccione </option>
                    @foreach ($radicado as $tipo)
                    <option value="{{ $tipo['id'] }}">{{ $tipo['ErDescripcion'] }}</option>
                    @endforeach
                  </select>             
                </div>
              </div>
            </td> 
            <td>        
             <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <h4>Tipo radicado</h4>
                <select name='RaTipo' id='RaTipo' class="form-control" required >
                  @foreach ($tipoderadicados as $tipo)
                  <option value="{{ $tipo['id'] }}">{{ $tipo['TrDescripcion'] }}</option>
                  @endforeach
                </select>        
              </div>
            </div>
          </td> 
          <td>     
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <h4>Tipo de solicitud</h4>
                <select name='RaTiposSolicitud' id='RaTiposSolicitud' class="form-control" required>
                  <option value="">Seleccione</option>
                  @foreach ($tipodesolicitudes as $tipo)
                  <option value="{{ $tipo['id'] }}">{{ $tipo['TsTipoSolicitud'] }}</option>
                  @endforeach
                </select>          
              </div>
            </div>
          </td>  
          <td>     
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <h4>Sitio de radicacion</h4>
                <select name='RaSitio' id='RaSitio' class="form-control" required style="width:220px">
                  @foreach ($sitioradicar as $tipo)
                  <option value="{{ $tipo['id'] }}">{{ $tipo['SrDescripcion'] }}</option>
                  @endforeach
                </select>        
              </div>
            </div>
          </td> 
          <tr>
           <td>     
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
               <h4>Numero de folios</h4>
               <input placeholder="Inserte numeros" name="RaNoElementos" type="number" class="form-control" value="0"style="width:260px">

             </div>
           </div>
         </td> 
         <td>     
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <h4>Radicado asociado</h4>
              <select name='RasRadcadoAsociado' id='RasRadcadoAsociado' class="form-control" >
               <option value="0">Seleccione</option> </select>   
             </div>
           </div>
         </td> 
         <td>     
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <h4>Tipo de courie</h4>
              <select name='RaCourier' id='RaCourier' class="form-control" >
                <option value="4">Seleccione </option>
                @foreach ($tiposdecurrie as $tipo)
                <option value="{{ $tipo['id'] }}">{{ $tipo['TcoDescripcion'] }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </td>
        <td>     
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <h4>Numero guia</h4>

              <input placeholder="Inserte numeros" name="RaNoGuia" type="number" class="form-control" value="0" style="width:220px">
            </div>
          </div>
        </td>  
      </tr>
      <table WIDTH="1000">    
       <tr>
         <td>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <h4>Tipo documental<h4>
                <select name='RaTipoDocumental' id='RaTipoDocumental' class="form-control" style="width:260px">
                  <option value="0">Seleccione </option>
                  @foreach ($tiposducmentos as $tipo)
                  <option value="{{ $tipo['id'] }}">{{ $tipo['TdrDescripcion'] }}</option>
                  @endforeach
                </select>
              </div>
            </td>
            <td>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <h4>Adjuntar Documentos<h4>
                   <input type="file" class="carpeta" name="file[]" multiple ></div>
                 </div>
               </td>
             </tr> 
           </table>
           <table WIDTH="1000">    
             <tr>


             </tr> 
           </table>
           <table WIDTH="1000">    
             <tr>
               <td>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                   <h4>Asunto</h4>
                   <input name="RaAsunto" type="text" class="form-control"   style="width:920px" required>
                 </div>
               </div>
             </td>

           </tr> 
         </table>
         <table WIDTH="1000">    
           <tr>
             <td>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <h4>Observaciones</h4>
                  {!! Form::textarea('RaObservaciones', null, array('placeholder' => '  ','class' => 'form-control','required','style' => 'width:920px')) !!}
                </div>
              </div>
            </td>

          </tr> 
        </table>

        <tr>
         <td>
          <div class="col-xs-12 col-sm-12 col-md-12">
           <div class="form-group">
             <h4>Asignar a</h4>
             <select name='RaUsuarioAsignar' id='RaUsuarioAsignar'  class="form-control"  required> </select>
           </div>
         </div>
       </td>
     </tr> 
   </div>
 </div>
</div>


<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <strong>Informacion de remitente</strong>
    <div class="pull-right">
      <input class="btn btn-success" type="button" value="Nuevo Remitente" onclick="if (navigator . appName . indexOf('Microsoft Internet Explorer') !=-1) {window . showModelessDialog('radiperso', '', 'dialogTop:50px;dialogLeft:50px;dialogHeight:500px;dialogWidth:700px')}if (navigator . appName . indexOf('Netscape') != -1) {
        window . open('radiperso', '', 'width=700,height=500,left=50,top=50');
        void0}
        ;return false;" />
      </div>
    </div>
    <div class="box-body">
      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <h4>Nombre:</h4>
            <select name='RePersona' id='RePersona' class="form-control"  required> </select>
          </div>
        </div>

      </div>
    </div>
  </div>


  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <strong>Informacion de destinatario</strong>
      <div class="pull-right">
        <input class="btn btn-success" type="button" value="Nuevo destinatario" onclick="if (navigator . appName . indexOf('Microsoft Internet Explorer') !=-1) {window . showModelessDialog('radiperso', '', 'dialogTop:50px;dialogLeft:50px;dialogHeight:500px;dialogWidth:700px')}if (navigator . appName . indexOf('Netscape') != -1) {
          window . open('radiperso', '', 'width=700,height=500,left=50,top=50');
          void0}
          ;return false;" />
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <table WIDTH="1000">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <h4>Nombre:</h4>
                <select name='DesPersona' id='DesPersona' class="form-control" data-live-search="true"> 
                 <option value="0"></option> </select>
               </div>
             </div>
           </table>
         </div>
       </div>
     </div>

     <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary">Generar solicitud</button>
    </div>
  </form>
