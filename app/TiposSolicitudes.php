<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposSolicitudes extends Model
{
   protected $table = 'tipos_solicitudes';
   public $timestamps = false;
   protected $fillable = ['TsTipoSolicitud', 'TsTiempoSolicitud','descripcion','TsSigla'
        ];
}
