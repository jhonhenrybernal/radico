<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolUsuario extends Model
{
    protected $table = 'role_user';
    protected $fillable = ['role_id', 'user_id'
        ];

    /**
     * A departamento can have many comments
     */
	public function roles()
	{
	    return $this->hasMany(Rol::class);
	    // return $this->hasMany('App\Pais');
	}

	public function users()
	{
	    return $this->hasOne(App\User::class,'user_id','id');
	    // return $this->hasMany('App\Pais');
	}
}
