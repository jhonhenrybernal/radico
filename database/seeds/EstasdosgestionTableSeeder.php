<?php

use Illuminate\Database\Seeder;
use App\Estasdosgestion;

class EstasdosgestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estasdosgestion::create([
            'EgNombreEstado'          => 'Peticion',
        ]);

        Estasdosgestion::create([
            'EgNombreEstado'          => 'Queja	',
        ]);

        Estasdosgestion::create([
            'EgNombreEstado'          => 'Reclamo',
        ]);

        Estasdosgestion::create([
            'EgNombreEstado'          => 'Sugerencia',
        ]);

        Estasdosgestion::create([
            'EgNombreEstado'          => 'Denuncia',
        ]);

        Estasdosgestion::create([
            'EgNombreEstado'          => 'Radicado',
        ]);
    }
}
