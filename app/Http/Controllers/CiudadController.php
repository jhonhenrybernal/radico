<?php

namespace App\Http\Controllers;
use App\Ciudad;
use App\Departamento;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departamentos=Departamento::all();
        $ciudades = Ciudad::latest()->paginate(5);
        return view('ciudades.index',compact('ciudades', 'departamentos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos=Departamento::all();
        return view('ciudades.create', compact('departamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'departamento_id'=> 'required',
        ]);
        Ciudad::create($request->all());
        return redirect()->route('ciudades.index')
                                ->with('success','Cities created successfully');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ciudad = Ciudad::find($id);
        $departamento = $ciudad-> departamento;
        return view('ciudades.show',compact('ciudad','departamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamentos = Departamento::all();
        $ciudad = Ciudad::find($id);
        return view('ciudades.edit',compact('ciudad','departamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'departamento_id'=> 'required',
            ]);
            Ciudad::find($id)->update($request->all());
            return redirect()->route('ciudades.index')
                                ->with('success','Cities updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ciudad::find($id)->delete();
        return redirect()->route('ciudades.index')
                        ->with('success','Citys deleted successfully');
    }
}
