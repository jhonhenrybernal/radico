<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primer_nombre',50);
            $table->string('segundo_nombre',50); 
            $table->string('primer_apellido',50); 
            $table->string('segundo_apellido',50);
            $table->integer('tipoidentificacion_id')->unsigned();
            $table->foreign('tipoidentificacion_id')->references('id')->on('tiposidentificacion')->onDelete('no action')->onUpdate('no action');
            $table->string('identificacion',20)->unique(); 
            $table->string('telefono_fijo',10); 
            $table->string('telefono_movil',12); 
            $table->string('email',50)->unique(); 
            $table->string('direccion',50);
            $table->integer('pais_id')->unsigned();
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('no action')->onUpdate('no action');
            $table->integer('departamento_id')->unsigned();
            $table->foreign('departamento_id')->references('id')->on('departamentos')->onDelete('no action')->onUpdate('no action');
            $table->integer('ciudad_id')->unsigned();
            $table->foreign('ciudad_id')->references('id')->on('ciudades')->onDelete('no action')->onUpdate('no action');
            $table->string('ocupacion',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
