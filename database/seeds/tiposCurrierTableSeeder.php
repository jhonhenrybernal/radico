<?php

use Illuminate\Database\Seeder;
use App\TiposCourier;

class tiposCurrierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TiposCourier::create
        ([
            'TcoDescripcion' => 'Mensajeria',
        ]);

         TiposCourier::create
        ([
            'TcoDescripcion' => 'Patinadoe',
        ]);

         TiposCourier::create
        ([
            'TcoDescripcion' => 'Mensajero',
        ]);


    }
}
