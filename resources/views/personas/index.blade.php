@extends('layout')


@section('content')
 <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>personas</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('personas.create') }}">Crear personas</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive" style="overflow: auto">
        <table class="table table-bordered">
            <tr class="thead-inverse" style="font-size:16px">
                <th>No</th>
                <th>Primer Nombre</th>
                <th>Segundo Nombre</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Tipo Identificacion</th>
                <th>Identificacion</th>
                <th>Telefono Fijo</th>
                <th>Telefono Movil</th>
                <th>Email</th>
                <th>Direccion</th>
                <th>Pais</th>
                <th>Deartamento</th>
                <th>Ciudad</th>
                <th>Ocupacion o Cargo</th>
                <th width="280px">Operacion</th>
            </tr>
        @foreach ($personas as $persona)
            <tr style="font-size:14px">
                <td>{{ ++$i }}</td>
                <td>{{ $persona-> primer_nombre}}</td>
                <td>{{ $persona-> segundo_nombre}}</td>
                <td>{{ $persona-> primer_apellido}}</td>
                <td>{{ $persona-> segundo_apellido}}</td>
                <td>
                    @foreach ($tiposidentificacion as $tipoidentificacion)
                        @if ($tipoidentificacion->id === $persona->tipoidentificacion_id)
                            {{$tipoidentificacion->nombre}}
                        @endif
                    @endforeach
                </td>
                <td>{{ $persona-> identificacion}}</td>
                <td>{{ $persona-> telefono_fijo}}</td>
                <td>{{ $persona-> telefono_movil}}</td>
                <td>{{ $persona-> email}}</td>
                <td>{{ $persona-> direccion}}</td>
                <td>
                    @foreach ($paises as $pais)
                        @if ($pais->id === $persona->pais_id)
                            {{$pais->painombre}}
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($departamentos as $departamento)
                        @if ($departamento->id === $persona->departamento_id)
                            {{$departamento->dtonombre}}
                        @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($ciudades as $ciudad)
                        @if ($ciudad->id === $persona->ciudad_id)
                            {{$ciudad->nombre}}
                        @endif
                    @endforeach
                </td>
                <td>{{ $persona-> ocupacion}}</td>
                <td>
                    <a class="btn btn-info btn-sm" href="{{ route('personas.show',$persona->id) }}">Ver</a>
                    <a class="btn btn-primary btn-sm" href="{{ route('personas.edit',$persona->id) }}">Editar</a>
                    {!! Form::open(['method' => 'DELETE','route' => ['personas.destroy', $persona->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    </div>
    {!! $personas->render() !!}
@endsection