<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sistema;
use Illuminate\Routing\Redirector;
use RedirectResponse;

class ConfSistemaController extends Controller
{ 
	public function conf(){

		$conf = [
                    'MAIL_DRIVER' => Sistema::where('key_primaria','MAIL_DRIVER')->value('value_primaria'),
                    'MAIL_HOST' => Sistema::where('key_primaria','MAIL_HOST')->value('value_primaria'),
                    'MAIL_USERNAME' => Sistema::where('key_primaria','MAIL_USERNAME')->value('value_primaria'),
                    'MAIL_PASSWORD' => Sistema::where('key_primaria','MAIL_PASSWORD')->value('value_primaria'),
                    'DB_HOST' => Sistema::where('key_primaria','DB_HOST')->value('value_primaria'),
                    'DB_PORT' => Sistema::where('key_primaria','DB_PORT')->value('value_primaria'),
                    'DB_DATABASE' => Sistema::where('key_primaria','DB_DATABASE')->value('value_primaria'),
                    'DB_USERNAME' => Sistema::where('key_primaria','DB_USERNAME')->value('value_primaria'),
                    'DB_PASSWORD' => Sistema::where('key_primaria','DB_PASSWORD')->value('value_primaria'),
                    'APP_DEBUG' => Sistema::where('key_primaria','APP_DEBUG')->value('value_primaria'),
                    'APP_URL' => Sistema::where('key_primaria','APP_URL')->value('value_primaria'),
					'MAIL_HOST' => Sistema::where('key_primaria','MAIL_HOST')->value('value_primaria'),
                ];


		return view('sistema.index',['sistema' => $conf]);

	}

	public function actualizarConf(Request $request){

				if ($request['MAIL_DRIVER'] == 1) {
					$statusMail = 'smtp';
				}elseif($request['MAIL_DRIVER'] == 0){
					$statusMail = 'log';
				}

				if ($request['APP_DEBUG'] == 1) {
					$statusDebug = 'true';
					
				}elseif($request['APP_DEBUG'] == 0){
					$statusDebug = 'false';
					
				}

     			
  				 Sistema::where('key_primaria','MAIL_DRIVER')->update(['value_primaria' => $statusMail]);
  				 Sistema::where('key_primaria','MAIL_HOST')->update(['value_primaria'=> $request['MAIL_HOST']]);
  				 Sistema::where('key_primaria','MAIL_USERNAME')->update(['value_primaria'=> $request['MAIL_USERNAME']]);
  				 Sistema::where('key_primaria','MAIL_PASSWORD')->update(['value_primaria' => $request['MAIL_PASSWORD']]);
  				 Sistema::where('key_primaria','DB_HOST')->update(['value_primaria' => $request['DB_HOST']]);
  				 Sistema::where('key_primaria','DB_PORT')->update(['value_primaria'=> $request['DB_PORT']]);
  				 Sistema::where('key_primaria','DB_DATABASE')->update(['value_primaria' => $request['DB_DATABASE']]);
  				 Sistema::where('key_primaria','DB_USERNAME')->update(['value_primaria' => $request['DB_USERNAME']]);
  				 Sistema::where('key_primaria','DB_PASSWORD')->update(['value_primaria' => $request['DB_PASSWORD']]);
  				 Sistema::where('key_primaria','APP_DEBUG')->update(['value_primaria' => $statusDebug]);
  				 Sistema::where('key_primaria','APP_URL')->update(['value_primaria' => $request['APP_URL']]);
  				 Sistema::where('key_primaria','MAIL_HOST')->update(['value_primaria' => $request['MAIL_HOST']]);
  				

       			$conf = [
                    'MAIL_DRIVER' => $statusMail,
                    'MAIL_HOST' => $request['MAIL_HOST'],
                    'MAIL_USERNAME' => $request['MAIL_USERNAME'],
                    'MAIL_PASSWORD' => $request['MAIL_PASSWORD'],
                    'DB_HOST' => $request['DB_HOST'],
                    'DB_PORT' => $request['DB_PORT'],
                    'DB_DATABASE' => $request['DB_DATABASE'],
                    'DB_USERNAME' => $request['DB_USERNAME'],
                    'DB_PASSWORD' => $request['DB_PASSWORD'],
                    'APP_DEBUG' => $statusDebug ,
                    'APP_URL' => $request['APP_URL'],
					'MAIL_HOST' =>$request['MAIL_HOST'],
                ];
             

		$this->confOptMail($conf);
		return redirect('conf/general')->with('status', 'Sistema Actualizado!');;
	}

    public function confOptMail(array $values)
	{

	    $envFile = app()->environmentFilePath();
	    $str = file_get_contents($envFile);

	    if (count($values) > 0) {
	        foreach ($values as $envKey => $envValue) {

	            $str .= "\n"; // In case the searched variable is in the last line without \n
	            $keyPosition = strpos($str, "{$envKey}=");
	            $endOfLinePosition = strpos($str, "\n", $keyPosition);
	            $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

	            // If key does not exist, add it
	            if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
	                $str .= "{$envKey}={$envValue}\n";
	            } else {
	                $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
	            }

	        }
	    }

	    $str = substr($str, 0, -1);
	    if (!file_put_contents($envFile, $str)) return false;
	    return true;

	}
}
