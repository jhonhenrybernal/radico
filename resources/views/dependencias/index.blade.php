@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Dependencias</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('dependencias.create') }}"> Crear nueva dependencia </a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Organizacion</th>
            <th width="280px">Operacion</th>
        </tr>
        @foreach ($dependencias as $dependencia)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $dependencia->codigo}}</td>
            <td>{{ $dependencia->nombre}}</td>
            <td>
                @foreach ($organizaciones as $organizacion)
                    @if ($organizacion->id === $dependencia->organizacion_id)
                        {{$organizacion->nombre}}
                    @endif
                @endforeach
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('dependencias.show',$dependencia->id) }}">Ver</a>
                <a class="btn btn-primary" href="{{ route('dependencias.edit',$dependencia->id) }}">Editar</a>
                {!! Form::open(['method' => 'DELETE','route' => ['dependencias.destroy', $dependencia->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
    {!! $dependencias->render() !!}
@endsection