@extends('layout')


@section('content')
   <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Ver tipo de identificacion</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiposidentificacion.index') }}"> Vovler</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tipo:</strong>
                {{$tipoidentificacion->tipo}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $tipoidentificacion->nombre}}
            </div>
        </div>
    </div>
@endsection