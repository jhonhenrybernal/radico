@extends('layout')
@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Estados Usuarios</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('estadosusuario.create') }}"> Crear nuevo usuario</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Usuario de red</th>
            <th>Role</th>
            <th width="280px">Operación</th>
        </tr>
    @foreach ($user as $listauser)
    <tr>
        <td>{{ $listauser->name}}</td>
        <td>{{ $listauser->lastname}}</td>
        <td>{{ $listauser->username}}</td>
        <td>{{ $listauser->roleusers['name']}}</td>
       <td>
            <a class="btn btn-info" href="{{ route('estadosusuario.show',$listauser->id) }}">Detalles</a>
            <a class="btn btn-primary" href="{{ route('estadosusuario.edit',$listauser->id) }}">Editar</a>
            @if($listauser->active == 1)
            <a class="btn btn-danger" href="{{ route('estadosusuario.desactive',$listauser->id) }}">Desactivar</a>
            @else
            <a class="btn btn-warning" href="{{ route('estadosusuario.active',$listauser->id) }}">Activar</a>
            @endif
        </td>
    </tr>
    @endforeach
    </table>
    
@endsection