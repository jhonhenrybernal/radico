<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estasdosgestion;
use App\Radicados;
use App\User;

class Gestiones extends Model
{
    protected $table = 'gestiones';
   
   protected $fillable = ['Estasdosgestion_EgId','	Radicados_RaId','Usuarios_UsId','observaciones','created_at','RaNumero'];


   public function usuariogestion()
   {
       return $this->hasMany(User::class,'Usuarios_UsId','id');
    
   } 
  

 
}
