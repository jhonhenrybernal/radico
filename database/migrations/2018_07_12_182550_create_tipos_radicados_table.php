<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposRadicadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_radicados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('TrDescripcion',60);
            $table->string('TrFuncionNumeracion',255);
            $table->string('TrSigla',1);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_radicados');
    }
}
