<?php

namespace App\Http\Controllers;
use App\TipoAccion;
use Illuminate\Http\Request;

class TiposAccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposaccion = TipoAccion::latest()->paginate(5);
        return view('tiposaccion.index',compact('tiposaccion'))
            ->with('i', (request()->input('page', 1) - 1) * 5); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposaccion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'nombre' => 'required',
        ]);
        TipoAccion::create($request->all());
        return redirect()->route('tiposaccion.index')
                        ->with('success','Type Action created successfully');    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoaccion = TipoAccion::find($id);
        return view('tiposaccion.show',compact('tipoaccion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoaccion = TipoAccion::find($id);
        return view('tiposaccion.edit',compact('tipoaccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'nombre' => 'required',
        ]);
        TipoAccion::find($id)->update($request->all());
        return redirect()->route('tiposaccion.index')
                        ->with('success','Type Action updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TipoAccion::find($id)->delete();
        return redirect()->route('tiposaccion.index')
                        ->with('success','Type Action deleted successfully');
    }
}
