<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remitente extends Model
{
    protected $table = 'remitentes';
    public $timestamps = false;
    protected $fillable = ['ReRadicado', 'RePersona'
    ];
}
