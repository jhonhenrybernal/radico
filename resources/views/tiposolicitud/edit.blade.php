@extends('layout')


@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar tipo de solicitud</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiposolicitud.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Suele suceder,por favor verifique.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($tsolicitud, ['method' => 'PUT','route' => ['tiposolicitud.update', $tsolicitud->id]]) !!}
        @include('tiposolicitud.form')
    {!! Form::close() !!}

@endsection