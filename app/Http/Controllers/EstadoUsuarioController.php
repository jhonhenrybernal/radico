<?php

namespace App\Http\Controllers;
use App\User;
use App\Roles;
use App\RolUsuario;
use App\TipoIdentificacion;
use App\Cargo;
use Illuminate\Http\Request;
use App\Dependencia;

class EstadoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('estadosusuario.index',compact('user'));
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Roles::all();
        $oLisIdentidad = TipoIdentificacion::all();
        $oLisCargo = Cargo::all();
        $oLisDependencia = Dependencia::all();
        return view('estadosusuario.create',compact('roles','oLisIdentidad','oLisCargo','oLisDependencia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          request()->validate([
            'email' =>  'required|unique:users',
            'name' => 'required',
            'lastname' => 'required',
            'username' => 'required|max:255|unique:users',
            'identificacion' => 'max:255|unique:users',
            'password' => 'required',
            'tipo_role' => 'required',

           
        ],
        [
            'name.required' => 'El campo nombre es obligatorio',
            'lastname.required' => 'El campo apellido es obligatorio',
            'username.required' => 'El campo usuario de red es obligatorio',
            'email.required' => 'El campo Email de red es obligatorio',
            'password.required' => 'El campo password de red es obligatorio',
            'tipo_role.required' => 'El campo Tipo de usuario de red es obligatorio',
            'lastname.unique' => 'El apellido ya esta registrado',
            'username.unique' => 'El usuario de red ya esta registrado',
            'email.unique' => 'El Email ya esta registrado',
            'identificacion.unique' => 'El numero de identificacion ya esta registrado',
          

        ]
    );
        $usuario = User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'UsTipoIdentificacion' => $request['UsTipoIdentificacion'],
            'UsIdentificacion' => $request['UsIdentificacion'],
            'UsTelefono' => $request['UsTelefono'],
            'UsCelular' => $request['UsCelular'],
            'remember_token' => $request['remember_token'],
            'UsDependencia' => $request['UsDependencia'],
            'UsCargos' => $request['UsCargos'],
            'UsRol' => $request['tipo_role']
        ]);
        $id_user=$usuario->id;

         RolUsuario::create([
            'user_id' => $id_user,
            'role_id' => $request['tipo_role'],
         ]);
        return redirect()->route('estadosusuario.create')
                        ->with('success','Creación de usuario exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estadousuario = User::find($id);
        return view('estadosusuario.show',compact('estadousuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Roles::all();
        $oLisIdentidad = TipoIdentificacion::all();
        $oLisCargo = Cargo::all();
        $estadousuario = User::find($id);
        $oLisDependencia = Dependencia::all();
        return view('estadosusuario.edit',compact('roles','oLisIdentidad','oLisCargo','estadousuario','oLisDependencia'));
    }

    public function activeUser($aDat)
    {
        $oStatus = User::find($aDat);
        $oStatus->active = 1;
        $oStatus->save();
        
        return redirect()->route('estadosusuario.index')
                        ->with('success','Actualizado');

    }

     public function desactiveUser($aDat)
    {
        $oStatus = User::find($aDat);
        $oStatus->active = 0;
        $oStatus->save();
        
        return redirect()->route('estadosusuario.index')
                        ->with('success','Actualizado');

    }

    /**

     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $oUser = User::find($id);
         $oUser->name = $request->name;
         $oUser->lastname = $request->lastname;
         $oUser->username = $request->username;
         $oUser->email = $request->email;
         $oUser->UsTipoIdentificacion = $request->UsTipoIdentificacion;
         $oUser->UsIdentificacion = $request->UsIdentificacion;
         $oUser->remember_token = $request->remember_token;
         $oUser->UsTelefono = $request->UsTelefono;
         $oUser->UsCelular = $request->UsCelular;
         $oUser->UsDependencia = $request->UsDependencia;
         $oUser->UsCargos = $request->UsCargos;
         $oUser->UsRol = $request->tipo_role;
         $oUser->password = bcrypt($request->password);
         $oUser->save();

         $oIdRole = RolUsuario::where('user_id', $id)->value('id');
         $oTipoRole = RolUsuario::find($oIdRole);
         $oTipoRole->user_id = $id;
         $oTipoRole->role_id = $request->tipo_role;
         $oTipoRole->save();

        return redirect()->route('estadosusuario.index')
                        ->with('success','Actualizado');
    }

}
