<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
     protected $table = 'sistema';
    public $timestamps = false;
    protected $fillable = [
    'id',	
   	'key_primaria',
   	'value_primaria',
   	'tipo'   	
   	];

}
