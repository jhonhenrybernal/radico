@extends('layout')


@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Departamentos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('departamentos.create') }}"> Crear nuevo departamento</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Codigo</th>
            <th>Departamento</th>
            <th>Pais</th>
            <th width="280px">Operacion</th>
        </tr>
    @foreach ($departamentos as $departamento)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $departamento->dtocodigo}}</td>
        <td>{{ $departamento->dtonombre}}</td>
        <td>
            @foreach ($paises as $pais)
                @if ($pais->id === $departamento->pais_id)
                    {{$pais->painombre}}
                @endif
            @endforeach
        </td>
        <td>
            <a class="btn btn-primary" href="{{ route('departamentos.edit',$departamento->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['departamentos.destroy', $departamento->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $departamentos->render() !!}
@endsection