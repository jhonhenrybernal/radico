<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIdentificacion extends Model
{
    protected $table = 'tiposidentificacion';
    public $timestamps = false;
    protected $fillable = ['tipo', 'nombre',
        ];
}
