<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #493dcc;
  color: white;
}
</style>
<style>
div {
  border: 1px solid gray;
  padding: 8px;
}

h1 {
  text-align: left;
  text-transform: left;
  color: black;
}

p {
  text-indent: 50px;
  text-align: Arial black ;
  letter-spacing: 3px;
}

a {
  text-decoration: none;
  color: #008CBA;
}
</style>
</head>
<body>

<div>
  <h2>Radicados</h2>
  <p>señor, Sra. {{$nombre_usuario}} {{$apellido_usuario}} Los sigientes radicados esta en pronto en vencer.</p>
</div>

<table id="customers">
  <tr>
    <th># radicado</th>
    <th>Asunto</th>
    <th>fecha a vencer</th>
  </tr>
  <tr>
    <td>{{$numero_radicado}}</td>
    <td>{{$asunto_radicado}}</td>
    <td>{{$fecha_vencer}}</td>
  </tr>
 
</table>

</body>
</html>
