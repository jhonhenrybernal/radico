<?php

namespace App\Http\Controllers;
use App\RolUsuario;
use App\User;
use App\Rol;
use Illuminate\Http\Request;

class RolUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles=Rol::all();
        $users=User::all();
        $rolesusuarios = RolUsuario::latest()->paginate(5);
        return view('rolesusuarios.index',compact('rolesusuarios','users', 'roles'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Rol::all();
        $users=User::all();
        return view('rolesusuarios.create', compact('roles','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'user_id' => 'required',
            'rol_id' => 'required',
        ]);
        RolUsuario::create($request->all());
        return redirect()->route('rolesusuarios.index')
                        ->with('success','User Role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rolusuario = RolUsuario::find($id);
        $rol = $rolusuario-> rol;
        $user = $rolusuario-> user;
        return view('rolesusuarios.show',compact('rolusuario','rol', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rolusuario = RolUsuario::find($id);
        $roles = Rol::all();
        $users = User::all();
        return view('rolesusuarios.edit',compact('rolusuario','roles','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'rol_id' => 'required',
            'user_id' => 'required', 
        ]);
        RolUsuario::find($id)->update($request->all());
        return redirect()->route('rolesusuarios.index')
                        ->with('success','User Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RolUsuario::find($id)->delete();
        return redirect()->route('rolesusuarios.index')
                        ->with('success','User Role deleted successfully');
    }
}
