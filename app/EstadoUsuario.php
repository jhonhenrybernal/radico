<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoUsuario extends Model
{
		protected $table = 'estadosusuario';
        protected $fillable = ['nombre', 'descripcion'
        ];
}
