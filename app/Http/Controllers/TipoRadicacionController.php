<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposRadicados;
class TipoRadicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $tradicado = TiposRadicados::all();
        return view('tiporadicacion.index',compact('tradicado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $tradicado = TiposRadicados::all();
        return view('tiporadicacion.create', compact('tradicado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          request()->validate([
        'TrDescripcion' => 'required',
       
        ]);
        TiposRadicados::create([
            'TrDescripcion' => request()->TrDescripcion,
            'TrFuncionNumeracion' => request()->TrFuncionNumeracion,
            'TrSigla' => strtoupper(request()->TrSigla) 

        ]);
        return redirect()->route('tiporadicacion.index')
                        ->with('success','Tipo creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function destroy($id)
    {
       TiposRadicados::find($id)->delete();
        return redirect()->route('tiporadicacion.index')
                        ->with('success','Eliminado');
    }
}
