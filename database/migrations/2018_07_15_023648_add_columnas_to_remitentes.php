<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToRemitentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remitentes', function (Blueprint $table)
         {
            $table->integer('ReRadicado')->unsigned();
            $table->foreign('ReRadicado')->references('RaId')->on('radicados')->onDelete('no action')->onUpdate('no action');
            $table->integer('RePersona')->unsigned();
            $table->foreign('RePersona')->references('id')->on('personas')->onDelete('no action')->onUpdate('no action');
           
                     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remitentes', function (Blueprint $table) {

            $table->dropColumn('ReRadicado');
            $table->dropColumn('RePersona');
           
        });
    }
}
