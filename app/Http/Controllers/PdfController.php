<?php

namespace App\Http\Controllers;

use App\Radicados;

class PdfController extends Controller
{

    public function Reporteuno($id)
    {

        $radicados  = Radicados::find($id);
        $view       = view('pdf.radicadoPdf')->with(compact('radicados'));
        $vistaurl   = "pdf.radicadoPdf";
        $pdf        = \App::make('dompdf.wrapper');
        $paper_size = array(0, 0, 1000, 270); //ajuste de hoja
        $pdf->loadHTML($view)->setPaper($paper_size);

        return $pdf->stream();
    }

}
