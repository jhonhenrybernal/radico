@extends('layout')
@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<form action="{{route('actualizar.sistema')}}" method="post">
      {{ csrf_field() }}
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Servidor base de datos</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Direccion servidor</label>
          <input type="text" name="DB_HOST" value="{{$sistema['DB_HOST']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Puerto servidor</label>
          <input type="text" name="DB_PORT" value="{{$sistema['DB_PORT']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Nombre base de datos</label>
          <input type="text" name="DB_DATABASE" value="{{$sistema['DB_DATABASE']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Usuario base de datos</label>
          <input type="text" name="DB_USERNAME" value="{{$sistema['DB_USERNAME']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>constraseña base de datos</label>
          <input type="password" name="DB_PASSWORD" value="{{$sistema['DB_PASSWORD']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
    </div>
    <!-- /.row -->
  </div>
</div>
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Servidor correo</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label>Estado</label>
          <select class="form-control select2" name="MAIL_DRIVER" style="width: 100%;">
            <option value="1" @if($sistema['MAIL_DRIVER'] == 'smtp')selected="selected" @endif>Activo</option>
            <option value="0"  @if($sistema['MAIL_DRIVER'] == 'log')selected="selected" @endif >Inactivo</option>

          </select>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-md-3">
        <div class="form-group">
          <label>nombre servidor</label>
          <input type="text"  name="MAIL_HOST" value="{{$sistema['MAIL_HOST']}}" class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>nombre usuario</label>
          <input type="text" name="MAIL_USERNAME" value="{{$sistema['MAIL_USERNAME']}}"  class="form-control my-colorpicker1">
        </div>             
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Contraseña usuario</label>
          <input type="password" name="MAIL_PASSWORD" value="{{$sistema['MAIL_PASSWORD']}}"  class="form-control my-colorpicker1">
        </div>             
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</div>
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Proyecto</h3>
    <div class="box-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Estado produccion/prueba</label>
            <select class="form-control select2"  name="APP_DEBUG"  style="width: 100%;">
            <option value="1" @if($sistema['APP_DEBUG'] == 'log')selected="selected" @endif>Activo</option>
            <option value="0"  @if($sistema['APP_DEBUG'] == 'smtp')selected="selected" @endif >Inactivo</option>
            </select>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="form-group">
            <label>Url proyecto</label>
            <input type="text" name="APP_URL" value="{{$sistema['APP_URL']}}" class="form-control my-colorpicker1">
          </div>             
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
</div>
<div class="box-footer">
    <button type="submit" class="btn btn-primary">Actualizar cambios</button>
</div>
</form>              
@endsection