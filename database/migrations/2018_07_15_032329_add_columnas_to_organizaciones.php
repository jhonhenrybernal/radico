<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToOrganizaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('organizaciones', function (Blueprint $table)
         {
           $table->integer('tipoidentificacion_id')->unsigned();
            $table->foreign('tipoidentificacion_id')->references('id')->on('tiposidentificacion')->onDelete('no action')->onUpdate('no action');
            
           
                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizaciones', function (Blueprint $table) {

            $table->dropColumn('OrTipoIdentificacion');
         
            
           
        });
    }
}
