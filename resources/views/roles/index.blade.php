@extends('layout')


@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Roles</h2>
            </div>
                   </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
          
            
        </tr>
    @foreach ($roles as $rol)
    <tr>
        <td>{{ $rol->name}}</td>
        <td>{{ $rol->descripcion}}</td>
        <td>
        </td>
    </tr>
    @endforeach
    </table>
  @endsection