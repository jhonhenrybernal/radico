<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TiposSecuenciaRadicacion;  

class SecuenciaRadicado extends Model
{
    protected $table = 'secuencias_radicacion';
    public $timestamps = false;
    protected $fillable = [
    'id',	
   	'SecrAño',
   	'SecrNumero'   	
   	];



public function secuenciaradicados()
   {
       return $this->belongsTo(TiposSecuenciaRadicacion::class,'SecrNumero','id','secuencia');
    
   }
}
