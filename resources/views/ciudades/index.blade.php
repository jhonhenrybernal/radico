@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Ciudades</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('ciudades.create') }}"> Crear nueva ciudad</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr style="font-size:16px">
            <th>No</th>
            <th>Codigo</th>
            <th>Ciudad</th>
            <th>Departamento</th>
            <th width="280px">Operacion</th>
        </tr>
        @foreach ($ciudades as $ciudad)
        <tr style="font-size:14px">
            <td>{{ ++$i }}</td>
            <td>{{ $ciudad->codigo}}</td>
            <td>{{ $ciudad->nombre}}</td>
            <td>
                @foreach ($departamentos as $departamento)
                    @if ($departamento->id === $ciudad->departamento_id)
                        {{$departamento->dtonombre}}
                    @endif
                @endforeach
            </td>
            <td>
                <a class="btn btn-primary btn-sm" href="{{ route('ciudades.edit',$ciudad->id) }}">Editar</a>
                {!! Form::open(['method' => 'DELETE','route' => ['ciudades.destroy', $ciudad->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-sm']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
    {!! $ciudades->render() !!}
@endsection