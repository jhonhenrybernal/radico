<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';
    protected $fillable = ['codigo', 'nombre', 'departamento_id'
        ];

    /**
     * A departamento can have many comments
     */
	public function departamentos()
	{
	    return $this->hasMany(Departamento::class);
	    // return $this->hasMany('App\Departamento');
	}

}
