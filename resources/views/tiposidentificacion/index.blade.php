@extends('layout')


@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tipos Identificacion</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tiposidentificacion.create') }}"> Crear nuevo tipo de identificacion</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Tipo</th>
            <th>Nombre</th>
            <th width="280px">Operacion</th>
        </tr>
    @foreach ($tiposidentificacion as $tipoidentificacion)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $tipoidentificacion->tipo}}</td>
        <td>{{ $tipoidentificacion->nombre}}</td>
        <td>
            <a class="btn btn-primary" href="{{ route('tiposidentificacion.edit',$tipoidentificacion->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['tiposidentificacion.destroy', $tipoidentificacion->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $tiposidentificacion->render() !!}
@endsection