@extends('layout')


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	Cargar documentos
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
    <a type="button" class="btn bg-olive btn-flat margin" href="/ejemplo/descargar/">Descargar ejemplo de documento</a>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          </div>
        <div class="box-body">
        <input id="input-b6" type="file" class="file" name="file_cvs" method="POST" role="form" enctype="multipart/form-data">
       
    <br>
         <section class="content">         
         <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
             <div class="alert alert-info fade out">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Carga exitosa!</strong>  A continuacion se muestra la carga realizada
            </div>
            <div class="alert2 alert-error fade ups">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Carga fallo!</strong> <h5 href="" id="msm"></h5>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="list_cvs">
                <thead>
                  <tr>
                    <th>Estado</th>
                    <th>Tipo de Radicado</th>
                    <th>Tipo de solicitud</th>
                    <th>Sitio de solicitud</th>
                    <th>#Folios</th>
                    <th>Tipo Documental</th>
                    <th>Asunto</th>
                    <th>Observaciones</th>
                    <th>Asignado</th>
                  </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection