<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trazabilidad extends Model
{
    protected $table = 'trazabilidad';
   public $timestamps = false;
   protected $fillable = ['	TrId', 'Usuarios_UsId','Radicados_RaId','Gestiones_GeId','fecha_gestion','status_gestion'
        ];
}
