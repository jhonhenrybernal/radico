<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RadicadosAsociado extends Model
{
   protected $table = 'radicados_asociados';
   protected $fillable = [
   	'RasRadicados',
   	'RasRadcadoAsociado',
  ];
}
