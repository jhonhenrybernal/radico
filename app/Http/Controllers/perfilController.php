<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator as LAravelVAlidator;

class perfilController extends Controller
{
    public function index()
    {
       $idusuario = Auth::user()->id;
       $perfil = User::find($idusuario);
       return view('perfiles.perfil',['perfil' =>$perfil] );
    }


    public function update(Request $request)    
    {
 
      if ($request['password'] == $request['password-confir']) {
          if (Hash::check($request['password'],Auth::user()->password) ) {
          return redirect()->route('miperfil.index')->with('error','Contraseña no puede ser la anterior');  
        }else{

          $idusuario = Auth::user()->id;
          $UpdPerfil = User::find($idusuario);
          $UpdPerfil->password = bcrypt($request->password);
          $UpdPerfil->save();
          return redirect()->route('miperfil.index')->with('success','Contraseña actualizada');  
        }
      }else{
        return redirect()->route('miperfil.index')->with('error','Contraseña no coinciden');  
      }
    }
}
