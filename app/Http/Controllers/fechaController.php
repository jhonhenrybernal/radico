<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Radicados;
use App\User;
use DB;
use Illuminate\Support\Facades\Mail; 

class fechaController extends Controller
{
    public function consultaEstado()
    {   
    	
     	//obtener el dia actual 
        $oActual = date("Y-m-d",strtotime(date("Y-m-d"))); 
    	$aRadicado2 = DB::table('radicados')->where('RaFechaVencimiento2',$oActual)->update(['RaTipoTiempo' => 2,'RaTipoTiempoNnombre' => 'a punto de vencer' ]);
    	$aRadicado = DB::table('radicados')->where('RaFechaVencimiento',$oActual)->update(['RaTipoTiempo' => 1,'RaTipoTiempoNnombre' => 'vencido']);

		if ($aRadicado2 >= 1) {
    		$this->sendEmail();
		}         
        
    }


    public function sendEmail(){
 
               	$radicados = DB::table('radicados')
	            ->join('users', 'users.id', '=', 'radicados.RaUsuarioAsignar')
	            ->whereBetween('radicados.RaTipoTiempo', [2,3])
	            ->select('users.*', 'radicados.*')
	            ->get();
	            
	              
               	foreach ($radicados as $value) {

               		 $datos = [
	               		'nombre_usuario' => $value->name,
	               		'apellido_usuario' => $value->lastname,
	               		'numero_radicado' => $value->RaNumero,
	               		'asunto_radicado' => $value->RaAsunto,
	               		'fecha_vencer' => $value->RaFechaVencimiento2
	               	];

	               	$user = User::find($value->RaUsuarioAsignar);
               		# code...
	                Mail::send('radicaciones.mails.radicado_vencer',$datos, function($message) use ($user){
	                $message->subject('Radicado a vencer');
	                $message->to($user->email);
	                $message->from('correspondenciaradicados@epmcibate.com','Radicado a vencer ');
	                });
               	}

    }
}