<?php

namespace App\Http\Controllers;
use App\TipoIdentificacion;
use Illuminate\Http\Request;

class TipoIdentificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposidentificacion = TipoIdentificacion::latest()->paginate(5);
        return view('tiposidentificacion.index',compact('tiposidentificacion'))
            ->with('i', (request()->input('page', 1) - 1) * 5); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposidentificacion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'tipo' => 'required',
            'nombre' => 'required',
        ]);
        TipoIdentificacion::create([
            'tipo' => strtoupper(request()->tipo),
            'nombre' => request()->nombre
            ]);
        return redirect()->route('tiposidentificacion.index')
                        ->with('success','Type Identifaction created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoidentificacion = TipoIdentificacion::find($id);
        return view('tiposidentificacion.show',compact('tipoidentificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoidentificacion = TipoIdentificacion::find($id);
        return view('tiposidentificacion.edit',compact('tipoidentificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'tipo' => 'required',
            'nombre' => 'required',
        ]);
        TipoIdentificacion::find($id)->update($request->all());
        return redirect()->route('tiposidentificacion.index')
                        ->with('success','Type Identifaction updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TipoIdentificacion::find($id)->delete();
        return redirect()->route('tiposidentificacion.index')
                        ->with('success','Type Identifaction deleted successfully');
    }
}
