<?php

namespace App;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\TipoIdentificacion;
use App\Cargo;
use App\Dependencia;
use App\Roles;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'active',
        'username',
        'active',
        'lastname',
        'remember_token',
        'UsIdentificacion',
        'UsTelefono',
        'UsCelular',
        'UsUltimoLogin',
        'UsIntentos',
        'UsUltimaIP',
        'UsFechaHoraCambioPW',
        'UsRol',
        'UsEstado',
        'UsTipo',
        'UsCargos',
        'UsTipoIdentificacion',
        'UsDependencia',
        'UsCargos'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tipoidentidad()
    {
        return $this->belongsTo(TipoIdentificacion::class,'UsTipoIdentificacion','id');
    }

     public function tipocargo()
    {
        return $this->belongsTo(Cargo::class,'UsCargos','id','nombre');
    }

     public function tipodependencia()
    {
        return $this->belongsTo(Dependencia::class,'UsDependencia','id','nombre');
    }

     public function scopeName($query , $name)
    {
         return $query->where('name','LIKE',"%$name%"); 
    }

    public function roleusers()
    {
        return $this->hasOne(Roles::class,'id','UsRol');
    } 
}

