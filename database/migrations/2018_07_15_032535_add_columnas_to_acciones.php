<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToAcciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acciones', function (Blueprint $table) {
             $table->integer('AcUsuarios')->unsigned();
            $table->foreign('AcUsuarios')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            $table->integer('AcTipo')->unsigned();
            $table->foreign('AcTipo')->references('TacId')->on('tipos_acciones')->onDelete('no action')->onUpdate('no action');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('acciones', function (Blueprint $table) {

            $table->dropColumn('AcUsuarios');
            $table->dropColumn('AcTipo');
         
            
           
        });
    }
}
