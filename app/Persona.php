<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';
    protected $fillable = ['primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido','tipoidentificacion_id', 'identificacion', 'telefono_fijo', 'telefono_movil', 'email', 'direccion', 'pais_id', 'departamento_id', 'ciudad_id','ocupacion'
        ];

    /**
     * A departamento can have many comments
     */
	public function tiposidentificacion()
	{
	    return $this->hasMany(TipoIdentificacion::class);
	    // return $this->hasMany('App\TipoIdentificacion');
	}

	public function paises()
	{
	    return $this->hasMany(Pais::class);
	    // return $this->hasMany('App\Pais');
	}

	public function departamentos()
	{
	    return $this->hasMany(Departamento::class);
	    // return $this->hasMany('App\Departamento');
	}

	 public function scopePersona($query , $name)
    {
         return $query->where('primer_nombre','LIKE',"%$name%"); 
    }

}
