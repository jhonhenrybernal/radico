<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destinatarios extends Model
{
   protected $table = 'destinatarios';
   public $timestamps = false;
   protected $fillable = ['DesRadicado', 'DesPersona'
        ];
}
