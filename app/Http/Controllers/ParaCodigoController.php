<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SecuenciaRadicado;
use App\TiposSecuenciaRadicacion; 

class ParaCodigoController extends Controller
{
   public function index()
   {
   	  $cod = SecuenciaRadicado::all();
     
      $secod = TiposSecuenciaRadicacion::all();
   	  
   	  return view('radicacioncodigo.index',compact('cod','secuenciaradicados','secod'));
   }

   public function store(Request $request)
   {  
   	       request()->validate([
            'SecrAño' => 'required',
            'SecrNumero' => 'required',
        ]);
        SecuenciaRadicado::create($request->all());
        return redirect()->route('parcod.index')
                        ->with('success','Parametrizacion realizada');
   }
}
