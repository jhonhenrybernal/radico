<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToTrazabilidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trazabilidad', function (Blueprint $table)
         {
            $table->integer('Usuarios_UsId')->unsigned();
            $table->foreign('Usuarios_UsId')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
            $table->integer('Radicados_RaId')->unsigned();
            $table->foreign('Radicados_RaId')->references('RaId')->on('radicados')->onDelete('no action')->onUpdate('no action');
            $table->integer('Gestiones_GeId')->unsigned();
            $table->foreign('Gestiones_GeId')->references('GeId')->on('gestiones')->onDelete('no action')->onUpdate('no action');
        
            
                     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trazabilidad', function (Blueprint $table) {

            $table->dropColumn('Usuarios_UsId');
            $table->dropColumn('Radicados_RaId');
            $table->dropColumn('Gestiones_GeId');
           
        });
    }
}
