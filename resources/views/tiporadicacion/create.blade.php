@extends('layout')


@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Generar Nuevo tipo</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiporadicacion.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>no se pudo generar estado.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'tiporadicacion.store','method'=>'POST')) !!}
         @include('tiporadicacion.form')
    {!! Form::close() !!}

@endsection