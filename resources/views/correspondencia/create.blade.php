@extends('layout')


@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Gestionar radicado</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('correspondencia.index') }}">Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>no se pudo gestionar.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
      @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    {!! Form::open(array('route' => 'gestion.gestionar','method'=>'POST','enctype' => 'multipart/form-data')) !!}
         @include('correspondencia.form')
    {!! Form::close() !!}

@endsection