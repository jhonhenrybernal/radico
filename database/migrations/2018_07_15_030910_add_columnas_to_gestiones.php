<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnasToGestiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gestiones', function (Blueprint $table)
         {
            $table->integer('Estasdosgestion_EgId')->unsigned();
            $table->foreign('Estasdosgestion_EgId')->references('EgId')->on('estasdosgestion')->onDelete('no action')->onUpdate('no action');
            $table->integer('Radicados_RaId')->unsigned();
            $table->foreign('Radicados_RaId')->references('RaId')->on('radicados')->onDelete('no action')->onUpdate('no action');
            $table->integer('Usuarios_UsId')->unsigned();
            $table->foreign('Usuarios_UsId')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        
            
                     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('gestiones', function (Blueprint $table) {

            $table->dropColumn('Estasdosgestion_EgId');
            $table->dropColumn('Radicados_RaId');
            $table->dropColumn('Usuarios_UsId');
           
        });
    }
}
