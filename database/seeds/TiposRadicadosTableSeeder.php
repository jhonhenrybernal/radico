<?php

use Illuminate\Database\Seeder;
use App\TiposRadicados;

class tiposRadicadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TiposRadicados::create([
            'TrDescripcion'       => 'Entrada',
            'TrFuncionNumeracion' => '1',
         ]);
         TiposRadicados::create([
            'TrDescripcion'       => 'Salida',
            'TrFuncionNumeracion' => '2',
         ]);
          TiposRadicados::create([
            'TrDescripcion'       => 'Interno',
            'TrFuncionNumeracion' => '3',
         ]);
    }
}
