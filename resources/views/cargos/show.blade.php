@extends('layout')


@section('content')
   <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Ver cargo </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('cargos.index') }}"> Volver</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Codigo:</strong>
                {{$cargo->codigo}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $cargo->nombre}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Depedencia:</strong>
                {{ $cargo->dependencia_id}}
            </div>
        </div>
    </div>
@endsection