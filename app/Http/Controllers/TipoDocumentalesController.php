<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposDocumentalesRadicados;

class TipoDocumentalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $tiposdocumentales = TiposDocumentalesRadicados::all();
       return view('tiposdocumentales.index',compact('tiposdocumentales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $tiposdocumentales = TiposDocumentalesRadicados::all();
        return view('tiposdocumentales.create', compact('tiposdocumentales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          request()->validate([
        'TdrDescripcion' => 'required',
        'TdrSigla' => 'required',
        
        ]);
        TiposDocumentalesRadicados::create([
            'TdrDescripcion' => request()->TdrDescripcion,
            'TdrSigla' => strtoupper(request()->TdrSigla)         ]
            ]);
        return redirect()->route('tiposdocumentales.index')
                        ->with('success','Documento creado exitosamente');
    }

    public function destroy($id)
    {
         TiposDocumentalesRadicados::find($id)->delete();
        return redirect()->route('tiposdocumentales.index')
                        ->with('success','Eliminado');
    }
}
