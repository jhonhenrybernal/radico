@extends('layout')


@section('content')
 <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Paises</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('paises.create') }}"> Crear nuevo pais</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Codigo</th>
            <th>Nombre</th>
            <th width="280px">Operacion</th>
        </tr>
    @foreach ($paises as $pais)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $pais->paicodigo}}</td>
        <td>{{ $pais->painombre}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('paises.show',$pais->id) }}">Ver</a>
            <a class="btn btn-primary" href="{{ route('paises.edit',$pais->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['paises.destroy', $pais->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $paises->render() !!}
@endsection