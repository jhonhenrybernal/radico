<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposCourier;
class TipoCourieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
      $tcourie  = TiposCourier::all();
       return view('tipocourie.index',compact('tcourie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tcourie = TiposCourier::all();
        return view('tipocourie.create', compact('tcourie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
        'TcoDescripcion' => 'required',
        
        ]);
        TiposCourier::create($request->all());
        return redirect()->route('tipocourie.index')
                        ->with('success','Courie creado exitosamente');
    }

   
    public function destroy($id)
    {
     TiposCourier::find($id)->delete();
        return redirect()->route('tipocourie.index')
                        ->with('success','Eliminado');
    }
}
