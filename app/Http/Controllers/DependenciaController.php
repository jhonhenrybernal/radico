<?php

namespace App\Http\Controllers;
use App\Dependencia;
use App\Organizacion;
use Illuminate\Http\Request;

class DependenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizaciones=Organizacion::all();
        $dependencias = Dependencia::latest()->paginate(5);
        return view('dependencias.index',compact('organizaciones', 'dependencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organizaciones=Organizacion::all();
        return view('dependencias.create', compact('organizaciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'organizacion_id'=> 'required',
        ]);
        Dependencia::create($request->all());
        return redirect()->route('dependencias.index')
                                ->with('success','Dependence created successfully');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dependencia = Dependencia::find($id);
        $organizacion = $dependencia-> organizacion;
        return view('dependencias.show',compact('organizacion','dependencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizaciones = Organizacion::all();
        $dependencia = Dependencia::find($id);
        return view('dependencias.edit',compact('organizaciones','dependencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'codigo'=> 'required',
            'nombre'=> 'required',
            'organizacion_id'=> 'required',
            ]);
            Dependencia::find($id)->update($request->all());
            return redirect()->route('dependencias.index')
                                ->with('success','Dependence updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dependencia::find($id)->delete();
        return redirect()->route('dependencias.index')
                        ->with('success','Dependence deleted successfully');
    }
}
