@extends('layout')


@section('content')
 <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sitio de radicaciones</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('sitioradicacion.create') }}">Generar nuevo sitio</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive" style="overflow: auto">
        <table class="table table-bordered">
            <tr class="thead-inverse" style="font-size:16px">
                <th>id</th>
                <th>Descripcion</th>
                <th>Secuencia numero radicacion</th>
                <th>Sigla</th>
                <th width="280px">Operation</th>
            </tr>
        @foreach ($sradicado as $dato)
            <tr style="font-size:14px">
                <td>{{ $dato-> id}}</td>
                <td>{{ $dato-> SrDescripcion}}</td>
                <td>{{ $dato-> SrSecuenciaNoRad}}</td>
                <td>{{ $dato-> SrSigla}}</td>
                <td>
                   
                    <a class="btn btn-primary btn-sm" href="{{ route('sitioradicacion.edit',$dato->id) }}">Editar</a>
   {!! Form::open(['method' => 'DELETE','route' => ['sitioradicacion.destroy', $dato->id],'style'=>'display:inline']) !!}
   {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    </div>

@endsection