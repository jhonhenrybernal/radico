<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivosdoc extends Model
{
    protected $table = 'archivos';
    public $timestamps = false;
    protected $fillable = ['id','rutaarchivos', 'ArRadicado','archivos'];
}
