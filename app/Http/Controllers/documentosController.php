<?php

namespace App\Http\Controllers;
use DB;
use Excel;
use Illuminate\Http\Request;
use App\imp_exp_doc;
use App\TiposSolicitudes;
use App\User;
use App\Radicados;
use Yajra\DataTables\Datatables;
use Auth;
use App\Persona;
use App\RadicadosAsociado;
use App\TipoIdentificacion;
use App\TiposRadicados;
use App\SitioRadicado;

class documentosController extends Controller
{
	public function cargar(){

		return view('documentos.cargar');
	}

	public function consultaDB(){
		$Radicados = Radicados::with('usuario','estado','tiporadicacion','sitioradicacion','radicadoasociado','tipocurrie','tipossolicitudes','tipossolicitudes','destinatario')->where('RaSitio',6)->where('created_at', date('Y-m-d H:i'))->select('Radicados.*');

		return Datatables::of($Radicados)->make(true);

	}

	public function upload(Request $cvs){
		if($cvs->file('file_cvs'))
		{
			$path = $cvs->file('file_cvs')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get()->toArray();
	
			foreach ($data as $key => $value) {

				$tRque = TiposSolicitudes::where('TsTipoSolicitud',$value['tiporequerimiento'])->first();
				$usuario = User::where('UsIdentificacion','like',$value['txtdependencia'])->first();

				$request = [
					'RaTiposSolicitud' => $value['tiporequerimiento'],
					'RaSitio' => 6,
					'RaTipoDocumental' => 8,
					'RaTipo' => 1,
					'RaTiposSolicitud' => $tRque['id']
				];

				$responseDay = $this->radicaciones()->fechaVencimiento($request);
				$dias = intval($responseDay['oTipoVenci']);
				$diasVencer2 = date("Y-m-d",strtotime(date("Y-m-d")."+ $dias days"));




				if (empty($usuario) ) {
					return response()->json(['status' => false, 'msm' => 'Hay usuarios que no estan registrados, por favor registrar.' ]);
				}else{

				
							$persona = Persona::create([
							'primer_nombre' => $value['txtpresentadorpqr'],
							'tipoidentificacion_id' => TipoIdentificacion::where('tipo',$value['txttipoidentificacion'])->value('id'),
							'identificacion' =>  $value['cedpresentador']

							]);

						$raNumero = $this->radicaciones()->raNumero($request);

						$idPErsona = $persona->id;
						$radicar = Radicados::create([
							'RaNumero' => $raNumero,
							'RaEstado' => (!empty($value['txtdependencia']))?1:0,
							'created_at' => date('Y-m-d H:i'),
							'RaTipo' => (!empty($value['txtdependencia']))?1:0,
							'RaTiposSolicitud' =>(!empty($value['txtdependencia']))?$tRque['id']:0 ,
							'RaTipoDocumental' =>(!empty($value['txtdependencia']))?8:0,
							'RaObservaciones' =>  (!empty($value['txtdependencia']))?$value['observaciones']:'Sin datos',
							'RaAsunto' => 'PQR'.$raNumero,
							'RaNoElementos' => (!empty($value['txtdependencia']))?intval(round($value['folios'])):0,
							'RaUsuarioAsignar' => (!empty($usuario))? $usuario['id']:0,
							'RaFechaVencimiento' => $diasVencer2, 
							'RaFechaVencimiento2' => $responseDay['RaFechaVen2'],
							'RaDesPersona' =>  (!empty($persona))? $idPErsona:0,
							'RaTipoTiempo' => 3,
							'RaTipoTiempoNnombre' =>'iniciado',
							'RaSitio' =>  6,
							'RaUsuario' => Auth::user()->id,
							'RaGestionId' => 1
						]);

						$id = $radicar->id;

						$valorasociado = new RadicadosAsociado();
						$valorasociado->RasRadicados = $id;
						$valorasociado->RasRadcadoAsociado = 0;
						$valorasociado->save();
            	

				}
				 /*
					$image_array = $cvs->file('file_cvs');
					$name_doc = $image_array->getClientOriginalName();
					$destination_path = public_path('/cvs/'.Auth::user()->id);
					$image_array->move($destination_path, $name_doc);

					$img = new imp_exp_doc();
					$img->id_usuario = Auth::user()->id;
					$img->create_at = date('Y-m-d');
					$img->nombre_documento = $name_doc;
					$img->tipo = 'import';
					$img->save();
					 */

				 $datos = [
                       'code' =>  $this->radicaciones()->raNumero($request),
                       'RaUsuarioAsignar'=> $usuario['name'].$usuario['lastname'],
                       'RaEstado'=> 'Creado',
                       'RaTipo'=> TiposRadicados::where('id' ,$tRque['id'])->pluck('TrDescripcion'),
                       'RaSitio'=> SitioRadicado::where('id', $request['RaSitio'])->pluck('SrDescripcion'),
                       'RaTiposSolicitud'=> TiposSolicitudes::where('id',$request['RaTiposSolicitud'])->pluck('TsTipoSolicitud'),
                       'RaObservaciones'=> $value['observaciones'],
                       'RaAsunto'=> $value['nroradicacion']];

				$this->radicaciones()->Email($usuario['id'],$RaPerMail = null,$datos);	
			}
		}
							return response()->json(['status' => true ]);
	}

}
