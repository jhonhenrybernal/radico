<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class status extends Model
{
    protected $table = 'status';
    public $timestamps = false;
    protected $fillable = [
    'id',	
   	'datouno',
   	'datodos',
   	'datotres',
   	];
}
