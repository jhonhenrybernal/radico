@extends('layout')


@section('content')
 <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Correspondencia</h2>
            </div>
           
        </div>
    </div>
  @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong>no se pudo gestionar.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
      @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row" class="controlSidebarOptions" >
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Busqueda de radicados <a type="button" class="btn btn-info" href="{{url('documentos/descargar/xlsx')}}">Exportar Radicados</a></h2>
            </div>
           
        </div>
    </div>
    
       <table id="raditable2" class="display" >
        <thead>
            <tr>
                <th>Filtros</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
        </thead>
        <thead>
           <th>Id</th>
                <th>#Radicado</th>
                <th>Fecha de creacion</th>
                <th>Fecha de vencimiento</th>
                <th>Tiempo (dias)</th>
                <th>Estado</th>
                <th>Sitio</th>
                <th>Tipo solicitud</th>
                <th>Accion</th>
        </thead>
        <tbody>
            
        </tbody>
       </table>
@endsection