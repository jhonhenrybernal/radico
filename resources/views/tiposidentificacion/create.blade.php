@extends('layout')


@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Crear nuevo tipo de identificación</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tiposidentificacion.index') }}"> Volver</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> No se pudo realizar.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'tiposidentificacion.store','method'=>'POST')) !!}
         @include('tiposidentificacion.form')
    {!! Form::close() !!}

@endsection