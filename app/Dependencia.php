<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependencia extends Model
{
    protected $table = 'dependencias';
    protected $fillable = ['codigo', 'nombre', 'organizacion_id'
        ];

    /**
     * A departamento can have many comments
     */
	public function organizaciones()
	{
	    return $this->hasMany(Organizacion::class);
	    // return $this->hasMany('App\Organizacion');
	}
}
