<?php

use Illuminate\Database\Seeder;
use App\TipoIdentificacion;

class TiposIdentificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         TipoIdentificacion::create([
            'tipo'       => 'CC',
            'nombre'     => 'Cedula de ciudadania',
         ]);

         TipoIdentificacion::create([
            'tipo'       => 'Nuip',
            'nombre'     => 'número único de identificación personal',
         ]);

         TipoIdentificacion::create([
            'tipo'       => 'Nip',
            'nombre'     => 'Número de identificación personal',
         ]);

         TipoIdentificacion::create([
            'tipo'       => 'Ce',
            'nombre'     => 'Cédula de Extranjería',
         ]);
    }
}
