@extends('layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tipos de Acción</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tiposaccion.create') }}"> Crear nuevo tipo de acción</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Accion</th>
            <th width="280px">Operacion</th>
        </tr>
    @foreach ($tiposaccion as $tipoaccion)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $tipoaccion->nombre}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('tiposaccion.show',$tipoaccion->id) }}">Ver</a>
            <a class="btn btn-primary" href="{{ route('tiposaccion.edit',$tipoaccion->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['tiposaccion.destroy', $tipoaccion->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $tiposaccion->render() !!}
@endsection