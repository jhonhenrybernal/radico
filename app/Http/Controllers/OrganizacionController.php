<?php

namespace App\Http\Controllers;
use App\Organizacion;
use App\TipoIdentificacion;
use Illuminate\Http\Request;

class OrganizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiposidentificacion=TipoIdentificacion::all();
        $organizaciones = Organizacion::latest()->paginate(5);
        return view('organizaciones.index',compact('organizaciones', 'tiposidentificacion'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tiposidentificacion=TipoIdentificacion::all();
        return view('organizaciones.create', compact('tiposidentificacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'nombre'=> 'required',
            'identificacion'=> 'unique:organizaciones,identificacion',
        ]);
        Organizacion::create($request->all());
        return redirect()->route('organizaciones.index')
                                ->with('success','Organization created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizacion = Organizacion::find($id);
        $tipoidentificacion = $organizacion-> tipoidentificacion;
        return view('organizaciones.show',compact('organizacion','tipoidentificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tiposidentificacion=TipoIdentificacion::all();
        $organizacion = Organizacion::find($id);
        return view('organizaciones.edit',compact('tiposidentificacion','organizacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            request()->validate([
            'nombre'=> 'required',
        ]);
        Organizacion::find($id)->update($request->all());
        return redirect()->route('organizaciones.index')
                                ->with('success','Organization updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Organizacion::find($id)->delete();
        return redirect()->route('organizaciones.index')
                        ->with('success','Organization deleted successfully');
    }
}
