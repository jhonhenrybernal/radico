 <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Informacion gestion principal </h3>
 </div>
 <input type="hidden" name="id_radicado" value="{{$infRadicado->id}}">
 <input type="hidden" id="estado" value="{{$estado_radicado}}" >


        <div class="box-body">
   <dl class="dl-horizontal">
                <dt># radicado:</dt>
                <dd>{{$infRadicado->RaNumero}}</dd>
                <dt>Tipo</dt>
                <dd>{{$infRadicado->TrDescripcion}}</dd>
                <dt>Sitio</dt>
                <dd>{{$infRadicado->SrDescripcion}}</dd>
                <dt>Asunto</dt>
                <dd>{{$infRadicado->RaAsunto}}</dd>
                <dt>Observaciones</dt>
                <dd>{{$infRadicado->RaObservaciones}}</dd>
                @if($estado_radicado == 10)
                <dt>Estado</dt>
                <dd>cerrado</dd>
                @else
                @endif
              </dl>
                   

        </div>
      </div>
      <!-- /.box -->

    </section>
     <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          @if(!$historial)
          <h3 class="box-title">Trazabilidad</h3>
          @else
          <h3 class="box-title">Trazabilidad al radicado asociado <dt>Numero:{{$radicado_principal}}</dt></h3> 
          @endif
     </div>
       
        <div class="box">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Quien lo realiza</th>
                    <th>fecha de gestion </th>
                    <th>Asignado a </th>
                    <th>Estado</th>
                  
                   
                  </tr>
                <tr>
                @foreach($historial as $valor)
                   <tr>
                  <td>{{$valor->name}} {{$valor->lastname}}</td>
                  <td>{{$valor->fecha_gestion}} </td>
                  <td>{{$valor->asig_name}}</td>
                  <td>{{$valor->EgNombreEstado}}</td>
                  
                 <td> <a  class="show-modals btn btn-info btn-sm" data-detalle-gestion='{{$valor->observaciones}}' >Mas detalles</a></td>
                  </tr>
                  @endforeach
              </tr>
              </tbody>
            </table>
           </div>
       </div>
      <!-- /.box -->

    </section>

    <section class="content">
    <div class="box" id="acciones">
      <div class="box-header with-border">
      <h3 class="box-title">Acciones</h3>
      </div>
     <div class="pull-left" id="campo-aparecer" style="display: none;">
          <input type="button" id="select-aparecer-si"
          name="select-aparecer" value="Reiniciar" >
          <label for="select-Reasignar-si" ></label>
      </div>
      <br>
      <div class="box-body">
        <div id="product1">
          <div>
          <a id="campo-reasignar"> 
          <input type="radio" id="select-Reasignar-si"
          name="select-acciones" value="2" >
          <label for="select-Reasignar-si" >Reasignar</label>
          </a>

          <a id="campo-delegar">
          <input type="radio" id="select-Delegar-si"
          name="select-acciones" value="3">
          <label for="select-Delegar-si">Delegar</label>
          </a>

          <a id="campo-devolver">
          <input type="radio" id="select-Devolver-si"
          name="select-acciones" value="4">
          <label for="select-Devolver-si">Devolver</label>
          </a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <a id="campo-parcial">
        <input type="radio" id="select-parcial-si"
        name="select-acciones" value="5">
        <label for="select-parcial-si">Respuesta parcial</label>
        </a>

        <a id="campo-definitiva">
        <input type="radio" id="select-Definitiva-si"
        name="select-acciones" value="6">
        <label for="select-Definitiva-si">Respuesta Definitiva</label>
        </a>

        <a id="campo-enviado">
  
        <input type="radio" id="select-enviado-mensajeria-si"
        name="select-acciones" value="9">
        <label for="select-enviado-mensajeria-si">Enviado a mensajeria</label>
        </a>

        <a id="campo-recibido">
        <input type="radio" id="select-recibido-mensajeria-si"
        name="select-acciones" value="10">
        <label for="select-recibido-mensajeria-si">Recibido por mensajeria</label>
 
        </a>
        
      <div id="campo-select-delegar" style="display: none;">
        <select name="selecion-usuario-delegar" class="form-control">
          <option value="#">Seleccione</option>
          @foreach($lista_usuarios_delegar as $list)
          <option value="{{$list['id']}}">{{$list['name']}} {{$list['lastname']}}</option>
          @endforeach
        </select>
      </div>
      <div id="campo-select-reasignar" style="display: none;">
        <select name="selecion-usuario-reasignar" class="form-control">
          <option value="#">Seleccione</option>
          @foreach($lista_usuarios_reasignar as $list)
          <option value="{{$list['id']}}">{{$list['name']}} {{$list['lastname']}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group" id="campo-dia" style="display: none;">
            <label>Seleccione:</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <div>
                
              <input type="text" name="date" class="form-control pull-right" id="datepicker">
              </div>
            </div>
            <!-- /.input group -->
          </div>
      </div>
    </div>
    </section>


     <section class="content">

      <!-- Default box -->
      <div class="box" id="archivos">
        <div class="box-header with-border">
          <h3 class="box-title">Almacenar archivos</h3>
      </div>
        <div class="box-body">
          <input type="file" class="carpeta" name="img[]" multiple >
          {{ csrf_field() }}
        </div>
      </div>  
      <!-- /.box -->

    </section>
     <section class="content" id="observaciones">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Observaciones</h3>

        </div>
        <div class="box-body">
          {!! Form::textarea('RaObservaciones', null, array('placeholder' => '  ','class' => 'form-control','required')) !!}
        </div>
       </div>
      <!-- /.box -->

       <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                  <button type="submit" class="btn btn-primary">Generar gestion</button>
       </div>
    </section>
          <!-- Modal -->
  <div class="modal fade" id="shows" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mas detalles</h4>
        </div>
        <div class="modal-body">
          <p id="text-namegestion"></p>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  