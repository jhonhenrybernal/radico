<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TiposSecuenciaRadicacion extends Model
{
     protected $table = 'tipos_secuencias_radicacion';
    public $timestamps = false;
    protected $fillable = ['id','secuencia'
        ];
}
