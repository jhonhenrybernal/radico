<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposCourier extends Model
{
    protected $table = 'tipos_courier';
    public $timestamps = false;
    protected $fillable = ['TcoDescripcion'
        ];
}
