<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'departamentos';
    protected $fillable = ['dtocodigo', 'dtonombre', 'pais_id'
        ];

    /**
     * A departamento can have many comments
     */
	public function paises()
	{
	    
	    return $this->hasMany('App\Pais');
	}

}
