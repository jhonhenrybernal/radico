<?php

use Illuminate\Database\Seeder;
use App\EstadosRadicados;

class EstadosRadicadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadosRadicados::create([
            'ErId' => '1',
            'ErDescripcion' => 'Creado',
        ]);

        EstadosRadicados::create([
            'ErId' => '2',
            'ErDescripcion' => 'Anulado',
        ]);

        EstadosRadicados::create([
            'ErId' => '3',
            'ErDescripcion' => 'Borrador',
        ]);


    }
}
